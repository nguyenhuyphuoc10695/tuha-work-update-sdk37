import axios from 'axios';
import CONFIG from '../config/config'

export async function getEmployee(skip, limit) {
    return axios.get(CONFIG.API + '/staff/list?skip=' + skip + '&limit=' + limit);
}

export async function getEmployeeDetail(staff_id) {
    return axios.get(CONFIG.API + '/staff/edit?staff_id=' + staff_id);
}

export async function getContractInfomation(staff_id) {
    return axios.get(CONFIG.API + '/staff/contract?staff_id=' + staff_id);
}

export async function getHRMShiftList() {
    return axios.get(CONFIG.API + '/staff/shift');
}

export async function getHRMShiftRegisterList(staff_id) {
    return axios.get(CONFIG.API + '/staff/shift-register?staff_id=' + staff_id);
}

export async function getDepartmentList() {
    return axios.get(CONFIG.API + '/staff/department/list');
}

export async function getPositionList() {
    return axios.get(CONFIG.API + '/staff/position/list');
}

export async function danh_sach_anh_cham_cong() {
    return axios.get(CONFIG.API + '/staff/position/list');
}

export async function getGroupOptionData() {
    return axios.get(CONFIG.API + '/staff/group/option');
}

export async function getImageList(store_id) {
    return axios.get('https://ai.tuha.vn/api/captured-face?store_id=' + store_id);
}

export async function getImageByUserName(store_id, user_id) {
    return axios.get('https://ai.tuha.vn/api/get-user-images?store_id=' + store_id + '&username_mapping=' + user_id);
}

export async function imageDelete(face_id, store_id) {
    return axios.delete('https://ai.tuha.vn/api/get-user-images', {
        headers: {
            'Content-Type': 'application/json',
        },
        data: JSON.stringify(
            {
                store_id: store_id,
                face_id: face_id
        })
    });
}

export async function updateImages(data) {
    return axios.post('https://ai.tuha.vn/api/faces_store', data,{
        headers: {
            'Content-Type': 'application/json',
        },
    });
}

export async function saveHRM(data) {
    return axios.post(CONFIG.API + '/staff/add', data);
}

export async function updateHRM(data) {
    return axios.post(CONFIG.API + '/staff/update', data);
}

export async function deleteHRM(id) {
    return axios.delete(CONFIG.API + '/staff/delete?id=' + id);
}

export async function saveHRMShift(data) {
    return axios.post(CONFIG.API + '/staff/shift/add', data);
}

export async function updateHRMShift(data) {
    return axios.post(CONFIG.API + '/staff/shift/update', data);
}

export async function deleteHRMShift(id) {
    return axios.delete(CONFIG.API + '/staff/shift/delete?id=' + id);
}

export async function registerHRMShift(data) {
    return axios.post(CONFIG.API + '/staff/shift/register', data);
}

export async function saveHRMStaffShift(data) {
    return axios.post(CONFIG.API + '/staff/shift-register/add', data);
}

export async function deleteHRMStaffShift(id) {
    return axios.delete(CONFIG.API + '/staff/shift-register/delete?id=' + id);
}

export async function getTimeSheetStaffData(month, year, staff_id) {
    return axios.get(CONFIG.API + '/staff/time-sheet?month=' + month + '&year=' + year + '&staff_id=' + staff_id);
}

export async function getTimeSheetData(month, year) {
    return axios.get(CONFIG.API + '/staff/time-sheet-rank?month=' + month + '&year=' + year);
}

export async function saveActionLeave(data) {
    return axios.post(CONFIG.API + '/staff/action/leave', data);
}

export async function updateBreakTime(data) {
    return axios.post(CONFIG.API + '/staff/action/update-break-time', data);
}

export async function updateTimeSheet(data) {
    return axios.post(CONFIG.API + '/staff/action/update-time-sheet', data);
}

export async function deleteTimeSheet(id) {
    return axios.delete(CONFIG.API + '/staff/action/delete-time-sheet?time_sheet_id=' + id);
}
