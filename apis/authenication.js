import axios from 'axios';
import CONFIG from '../config/config'

export function loginToTuhaWork(credentials) {
    // alert(CONFIG.API + 'login');
    return axios.post(CONFIG.API + '/login', {
        username: credentials.username,
        password: credentials.password
    });
}

export function getTuhaWorkToken(data) {
    // alert(CONFIG.API + 'login');
    return axios.post(CONFIG.API + '/tuha-login', data);
}

export function registerAccount(data) {
    return axios.post(CONFIG.API + '/register', {
        shop_name: data.company,
        first_name: data.first_name,
        last_name: data.last_name,
        register_email: data.username,
        register_password: data.password,
        retype_password: data.re_password,
    });
}

export async function getUserInfo() {
    return axios.get(CONFIG.API + '/user/info');
}

export async function getTuhaToken(credentials) {
    // return axios({
    //     method: 'post',
    //     url: 'https://tuha.vn/work-auth/0Auth.php?cmd=login',
    //     data: {
    //         user_id: credentials.username,
    //         password: credentials.password
    //     },
    //     config: { headers: {'Content-Type': 'multipart/form-data' }}
    // });
    const formData = new FormData();
    formData.append("user_id", credentials.username);
    formData.append("password", credentials.password);
    return axios.post('https://tuha.vn/work-auth/0Auth.php?cmd=login', formData);
}

export async function getTuhaUserInfo(token) {
    return axios.get('https://tuha.vn/work-auth/0Auth.php?cmd=get_user&jwt=' + token);
}

export async function getRefeshToken(token) {
    return axios.post(CONFIG.API + '/refresh', {
        token: token,
    });
}

export async function isAdminRole(role) {
    return axios.post(CONFIG.API + '/user/check-role', {
        role: role,
    });
}
