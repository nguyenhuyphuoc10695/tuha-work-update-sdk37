import axios from 'axios';
import CONFIG from '../config/config'

export async function getContractList(keyword, tu_ngay, den_ngay, day_order, status, view_by_date, search_category_id, search_customer_id, skip, limit) {
    return axios.get(CONFIG.API + '/contract/list?search=' + keyword + '&tu_ngay=' + tu_ngay +
        '&den_ngay=' + den_ngay + '&day_order=' + day_order + '&status=' + status + '&view_by_date=' +
        view_by_date + '&search_category_id=' + search_category_id + '&search_customer_id=' + search_customer_id +
        '&skip=' + skip + '&limit=' + limit);
}

export async function getContractCategoryList() {
    return axios.get(CONFIG.API + '/contract/category');
}

export async function getContractCustomerList() {
    return axios.get(CONFIG.API + '/contract/customer');
}


