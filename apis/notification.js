import axios from 'axios';
import CONFIG from '../config/config'

export async function getReminderList(keyword, start, finish, skip, limit) {
    // alert(CONFIG.API + '/reminder/list?keyword=' + keyword + '&start_time=' + start + '&finish_time=' + finish +
    //     '&skip=' + skip + '&limit=' + limit);
    return axios.get(CONFIG.API + '/reminder/list?keyword=' + keyword + '&start_time=' + start + '&finish_time=' + finish +
        '&skip=' + skip + '&limit=' + limit);
}

export async function deleteReminder(id) {
    return axios.delete(CONFIG.API + '/reminder/delete?id=' + id);
}

export async function editReminder(data) {
    return axios.post(CONFIG.API + '/reminder/update', {
        id: data.id,
        start_time: data.start_time,
        finish_time: data.finish_time,
        is_public: data.public,
        is_send_email: data.email,
        message: data.message,
    });
}

export async function saveReminder(data) {
    return axios.post(CONFIG.API + '/reminder/add', {
        start_time: data.start_time,
        finish_time: data.finish_time,
        is_public: data.public,
        is_send_email: data.email,
        message: data.message,
    });
}

