import axios from 'axios';
import CONFIG from '../config/config'

export async function getCheckList(staff_id, type, skip, limit) {
    return axios.get(CONFIG.API + '/work/list?staff_id=' + staff_id + '&type=' + type + '&skip=' + skip + '&limit=' + limit);
}

export async function deleteCheckList(id) {
    return axios.delete(CONFIG.API + '/work/delete?id=' + id);
}

export async function detailCheckList(id) {
    return axios.get(CONFIG.API + '/work/edit?id=' + id);
}

export async function editCheckList(data) {
    return axios.post(CONFIG.API + '/work/update', {
        name: data.name,
        description: data.description,
        start_time: data.start_time,
        end_time: data.end_time,
        category_id: data.category_id,
        staff_id: data.staff_id,
        task_status_id: data.task_status_id,
        project_id: data.project_id,
        point: parseInt(data.point),
        completed_percent: parseInt(data.completed_percent),
        task_id: data.task_id,
    });
}

export async function updateStatus(id) {
    return axios.post(CONFIG.API + '/work/update/status', {
        task_id: id,
    });
}

export async function saveCheckList(data) {
    return axios.post(CONFIG.API + '/work/add', {
        name: data.name,
        description: data.description,
        start_time: data.start_time,
        end_time: data.end_time,
        category_id: data.category_id,
        staff_id: data.staff_id,
        task_status_id: data.task_status_id,
        project_id: data.project_id,
    });
}

export async function getStatusList() {
    return axios.get(CONFIG.API + '/work/status/list');
}

export async function getProjectList() {
    return axios.get(CONFIG.API + '/work/project/list');
}

export async function getEmployeeList() {
    return axios.get(CONFIG.API + '/work/assign/list');
}

export async function getCategoryList() {
    return axios.get(CONFIG.API + '/work/category/list');
}


