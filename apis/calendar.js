import axios from 'axios';
import CONFIG from '../config/config'

export async function getCalendarData(staff_id, month, year) {
    // alert(CONFIG.API + '/calendar/list?month=' + month + '&year=' + year);
    return axios.get(CONFIG.API + '/calendar/list?staff_id=' + staff_id + '&month=' + month + '&year=' + year);
}

export async function getCalendarDetail(staff_id, date) {
    return axios.get(CONFIG.API + '/calendar/detail?staff_id=' + staff_id + '&date=' + date);
}

