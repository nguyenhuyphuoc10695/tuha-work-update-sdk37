import axios from 'axios';
import CONFIG from '../config/config'

export async function getReceiptExpenseList(keyword, start, finish, skip, limit) {
    return axios.get(CONFIG.API + '/receipt-expense/list?search=' + keyword + '&start_time=' + start + '&finish_time=' + finish +
        '&skip=' + skip + '&limit=' + limit);
}


