import React from 'react';
import {createSwitchNavigator, createAppContainer,} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import CalendarScreen from "../screens/calendar/CalendarScreen";
import WorkScreen from "../screens/works/WorkScreen";
import NotificationScreen from "../screens/notifications/NotificationScreen";
import HRMScreen from "../screens/hrm/HRMScreen";
import NotificationCreateScreen from "../screens/notifications/NotificationCreateScreen";
import NotificationSearchScreen from "../screens/notifications/NotificationSearchScreen";
import DetailNotificationScreen from "../screens/notifications/DetailNotificationScreen";
import WorkCreateScreen from "../screens/works/WorkCreateScreen";
import WorkEditScreen from "../screens/works/WorkEditScreen";
import CheckListByDateScreen from "../screens/calendar/CheckListByDateScreen";
// import NotificationSearchScreenResult from "../screens/notifications/NotificationSearchScreenResult";
import HRMCreateScreen from "../screens/hrm/HRMCreateScreen";
import HRMEditScreen from "../screens/hrm/HRMEditScreen";
import ContractScreen from "../screens/hrm/ContractScreen";
import HRMShiftScreen from "../screens/hrm/HRMShiftScreen";
import ShiftCreateScreen from "../screens/hrm/ShiftCreateScreen";
import ShiftEditScreen from "../screens/hrm/ShiftEditScreen";
import ShiftRegisterScreen from "../screens/hrm/ShiftRegisterScreen";
import ShiftRegisterStaffScreen from "../screens/hrm/ShiftRegisterStaffScreen";
import TimeSheetScreen from "../screens/calendar/TimeSheetScreen";
import TimeSheetRankScreen from "../screens/calendar/TimeSheetRankScreen";
import EditTimeSheetScreen from "../screens/calendar/EditTimeSheetScreen";
import EditBreakTimeScreen from "../screens/calendar/EditBreakTimeScreen";
import ReceiptExpenseScreen from "../screens/receipt_expense/ReceiptExpenseScreen";
import ReceiptExpenseSearchScreen from "../screens/receipt_expense/ReceiptExpenseSearchScreen";
import ContractSearchScreen from "../screens/contract/ContractSearchScreen";
import ContractManagerScreen from "../screens/contract/ContractManagerScreen";
import WorkSearch from "../screens/works/WorkSearch";
// import ReceiptExpenseSearchScreenResult from "../screens/receipt_expense/ReceiptExpenseSearchScreenResult";

const AuthStack = createStackNavigator(
    {
        LogIn: {
            screen: LoginScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        Register: {
            screen: RegisterScreen,
            navigationOptions: {
                headerShown: false,
            }
        }
    }
);

const CalendarStack = createStackNavigator(
    {
        Calendar: {
            screen: CalendarScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ListByDate: {
            screen: CheckListByDateScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        TimeSheet: {
            screen: TimeSheetScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        TimeSheetRank: {
            screen: TimeSheetRankScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        EditTimeSheet: {
            screen: EditTimeSheetScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        EditBreakTime: {
            screen: EditBreakTimeScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);
const WorkStack = createStackNavigator(
    {
        Work: {
            screen: WorkScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        WorkCreate: {
            screen: WorkCreateScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        WorkEdit: {
            screen: WorkEditScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        WorkSearch: {
            screen: WorkSearch,
            navigationOptions: {
                headerShown: false,
            }
        }
    }
);
const NotificationStack = createStackNavigator(
    {
        Notification: {
            screen: NotificationScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        NotificationCreate: {
            screen: NotificationCreateScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        NotificationSearch: {
            screen: NotificationSearchScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        // NotificationSearchResult: {
        //     screen: NotificationSearchScreenResult,
        //     navigationOptions: {
        //         headerShown: false,
        //     }
        // },
        DetailNotification: {
            screen: DetailNotificationScreen,
            navigationOptions: {
                headerShown: false,
            }
        }
    }
);

const ReceiptExpenseStack = createStackNavigator(
    {
        ReceiptExpense: {
            screen: ReceiptExpenseScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ReceiptExpenseSearch: {
            screen: ReceiptExpenseSearchScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

const ContractStack = createStackNavigator(
    {
        ContractManager: {
            screen: ContractManagerScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ContractSearch: {
            screen: ContractSearchScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

const HRMStack = createStackNavigator(
    {
        HRM: {
            screen: HRMScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        HRMCreate: {
            screen: HRMCreateScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        HRMEdit: {
            screen: HRMEditScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        Contract: {
            screen: ContractScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ShiftRegisterStaff: {
            screen: ShiftRegisterStaffScreen,
            navigationOptions: {
                headerShown: false,
            }
        }
    }
);

const HRMShift = createStackNavigator(
    {
        HRMShift: {
            screen: HRMShiftScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ShiftCreate: {
            screen: ShiftCreateScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ShiftEdit: {
            screen: ShiftEditScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
        ShiftRegister: {
            screen: ShiftRegisterScreen,
            navigationOptions: {
                headerShown: false,
            }
        },
    }
);

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth: AuthStack,
        Calendar: CalendarStack,
        WorkList: WorkStack,
        NotificationList: NotificationStack,
        ReceiptExpenseList: ReceiptExpenseStack,
        ContractList: ContractStack,
        HRMList: HRMStack,
        HRMShiftList: HRMShift,
    },
    {
        // initialRouteName: 'HRMList',
        initialRouteName: 'AuthLoading'
    }
));
