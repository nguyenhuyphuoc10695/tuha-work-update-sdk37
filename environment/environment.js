
const ENV = {
    dev: {
        // apiUrl: 'http://work-api-test.shopal.vn/api',
        apiUrl: 'https://api-work.tuha.vn/api',
        apiMap: 'https://maps.googleapis.com/maps/api/geocode/json?address=',
    },
    prod: {
        apiUrl: "https://api-work.tuha.vn/api",
        apiMap: "https://maps.googleapis.com/maps/api/geocode/json?address=",
    }
};

const getEnvVars = (env = process.env.NODE_ENV) => {

    if (env === 'development') {
        return ENV.dev;
    } else if (env === 'production') {
        return ENV.prod;
    }
};

export default getEnvVars;
