import React, {Component, useState} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Textarea,
    CheckBox,
    DatePicker,
    ListItem,
    Item,
    Label,
    Input,
    Picker, Toast
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, KeyboardAvoidingView} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {saveCheckList} from "../../apis/checklist";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class WorkCreateScreen extends Component {

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            dateFrom: this.getCurrentDateTime(today),
            dateTo: this.getCurrentDateTime(today),
            category_id: 0,
            staff_id: 0,
            project_id: 0,
            status_id: 8,
            description: '',
            name: '',
            dataStatus: this.props.navigation.getParam('listStatus') ? this.props.navigation.getParam('listStatus') : [],
            dataProject: this.props.navigation.getParam('listProject') ? this.props.navigation.getParam('listProject') : [],
            dataEmployee: this.props.navigation.getParam('listEmployee') ? this.props.navigation.getParam('listEmployee') : [],
            dataCategory: this.props.navigation.getParam('listCategory') ? this.props.navigation.getParam('listCategory') : [],
            isDatePickerVisible: false,
            isDatePickerVisible1: false,
            dateF: new Date(),
            dateT: new Date(),
        };
        this.setDateFrom = this.setDateFrom.bind(this);
        this.setDateTo = this.setDateTo.bind(this);
    }

    showDatePicker = () => {
        this.setState({
            isDatePickerVisible: true,
        });
    };

    hideDatePicker = () => {
        this.setState({
            isDatePickerVisible: false,
        });
    };

    handleConfirm = date => {
        // console.warn("A date has been picked: ", date);
        this.setState({
            dateF: date,
            dateFrom: this.getCurrentDateTime(date),
        });
        this.hideDatePicker();
    };

    showDatePicker1 = () => {
        this.setState({
            isDatePickerVisible1: true,
        });
    };

    hideDatePicker1 = () => {
        this.setState({
            isDatePickerVisible1: false,
        });
    };

    handleConfirm1 = date => {
        // console.warn("A date has been picked: ", date);
        this.setState({
            dateT: date,
            dateTo: this.getCurrentDateTime(date),
        });
        this.hideDatePicker1();
    };

    setDateFrom(newDate) {
        // alert(newDate);
        this.setState({
            dateFrom: newDate
        });
    }

    setDateTo(newDate) {
        this.setState({
            dateTo: newDate
        });
    }

    getCurrentDateTime(date) {
        // let date = new Date();
        let d = date.getDate();
        let m = (date.getMonth() + 1);
        let h = date.getHours();
        let mi = date.getMinutes();
        return (d < 10 ? '0' : '') + d + '/' + (m < 10 ? '0' : '') + m + '/' + date.getFullYear() + ' ' + (h < 10 ? '0' : '') + h + ':' + (mi < 10 ? '0' : '') + mi;
    }

    getFirstDayOfMonth() {
        let date = new Date();
        return '01/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getLastDayOfMonth() {
        let date = new Date();
        return (new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getWorkType(value) {
        this.setState({
            category_id: value
        });
    }

    getWorkAssign(value) {
        // alert(value);
        this.setState({
            staff_id: value
        });
    }

    getWorkProject(value) {
        this.setState({
            project_id: value
        });
    }

    getWorkStatus(value) {
        this.setState({
            status_id: value
        });
    }

    getDescription = (text) => {
        this.setState({ description: text });
    };

    getName = (text) => {
        this.setState({ name: text });
    };

    convertDateObjectToTime(date) {
        return date.getTime()/1000;
    }

    addCheckList() {
        let dataCheckList = {
            name: this.state.name,
            description: this.state.description,
            start_time: this.convertDateObjectToTime(this.state.dateF),
            end_time: this.convertDateObjectToTime(this.state.dateT),
            category_id: this.state.category_id,
            staff_id: this.state.staff_id,
            task_status_id: this.state.status_id,
            project_id: this.state.project_id,
        };
        const { navigation } = this.props;
        // console.log(dataCheckList);
        if (!this.state.name) {
            Toast.show({
                text: "Vui lòng nhập tên công việc",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (this.state.category_id == 0) {
            Toast.show({
                text: "Vui lòng chọn phân loại công việc",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (this.state.task_status_id == 0) {
            Toast.show({
                text: "Vui lòng chọn trạng thái công việc",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (this.state.name && this.state.category_id != 0 && this.state.task_status_id != 0) {
            saveCheckList(dataCheckList).then((response) => {
                // navigation.goBack(null);
                navigation.navigate('Work');
                navigation.state.params.refreshCheckList();
                Toast.show({
                    text: "Thêm mới công việc thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                Toast.show({
                    text: "Thêm mới công việc thất bại",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            })
        }
    }

    render() {
        // const headerHeight = Header.HEIGHT;
        const statusBarHeight = StatusBar.currentHeight;
        const heightToPush = Platform.OS === 'ios' ? 64 : 107 + statusBarHeight;

        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Thêm mới công việc</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.addCheckList()}>
                            <Text style={{color: '#fff'}}>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <ScrollView keyboardDismissMode="on-drag"
                                keyboardShouldPersistTaps="always">
                        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={heightToPush} enabled>
                            <View style={styles.item}>
                                <View>

                                    <DateTimePickerModal
                                        isVisible={this.state.isDatePickerVisible}
                                        mode="datetime"
                                        onConfirm={this.handleConfirm}
                                        onCancel={this.hideDatePicker}
                                    />
                                    <DateTimePickerModal
                                        isVisible={this.state.isDatePickerVisible1}
                                        mode="datetime"
                                        onConfirm={this.handleConfirm1}
                                        onCancel={this.hideDatePicker1}
                                    />
                                </View>
                                <View style={styles.date}>
                                    <View style={styles.dateFrom}>
                                        <Button info onPress={() => this.showDatePicker()}><Text style={{fontSize: 12}}> Chọn ngày bắt đầu </Text></Button>
                                    </View>
                                    <View style={styles.dateTo}>
                                        <Button info onPress={() => this.showDatePicker1()}><Text style={{fontSize: 12}}> Chọn ngày kết thúc </Text></Button>
                                    </View>
                                </View>
                                <View style={styles.date}>
                                    <View style={styles.dateFrom}>
                                        <Input disabled placeholder={this.state.dateFrom}/>
                                        {/*<View style={styles.txtDate}>*/}
                                            {/*<Text style={styles.txtLabel}>Từ ngày</Text>*/}
                                        {/*</View>*/}
                                        {/*<View style={styles.datepicker}>*/}
                                            {/*<DatePicker*/}
                                                {/*defaultDate={new Date()}*/}
                                                {/*minimumDate={new Date(2018, 1, 1)}*/}
                                                {/*maximumDate={new Date(2118, 12, 31)}*/}
                                                {/*locale={"vn"}*/}
                                                {/*timeZoneOffsetInMinutes={undefined}*/}
                                                {/*modalTransparent={false}*/}
                                                {/*animationType={"fade"}*/}
                                                {/*androidMode={"default"}*/}
                                                {/*placeHolderText={this.getFirstDayOfMonth()}*/}
                                                {/*textStyle={{color: Colors.tintColor}}*/}
                                                {/*placeHolderTextStyle={{color: "#070707"}}*/}
                                                {/*onDateChange={this.setDateFrom}*/}
                                                {/*disabled={false}*/}
                                            {/*/>*/}
                                        {/*</View>*/}

                                    </View>
                                    <View style={styles.dateTo}>
                                        <Input disabled placeholder={this.state.dateTo}/>
                                        {/*<View style={styles.txtDate}>*/}
                                            {/*<Text style={styles.txtLabel}>Đến ngày</Text>*/}
                                        {/*</View>*/}
                                        {/*<View style={styles.datepicker}>*/}
                                            {/*<DatePicker*/}
                                                {/*defaultDate={new Date()}*/}
                                                {/*minimumDate={new Date(2018, 1, 1)}*/}
                                                {/*maximumDate={new Date(2118, 12, 31)}*/}
                                                {/*locale={"vn"}*/}
                                                {/*timeZoneOffsetInMinutes={undefined}*/}
                                                {/*modalTransparent={false}*/}
                                                {/*animationType={"fade"}*/}
                                                {/*androidMode={"default"}*/}
                                                {/*placeHolderText={this.getLastDayOfMonth()}*/}
                                                {/*textStyle={{color: Colors.tintColor}}*/}
                                                {/*placeHolderTextStyle={{color: "#070707"}}*/}
                                                {/*onDateChange={this.setDateTo}*/}
                                                {/*disabled={false}*/}
                                            {/*/>*/}
                                        {/*</View>*/}

                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Công việc</Text>
                                    <Item>
                                        <Input placeholder="Nhập tên công việc" onChangeText={this.getName} value={this.state.name}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chọn phân loại</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Phân loại"
                                        placeholder="Phân loại công việc"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.category_id}
                                        onValueChange={this.getWorkType.bind(this)}
                                    >
                                        {this.state.dataCategory.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.name} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chọn nhân viên</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Phân công cho"
                                        placeholder="Chọn nhân viên được giao"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.staff_id}
                                        onValueChange={this.getWorkAssign.bind(this)}
                                    >
                                        <Picker.Item label="Chọn nhân viên được giao" value="0"/>
                                        {this.state.dataEmployee.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.full_name} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Thuộc dự án</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Thuộc dự án"
                                        placeholder="Thuộc dự án"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.project_id}
                                        onValueChange={this.getWorkProject.bind(this)}
                                    >
                                        <Picker.Item label="Thuộc dự án" value="0"/>
                                        {this.state.dataProject.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.name} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chọn trạng thái</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Trạng thái công việc"
                                        placeholder="Đang thực hiện"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.status_id}
                                        onValueChange={this.getWorkStatus.bind(this)}
                                    >
                                        {/*<Picker.Item label="Đang thực hiện" value="8"/>*/}
                                        {this.state.dataStatus.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.name} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Mô tả</Text>
                                    <Textarea rowSpan={5} bordered placeholder="Mổ tả công việc..."
                                              onChangeText={this.getDescription} value={this.state.description}/>
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 5 - 5,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) * 3 / 10 + 5,
    },
    txtLabel: {
        color: '#d3d3d3',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    checkbox02: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

