import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Icon,
    Toast,
    CheckBox,
    ListItem, Drawer, Header, Left, Title, Right, Picker
} from "native-base";
import {
    Platform,
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    Switch,
    TouchableOpacity,
    RefreshControl,
    Alert
} from 'react-native'
import Dimension from "../../constants/Dimension";
import _ from 'lodash';
import {getCheckList, deleteCheckList, updateStatus} from "../../apis/checklist";
import Setting from "../../constants/Setting";
import Colors from "../../constants/Colors";
import {getEmployeeList} from "../../apis/checklist";

const SCREEN_WIDTH = Dimension.window.width;

export default class WorkSearch extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        this.state = {
            switchValue: false,
            checkList: [],
            checkedId: -1,
            staff_id: null,
            work_type: 1000,
            employeeList: [],
            // isCompleted: false,
        }
    }

    async componentDidMount() {
        await this.getWorkEmployee();
    }

    getWorkEmployee() {
        getEmployeeList().then(response => {
            this.setState({
                employeeList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách nhân viên được giao')
        });
    }

    deleteWorkConfirm(id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa công việc này?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.deleteWork(id)
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    deleteWork(id) {
        deleteCheckList(id).then(response => {
            this.removeItemFromList(id);
            Toast.show({
                text: "Xóa công việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: {color: "#28a745"},
                buttonStyle: {backgroundColor: "white"}
            });
        }).catch(error => {
            Toast.show({
                text: "Xóa công việc thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        });
    }

    removeItemFromList = (rid) => {
        // let newArray = [];
        let newArray = this.state.checkList.filter(function (item) {
            return item.id !== rid;
        });
        this.setState({
            checkList: newArray,
        });
    };

    getStaff(value) {
        this.setState({
            staff_id: value,
        });
    }

    getWorkType(value) {
        this.setState({
            work_type: value,
        });
    }

    loadData() {
        this.setState({
            isFetching: true,
        }, () => {
            getCheckList(this.state.staff_id, this.state.work_type,  (this.page - 1) * this.itemPerPage, this.itemPerPage).then(checklists => {
                // alert(JSON.stringify(checklists.data));
                let newData = [];
                _.forEach(checklists.data, checklist => {
                    let isExist = _.find(this.state.checkList, {
                        id: checklist.id
                    });
                    if (!isExist) {
                        newData.push(checklist)
                    }
                });
                this.setState({
                    checkList: this.state.checkList.concat(newData),
                    isFetching: false,
                    allLoaded: checklists.data.length == 0 ? true : false
                });
            }).catch(err => {
                this.setState({
                    checkList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        // alert(123)
        this.page = 1;
        this.setState({
            checkList: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    getCheckListDetail = (item) => {
        this.props.navigation.navigate('WorkEdit', {
            checkListData: item,
            // refreshCheckList: this.removeItemFromList,
            refreshCheckList: this.onRefresh,
            statusData: this.props.dataStatus,
            staffData: this.props.dataEmployee,
            projectData: this.props.dataProject,
            categoryData: this.props.dataCategory,
        });
        1
    };

    handleCheck = (checkedId) => {
        this.setState({checkedId});
        updateStatus(checkedId).then(response => {
            this.removeItemFromList(checkedId);
            Toast.show({
                text: "Cập nhật trạng thái công việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: {color: "#28a745"},
                buttonStyle: {backgroundColor: "white"}
            });
        }).catch(error => {
            console.log(error.response.data);
            this.setState({checkedId: -1});
            Toast.show({
                text: "Cập nhật trạng thái công việc thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        })
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({item}) => {

        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.getCheckListDetail(item)}>
                    <View style={styles.switch_type}>
                        {item.project_name ?
                            <View style={styles.type}>
                                <Text style={styles.txtType}>{item.project_name}</Text>
                            </View>
                            :
                            <View style={styles.type}>
                                <Text style={styles.txtType}>Dự án</Text>
                            </View>
                        }
                        <View style={styles.status_switch}>
                            {/*<Switch*/}
                            {/*onChange = {() => this.changeSwitch(item.id)}*/}
                            {/*value = {this.state.switchValue}/>*/}
                            <Button transparent small onPress={() => this.deleteWorkConfirm(item.id)}
                                    style={{alignSelf: 'center', padding: 0}}>
                                <Icon style={{color: '#dc3545'}} ios='ios-trash' android="md-trash"/>
                            </Button>
                        </View>
                    </View>

                    <View>
                        <Text style={styles.information}>{item.name}</Text>
                    </View>
                    <View>
                        <Text>Thực hiện:
                            <Text style={{
                                fontWeight: 'bold',
                                color: '#8a6d3b'
                            }}> {item.first_name} {item.last_name}</Text>
                        </Text>
                    </View>
                    <View>
                        <Text>Thời gian:
                            <Text
                                style={{fontWeight: 'bold',}}> {this.timeConverter(item.start_date)} - {this.timeConverter(item.finish_date)}</Text>
                        </Text>
                    </View>
                    {item.completed_percent != 100 ?
                        <ListItem>
                            <CheckBox checked={item.id == this.state.checkedId} onPress={() => this.handleCheck(item.id)}/>
                            <Body>
                            <Text style={styles.txtBold}>Cập nhật trạng thái hoàn thành</Text>
                            </Body>
                        </ListItem>
                        : null
                    }
                </TouchableOpacity>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Danh sách công việc trống!</Text>
            </View>
        );
    };

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-back' android="md-arrow-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Tìm kiếm công việc</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.onRefresh()}>
                            <Text style={{color: '#fff', fontWeight: 'bold',}}>Xem</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                             <RefreshControl
                                 onRefresh={this.onRefresh.bind(this)}
                                 refreshing={this.state.isFetching}
                                 progressBackgroundColor={'#fff'}
                             />
                         }
                >
                    <View style={styles.content}>
                        <View>
                            <Picker
                                mode="dropdown"
                                iosHeader="Chọn nhân viên cần xem"
                                placeholder="Chọn nhân viên cần xem"
                                iosIcon={<Icon name="arrow-dropdown-circle"
                                               style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.staff_id}
                                onValueChange={this.getStaff.bind(this)}
                            >
                                <Picker.Item enabled={false} label="Chọn nhân viên cần xem" value="null"/>
                                {this.state.employeeList.map((item, key) => {
                                    return (
                                        <Picker.Item label={item.full_name} key={key} value={item.id} />
                                    )
                                })}
                            </Picker>
                        </View>
                        <View>
                            <Picker
                                mode="dropdown"
                                iosHeader="Chọn loại công việc"
                                placeholder="Chọn loại công việc"
                                iosIcon={<Icon name="arrow-dropdown-circle"
                                               style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.work_type}
                                onValueChange={this.getWorkType.bind(this)}
                            >
                                <Picker.Item label="Chọn loại công việc" value="1000"/>
                                <Picker.Item label="Chưa hoàn thành" value="200"/>
                                <Picker.Item label="Đã hoàn thành" value="100"/>
                            </Picker>
                        </View>
                        <FlatList
                            data={this.state.checkList}
                            renderItem={this.renderItem}
                            extraData={this.state.checkList}
                            keyExtractor={this.extractItemKey}
                            // ListEmptyComponent={this.ListEmpty}
                            onEndReached={() => this.loadMore()}
                            onEndReachedThreshold={0.4}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    switch_type: {
        flexDirection: 'row',
    },
    status_switch: {
        width: (SCREEN_WIDTH - 30) * 0.2,
    },
    type: {
        width: (SCREEN_WIDTH - 30) * 0.8,
    },
    txtType: {
        width: (SCREEN_WIDTH - 30) * 0.4,
        padding: 5,
        backgroundColor: '#5bc0de',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#ffffff',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
});

