import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Icon,
    Toast,
    CheckBox,
    ListItem
} from "native-base";
import {
    Platform,
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    Switch,
    TouchableOpacity,
    RefreshControl,
    Alert
} from 'react-native'
import Dimension from "../../constants/Dimension";
import _ from 'lodash';
import {getCheckList, deleteCheckList, updateStatus} from "../../apis/checklist";
import Setting from "../../constants/Setting";

const SCREEN_WIDTH = Dimension.window.width;

export default class UnCompletedWorkScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        this.state = {
            switchValue: false,
            checkList: [],
            checkedId: -1,
            // isCompleted: false,
        }
    }

    // changeSwitch(id) {
    //     // alert(id);
    //     this.setState({
    //         switchValue: true
    //     });
    //
    //     updateStatus(id).then(response => {
    //         this.removeItemFromList(id);
    //         Toast.show({
    //             text: "Cập nhật trạng thái công việc thành công",
    //             buttonText: "Ok",
    //             type: "success",
    //             duration: 5000,
    //             buttonTextStyle: { color: "#28a745" },
    //             buttonStyle: { backgroundColor: "white" }
    //         });
    //     }).catch(error => {
    //         Toast.show({
    //             text: "Cập nhật trạng thái công việc thất bại",
    //             buttonText: "Ok",
    //             type: "danger",
    //             duration: 5000,
    //             buttonTextStyle: { color: "#dc3545" },
    //             buttonStyle: { backgroundColor: "white" }
    //         });
    //     });
    // };
    deleteWorkConfirm(id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa công việc này?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.deleteWork(id)
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    deleteWork(id) {
        deleteCheckList(id).then(response => {
            this.removeItemFromList(id);
            Toast.show({
                text: "Xóa công việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: {color: "#28a745"},
                buttonStyle: {backgroundColor: "white"}
            });
        }).catch(error => {
            Toast.show({
                text: "Xóa công việc thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        });
    }

    removeItemFromList = (rid) => {
        // let newArray = [];
        let newArray = this.state.checkList.filter(function (item) {
            return item.id !== rid;
        });
        this.setState({
            checkList: newArray,
        });
    };

    async componentDidMount() {
        await this.loadData();
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getCheckList(null, 200, (this.page - 1) * this.itemPerPage, this.itemPerPage).then(checklists => {
                // alert(JSON.stringify(reminders.data));
                let newData = [];
                _.forEach(checklists.data, checklist => {
                    let isExist = _.find(this.state.checkList, {
                        id: checklist.id
                    });
                    if (!isExist) {
                        newData.push(checklist)
                    }
                });
                this.setState({
                    checkList: this.state.checkList.concat(newData),
                    isFetching: false,
                    allLoaded: checklists.data.length == 0 ? true : false
                });
            }).catch(err => {
                this.setState({
                    checkList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefreshData = () => {
        // alert(123)
        this.page = 1;
        this.setState({
            checkList: []
        });
        this.setState({
            isFetching: true
        }, () => {
            getCheckList(null, 200, (this.page - 1) * this.itemPerPage, this.itemPerPage).then(checklists => {
                // alert(JSON.stringify(reminders.data));
                let newData = [];
                _.forEach(checklists.data, checklist => {
                    let isExist = _.find(this.state.checkList, {
                        id: checklist.id
                    });
                    if (!isExist) {
                        newData.push(checklist)
                    }
                });
                this.setState({
                    checkList: this.state.checkList.concat(newData),
                    isFetching: false,
                    allLoaded: checklists.data.length == 0 ? true : false
                });
            }).catch(err => {
                this.setState({
                    checkList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    };

    onRefresh = () => {
        // alert(123)
        this.page = 1;
        this.setState({
            checkList: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    getCheckListDetail = (item) => {
        this.props.navigation.navigate('WorkEdit', {
            checkListData: item,
            // refreshCheckList: this.removeItemFromList,
            refreshCheckList: this.onRefresh,
            statusData: this.props.dataStatus,
            staffData: this.props.dataEmployee,
            projectData: this.props.dataProject,
            categoryData: this.props.dataCategory,
        });
        1
    };

    handleCheck = (checkedId) => {
        this.setState({checkedId});
        updateStatus(checkedId).then(response => {
            this.removeItemFromList(checkedId);
            Toast.show({
                text: "Cập nhật trạng thái công việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: {color: "#28a745"},
                buttonStyle: {backgroundColor: "white"}
            });
        }).catch(error => {
            console.log(error.response.data);
            this.setState({checkedId: -1});
            Toast.show({
                text: "Cập nhật trạng thái công việc thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        })
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({item}) => {

        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.getCheckListDetail(item)}>
                    <View style={styles.switch_type}>
                        {item.project_name ?
                            <View style={styles.type}>
                                <Text style={styles.txtType}>{item.project_name}</Text>
                            </View>
                            :
                            <View style={styles.type}>
                                <Text style={styles.txtType}>Dự án</Text>
                            </View>
                        }
                        <View style={styles.status_switch}>
                            {/*<Switch*/}
                            {/*onChange = {() => this.changeSwitch(item.id)}*/}
                            {/*value = {this.state.switchValue}/>*/}
                            <Button transparent small onPress={() => this.deleteWorkConfirm(item.id)}
                                    style={{alignSelf: 'center', padding: 0}}>
                                <Icon style={{color: '#dc3545'}} ios='ios-trash' android="md-trash"/>
                            </Button>
                        </View>
                    </View>

                    <View>
                        <Text style={styles.information}>{item.name}</Text>
                    </View>
                    <View>
                        <Text>Thực hiện:
                            <Text style={{
                                fontWeight: 'bold',
                                color: '#8a6d3b'
                            }}> {item.first_name} {item.last_name}</Text>
                        </Text>
                    </View>
                    <View>
                        <Text>Thời gian:
                            <Text
                                style={{fontWeight: 'bold',}}> {this.timeConverter(item.start_date)} - {this.timeConverter(item.finish_date)}</Text>
                        </Text>
                    </View>
                    <ListItem>
                        <CheckBox checked={item.id == this.state.checkedId} onPress={() => this.handleCheck(item.id)}/>
                        <Body>
                        <Text style={styles.txtBold}>Cập nhật trạng thái hoàn thành</Text>
                        </Body>
                    </ListItem>
                </TouchableOpacity>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{textAlign: 'center'}}>Danh sách công việc trống!</Text>
            </View>
        );
    };

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    render() {
        return (
            <Container style={styles.container}>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                             <RefreshControl
                                 onRefresh={this.onRefresh.bind(this)}
                                 refreshing={this.state.isFetching}
                                 progressBackgroundColor={'#fff'}
                             />
                         }
                >
                    <View style={styles.content}>
                        <FlatList
                            data={this.state.checkList}
                            renderItem={this.renderItem}
                            extraData={this.state.checkList}
                            keyExtractor={this.extractItemKey}
                            ListEmptyComponent={this.ListEmpty}
                            onEndReached={() => this.loadMore()}
                            onEndReachedThreshold={0.4}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    switch_type: {
        flexDirection: 'row',
    },
    status_switch: {
        width: (SCREEN_WIDTH - 30) * 0.2,
    },
    type: {
        width: (SCREEN_WIDTH - 30) * 0.8,
    },
    txtType: {
        width: (SCREEN_WIDTH - 30) * 0.4,
        padding: 5,
        backgroundColor: '#5bc0de',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        // ...Platform.select({
        //     android: {
        //         paddingTop: StatusBar.currentHeight
        //     }
        // }),
        // backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#ffffff',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
});

