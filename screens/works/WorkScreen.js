import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Tabs,
    Tab,
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import UnCompletedWorkScreen from "./UnCompletedWorkScreen";
import CompletedWorkScreen from "./CompletedWorkScreen";
import {getCategoryList, getCheckList, getEmployeeList, getProjectList, getStatusList} from "../../apis/checklist";
import Setting from "../../constants/Setting";

const SCREEN_WIDTH = Dimension.window.width;

export default class WorkScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        this.state = {
            statusList: [],
            projectList: [],
            employeeList: [],
            categoryList: [],
            checkList: [],
        };
        this.child = React.createRef();
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    getWorkStatuses() {
        getStatusList().then(response => {
            this.setState({
                statusList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách trạng thái công việc')
        });
    }

    getWorkProjects() {
        getProjectList().then(response => {
            this.setState({
                projectList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách dự án')
        });
    }

    getWorkEmployee() {
        getEmployeeList().then(response => {
            this.setState({
                employeeList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách nhân viên được giao')
        });
    }

    getWorkCategories() {
        getCategoryList().then(response => {
            // alert(JSON.stringify(response.data))
            this.setState({
                categoryList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách phân loại công việc')
        });
    }

    async componentDidMount() {
        // alert(123)
        await this.getWorkStatuses();
        await this.getWorkProjects();
        await this.getWorkEmployee();
        await this.getWorkCategories();
    }

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => {
                    this.drawer = ref;
                }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                            style={{backgroundColor: '#2a81ab'}}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: '#fff'}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: '#fff'}}>Quản lý công việc</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('WorkSearch')}>
                                <Icon style={{color: '#fff'}} ios='ios-search' android="md-search"/>
                            </Button>
                            <Button transparent onPress={() => this.props.navigation.navigate('WorkCreate', {
                                 listStatus: this.state.statusList,
                                 listProject: this.state.projectList,
                                 listEmployee: this.state.employeeList,
                                 listCategory: this.state.categoryList,
                                 refreshCheckList: this.child.current.onRefreshData,
                            })}>
                                <Icon style={{color: '#fff'}} ios='ios-add-circle' android="md-add-circle"/>
                            </Button>
                        </Right>
                    </Header>
                    {/*<Content style={styles.content}>*/}
                        <Tabs tabBarUnderlineStyle={{borderBottomWidth:3,borderBottomColor: '#2a81ab'}}>
                            <Tab heading="Chưa hoàn thành" tabStyle={{backgroundColor: 'white'}}
                                 textStyle={{color: 'grey',fontSize:18,paddingTop:10,paddingBottom: 10}}
                                 activeTabStyle={{backgroundColor: 'white'}}
                                 activeTextStyle={{color: '#2a81ab', fontWeight: 'normal',fontSize:18,paddingTop:10,paddingBottom: 10}}>
                                <UnCompletedWorkScreen navigation={this.props.navigation} dataStatus={this.state.statusList}
                                                       dataEmployee={this.state.employeeList} dataProject={this.state.projectList}
                                                       dataCategory={this.state.categoryList} ref={this.child}/>
                            </Tab>
                            <Tab heading="Đã hoàn thành" tabStyle={{backgroundColor: 'white'}}
                                 textStyle={{color: 'grey',fontSize:18,paddingTop:10,paddingBottom: 10}}
                                 activeTabStyle={{backgroundColor: 'white'}}
                                 activeTextStyle={{color: '#2a81ab', fontWeight: 'normal',fontSize:18,paddingTop:10,paddingBottom: 10}}>
                                <CompletedWorkScreen navigation={this.props.navigation} dataStatus={this.state.statusList}
                                    dataEmployee={this.state.employeeList} dataProject={this.state.projectList}
                                                     dataCategory={this.state.categoryList} ref={this.child}/>
                            </Tab>
                        </Tabs>
                    {/*</Content>*/}
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#d9534f',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#ffffff',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

