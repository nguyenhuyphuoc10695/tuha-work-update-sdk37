import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    FooterTab, Footer
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View, RefreshControl} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";

import Setting from "../../constants/Setting";
import Colors from "../../constants/Colors";
import _ from 'lodash';
import {getContractList} from "../../apis/contract";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class ContractManagerScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    state = {
        contractList: [],
        isFetching: false,
        keyword: '',
        start_time: '',
        end_time: '',
        day_order: '',
        status: '',
        view_by_date: '',
        search_category_id: '',
        search_customer_id: '',
        total_money: 0,
    };

    constructor(props) {
        super(props);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    async componentDidMount() {
        await this.loadData();
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getContractList(this.state.keyword, this.state.start_time, this.state.end_time, this.state.day_order,
                this.state.status, this.state.view_by_date, this.state.search_category_id, this.state.search_customer_id,
                (this.page - 1) * this.itemPerPage, this.itemPerPage).then(response => {
                // alert(JSON.stringify(response.data));
                let newData = [];
                _.forEach(response.data.data, item => {
                    let isExist = _.find(this.state.contractList, {
                        id: item.id
                    });
                    if (!isExist) {
                        newData.push(item)
                    }
                });
                this.setState({
                    contractList: this.state.contractList.concat(newData),
                    total_money: parseInt(this.state.total_money) + parseInt(response.data.total_money),
                    isFetching: false,
                    allLoaded: response.data.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    contractList: [],
                    total_money: 0,
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        this.page = 1;
        this.setState({
            contractList: [],
            total_money: 0,
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <View>
                    <Text style={{fontWeight: 'bold', color: Colors.tintColor}}>{item.name}</Text>
                </View>
                <View>
                    <Text style={styles.txtBold}>Khách hàng:</Text>
                    <Text style={{textAlign: 'left'}}>{item.customer_name} </Text>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Phân loại:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right', color: Colors.tintColor, fontWeight: 'bold'}}>{item.category_name} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Trạng thái:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right', color: Colors.tintColor, fontWeight: 'bold'}}>{this.checkStatus(item.status)} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Người ký:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right'}}>{item.ten_nguoi_ky} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Giá trị:</Text>   <Text style={{fontWeight: 'bold', color: Colors.tintColor, paddingLeft: 50,}}>{item.total_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Thanh toán:</Text>   <Text style={{fontWeight: 'bold', paddingLeft: 50,}}>{item.total_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                        </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Ngày ký:</Text>   <Text style={{paddingLeft: 50,}}>{this.timeConverter(item.ngay_ky)} </Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Kết thúc:</Text>   <Text style={{paddingLeft: 50,}}>{this.timeConverter(item.ngay_ket_thuc)} </Text>
                        </Text>
                    </View>
                </View>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Danh sách hợp đồng trống!</Text>
            </View>
        );
    };

    checkStatus(id) {
        if (id == "DANG_THUC_HIEN") {
            return 'Đang thực hiện'
        } else if(id == "HOAN_THANH") {
            return 'Đã hoàn thành'
        } else if(id == "CHO_DUYET") {
            return 'Chờ duyệt'
        } else if (id == "HUY") {
            return 'Bị hủy'
        }
    }

    timeConverter(date_string) {
        let day = date_string.split("-");
        return day[2] + '/' + day[1] + '/' + day[0];
    }

    timestampConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => {
                    this.drawer = ref;
                }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                            style={{backgroundColor: '#2a81ab'}}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: '#fff'}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: '#fff'}}>Quản lý hợp đồng</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('ContractSearch')}>
                                <Icon style={{color: '#fff'}} ios='ios-search' android="md-search"/>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                        refreshControl={
                           <RefreshControl
                               onRefresh={this.onRefresh.bind(this)}
                               refreshing={this.state.isFetching}
                               progressBackgroundColor={'#fff'}
                           />
                        }
                    >
                        <View style={styles.content}>
                            <FlatList
                            data={this.state.contractList}
                            renderItem={this.renderItem}
                            extraData={this.state.contractList}
                            keyExtractor={this.extractItemKey}
                            ListEmptyComponent={this.ListEmpty}
                            onEndReached={() => this.loadMore()}
                            onEndReachedThreshold={0.4}
                            />

                        </View>
                    </Content>
                    <View>
                        <Footer>
                            <FooterTab style={{backgroundColor: '#2a81ab', justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{color: '#fff', fontSize: 22, fontWeight: 'bold'}}>Tổng: {this.state.total_money ? this.state.total_money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0} đ</Text>
                            </FooterTab>
                        </Footer>
                    </View>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    item_child: {
        flexDirection: 'row',
    },
    item_col2: {
        width:( SCREEN_WIDTH - 35)/2,
    },
    item_col3: {
        width:( SCREEN_WIDTH - 35)*0.3,
    },
    item_col7: {
        width:( SCREEN_WIDTH - 35)*0.7,
    },
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#f0ad4e',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#f1eee2',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

