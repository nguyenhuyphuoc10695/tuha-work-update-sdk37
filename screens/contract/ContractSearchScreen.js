import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    DatePicker, Input, Item, Picker, Footer, FooterTab
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View, RefreshControl} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";

import Setting from "../../constants/Setting";
import _ from 'lodash';
import {getContractCategoryList, getContractCustomerList, getContractList} from "../../apis/contract";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class ContractSearchScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        let today = new Date();
        let string_first_day = today.getFullYear() + '-' + ((today.getMonth() + 1) < 10 ? '0' : '') + (today.getMonth() + 1) + '-01';
        let string_last_day = today.getFullYear() + '-' + ((today.getMonth() + 1) < 10 ? '0' : '') + (today.getMonth() + 1) + '-' + new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
        this.state = {
            dateFrom: new Date(string_first_day),
            dateTo: new Date(string_last_day),
            keyword: '',
            day_order: 0,
            status: '',
            view_by_date: 3,
            search_category_id: 0,
            search_customer_id: 0,
            contractList: [],
            categoryData: [],
            customerData: [],
            isFetching: false,
            isSearch: false,
            total_money: 0,
        };
        this.setDateFrom = this.setDateFrom.bind(this);
        this.setDateTo = this.setDateTo.bind(this);
    }

    setDateFrom(newDate) {
        this.setState({
            dateFrom: newDate
        });
    }

    setDateTo(newDate) {
        this.setState({
            dateTo: newDate
        });
    }

    async componentDidMount() {
        await this.getContractCategories();
        await this.getContractCustomers();
    }

    getContractCategories() {
        getContractCategoryList().then(response => {
            this.setState({
                categoryData: response.data,
            });
        }).catch(error => {
            this.setState({
                categoryData: [],
            });
        });
    }

    getContractCustomers() {
        getContractCustomerList().then(response => {
            this.setState({
                customerData: response.data,
            });
        }).catch(error => {
            this.setState({
                customerData: [],
            });
        });
    }

    getFirstDayOfMonth() {
        let date = new Date();
        return '01/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getLastDayOfMonth() {
        let date = new Date();
        return (new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getKeyword = (text) => {
        this.setState({ keyword: text });
    };

    getDayOrder(value) {
        this.setState({
            day_order: value
        });
    }

    getStatus(value) {
        this.setState({
            status: value
        });
    }

    getViewByDate(value) {
        this.setState({
            view_by_date: value
        });
    }

    getCategoryID(value) {
        this.setState({
            search_category_id: value
        });
    }

    getCustomerID(value) {
        this.setState({
            search_customer_id: value
        });
    }

    convertDateObjectToString(date) {
        let d = date.getDate();
        let m = (date.getMonth() + 1);
        return date.getFullYear() + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d;
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getContractList(this.state.keyword, this.convertDateObjectToString(this.state.dateFrom), this.convertDateObjectToString(this.state.dateTo),
                this.state.day_order, this.state.status, this.state.view_by_date, this.state.search_category_id, this.state.search_customer_id,
                (this.page - 1) * this.itemPerPage, this.itemPerPage).then(response => {
                // alert(this.convertDateObjectToString(this.state.dateFrom));
                // alert(this.convertDateObjectToString(this.state.dateTo));
                let newData = [];
                _.forEach(response.data.data, item => {
                    let isExist = _.find(this.state.contractList, {
                        id: item.id
                    });
                    if (!isExist) {
                        newData.push(item)
                    }
                });
                this.setState({
                    contractList: this.state.contractList.concat(newData),
                    total_money: parseInt(this.state.total_money) + parseInt(response.data.total_money),
                    isFetching: false,
                    allLoaded: response.data.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    contractList: [],
                    total_money: 0,
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        this.page = 1;
        this.setState({
            contractList: [],
            total_money: 0,
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <View>
                    <Text style={{fontWeight: 'bold', color: Colors.tintColor}}>{item.name}</Text>
                </View>
                <View>
                    <Text style={styles.txtBold}>Khách hàng:</Text>
                    <Text style={{textAlign: 'left'}}>{item.customer_name} </Text>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Phân loại:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right', color: Colors.tintColor, fontWeight: 'bold'}}>{item.category_name} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Trạng thái:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right', color: Colors.tintColor, fontWeight: 'bold'}}>{this.checkStatus(item.status)} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Người ký:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right'}}>{item.ten_nguoi_ky} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Giá trị:</Text>   <Text style={{fontWeight: 'bold', color: Colors.tintColor, paddingLeft: 50,}}>{item.total_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Thanh toán:</Text>   <Text style={{fontWeight: 'bold', paddingLeft: 50,}}>{item.total_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                        </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Ngày ký:</Text>   <Text style={{paddingLeft: 50,}}>{this.timeConverter(item.ngay_ky)} </Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Kết thúc:</Text>   <Text style={{paddingLeft: 50,}}>{this.timeConverter(item.ngay_ket_thuc)} </Text>
                        </Text>
                    </View>
                </View>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không tìm thấy hợp đồng phù hợp!</Text>
            </View>
        );
    };

    checkStatus(id) {
        if (id == "DANG_THUC_HIEN") {
            return 'Đang thực hiện'
        } else if(id == "HOAN_THANH") {
            return 'Đã hoàn thành'
        } else if(id == "CHO_DUYET") {
            return 'Chờ duyệt'
        } else if (id == "HUY") {
            return 'Bị hủy'
        }
    }

    timeConverter(date_string) {
        let day = date_string.split("-");
        return day[2] + '/' + day[1] + '/' + day[0];
    }

    timestampConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    getContractSearch() {
        // alert(this.state.dateFrom.getTime());
        this.setState({
            isSearch: true,
        });
        this.loadData();
    }

    render() {
        const dataViewByDate = [
            {
                id: 1,
                name: '1 ngày tờ lại',
            },
            {
                id: 2,
                name: '2 ngày tờ lại',
            },
            {
                id: 3,
                name: '3 ngày tờ lại',
            },
            {
                id: 4,
                name: '4 ngày tờ lại',
            },
            {
                id: 5,
                name: '5 ngày tờ lại',
            },
            {
                id: 6,
                name: '6 ngày tờ lại',
            },
            {
                id: 7,
                name: '7 ngày tờ lại',
            },
            {
                id: 8,
                name: '8 ngày tờ lại',
            },
            {
                id: 9,
                name: '9 ngày tờ lại',
            },
            {
                id: 10,
                name: '10 ngày tờ lại',
            },
            {
                id: 11,
                name: '11 ngày tờ lại',
            },
            {
                id: 12,
                name: '12 ngày tờ lại',
            },
            {
                id: 13,
                name: '13 ngày tờ lại',
            },
            {
                id: 14,
                name: '14 ngày tờ lại',
            },
            {
                id: 15,
                name: '15 ngày tờ lại',
            },
            {
                id: 16,
                name: '16 ngày tờ lại',
            },
            {
                id: 17,
                name: '17 ngày tờ lại',
            },
            {
                id: 18,
                name: '18 ngày tờ lại',
            },
            {
                id: 19,
                name: '19 ngày tờ lại',
            },
            {
                id: 20,
                name: '20 ngày tờ lại',
            },
            {
                id: 21,
                name: '21 ngày tờ lại',
            },
            {
                id: 22,
                name: '22 ngày tờ lại',
            },
            {
                id: 23,
                name: '23 ngày tờ lại',
            },
            {
                id: 24,
                name: '24 ngày tờ lại',
            },
            {
                id: 25,
                name: '25 ngày tờ lại',
            },
            {
                id: 26,
                name: '26 ngày tờ lại',
            },
            {
                id: 27,
                name: '27 ngày tờ lại',
            },
            {
                id: 28,
                name: '28 ngày tờ lại',
            },
            {
                id: 29,
                name: '29 ngày tờ lại',
            },
            {
                id: 30,
                name: '30 ngày tờ lại',
            },
        ];
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Tìm kiếm hợp đồng</Title>
                    </Body>
                    <Right>
                        <Button transparent
                                onPress={() => this.getContractSearch()}
                        >
                            <Text style={{color: '#fff'}}>Xem</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                             <RefreshControl
                                 onRefresh={this.onRefresh.bind(this)}
                                 refreshing={this.state.isFetching}
                                 progressBackgroundColor={'#fff'}
                             />
                         }
                >
                    <View style={styles.keyword}>
                        <Item bordered>
                            <Icon active name='search' />
                            <Input placeholder='Nhập từ khóa tìm kiếm'
                                   onChangeText={this.getKeyword}
                                   value={this.state.keyword}/>
                        </Item>
                    </View>
                    <View style={styles.date}>
                        <View style={styles.dateFrom}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Từ ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getFirstDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateFrom}
                                    disabled={false}
                                />
                            </View>

                        </View>
                        <View style={styles.dateTo}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Đến ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getLastDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateTo}
                                    disabled={false}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={styles.date}>
                        <View style={styles.dateFrom}>
                            <Picker
                                mode="dropdown"
                                iosHeader="Xem theo ngày"
                                placeholder="Ngày thanh toán"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.view_by_date}
                                onValueChange={this.getViewByDate.bind(this)}
                            >
                                <Picker.Item label="Xem theo ngày" value="0"/>
                                <Picker.Item label="Ngày ký" value="1"/>
                                <Picker.Item label="Ngày kết thúc" value="2"/>
                                <Picker.Item label="Ngày thanh toán" value="3"/>
                            </Picker>
                        </View>
                        <View style={styles.dateTo}>
                            <Picker
                                mode="dropdown"
                                iosHeader="Lọc ngày kết thúc hợp đồng"
                                placeholder="Lọc ngày kết thúc hợp đồng"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.day_order}
                                onValueChange={this.getDayOrder.bind(this)}
                            >
                                <Picker.Item label="Lọc ngày kết thúc hợp đồng" value="0"/>
                                {dataViewByDate.map((item, key) => {
                                    return (
                                        <Picker.Item label={item.name} key={key} value={item.id} />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.date}>
                        <View style={styles.dateFrom}>
                            <Picker
                                mode="dropdown"
                                iosHeader="Trạng thái"
                                placeholder="Trạng thái"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.status}
                                onValueChange={this.getStatus.bind(this)}
                            >
                                <Picker.Item label="Trạng thái" value=""/>
                                <Picker.Item label="Đang thực hiện" value="DANG_THUC_HIEN"/>
                                <Picker.Item label="Đã hoàn thành" value="HOAN_THANH"/>
                                <Picker.Item label="Chờ duyệt" value="CHO_DUYET"/>
                                <Picker.Item label="Bị hủy" value="HUY"/>
                            </Picker>
                        </View>
                        <View style={styles.dateTo}>
                            <Picker
                                mode="dropdown"
                                iosHeader="Chọn danh mục"
                                placeholder="Chọn danh mục"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.search_category_id}
                                onValueChange={this.getCategoryID.bind(this)}
                            >
                                <Picker.Item label="Chọn danh mục" value="0"/>
                                {this.state.categoryData.map((item, key) => {
                                    return (
                                        <Picker.Item label={item.name} key={key} value={item.id} />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.customer}>
                        <Picker
                            mode="dropdown"
                            iosHeader="Khách hàng"
                            placeholder="Khách hàng"
                            iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                            style={{width: undefined}}
                            selectedValue={this.state.search_customer_id}
                            onValueChange={this.getCustomerID.bind(this)}
                        >
                            <Picker.Item label="Khách hàng" value="0"/>
                            {this.state.customerData.map((item, key) => {
                                return (
                                    <Picker.Item label={item.name} key={key} value={item.id} />
                                )
                            })}
                        </Picker>
                    </View>
                    <View style={styles.reminder_list}>
                        {this.state.isSearch ?
                            <FlatList
                                data={this.state.contractList}
                                renderItem={this.renderItem}
                                extraData={this.state.contractList}
                                keyExtractor={this.extractItemKey}
                                ListEmptyComponent={this.ListEmpty}
                                onEndReached={() => this.loadMore()}
                                onEndReachedThreshold={0.4}
                            />
                            : null }
                    </View>

                </Content>
                {this.state.isSearch ?
                    <View>
                        <Footer>
                            <FooterTab style={{backgroundColor: '#2a81ab', justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{color: '#fff', fontSize: 22, fontWeight: 'bold'}}>Tổng: {this.state.total_money ? this.state.total_money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0} đ</Text>
                            </FooterTab>
                        </Footer>
                    </View>
                    : null }
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    item_child: {
        flexDirection: 'row',
    },
    item_col2: {
        width:( SCREEN_WIDTH - 35)/2,
    },
    item_col3: {
        width:( SCREEN_WIDTH - 35)*0.3,
    },
    item_col7: {
        width:( SCREEN_WIDTH - 35)*0.7,
    },
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 5 - 5,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) * 3 / 10 + 5,
    },
    txtLabel: {
        color: '#d3d3d3',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    customer: {
        width: (SCREEN_WIDTH - 20),
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        // paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        // paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        // paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#f0ad4e',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    reminder_list: {
        backgroundColor: '#F7F7F7',
        flex: 1,
        marginTop: 10,
    },
    item: {
        padding: 10,
        backgroundColor: '#f1eee2',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

