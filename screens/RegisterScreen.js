import React, {Component} from 'react';
import Dimension from "../constants/Dimension";
import {
    StyleSheet,
    View,
    AsyncStorage,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Platform
} from "react-native";
import {
    Container,
    Content,
    Item,
    Input,
    Text,
    Icon,
    Header,
    Label,
    Button,
    Left,
    Body,
    Title,
    Right, Toast,
} from 'native-base';
import Colors from "../constants/Colors";
import {registerAccount} from "../apis/authenication";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class RegisterScreen extends Component {
    static navigationOptions = {
        headerShown: false
    };

    constructor(props) {
        super(props);
        AsyncStorage.clear();
        this.state = {
            company: '',
            last_name: '',
            first_name: '',
            username: '',
            password: '',
            re_password: '',
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.status) {
            this.props.navigation.navigate('AuthLoading');
        }
        return false;
    }

    getCompany = (text) => {
        this.setState({company: text});
    };

    getLastName = (text) => {
        this.setState({last_name: text});
    };

    getFirstName = (text) => {
        this.setState({first_name: text});
    };

    getUserName = (text) => {
        this.setState({username: text});
    };

    getPassword = (text) => {
        this.setState({password: text});
    };

    getRePassword = (text) => {
        this.setState({re_password: text});
    };

    addAccount() {
        let dataAccount = {
            company: this.state.company,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            username: this.state.username,
            password: this.state.password,
            re_password: this.state.re_password,
        };
        // console.log(dataAccount);
        const { navigation } = this.props;
        if (this.state.company && this.state.first_name && this.state.last_name && this.state.username && this.state.password && this.state.re_password) {
            if (this.state.password ==  this.state.re_password) {
                registerAccount(dataAccount).then(response => {
                    navigation.navigate('LogIn');
                    Toast.show({
                        text: "Đăng ký tài khoản thành công",
                        buttonText: "Ok",
                        type: "success",
                        duration: 5000,
                        buttonTextStyle: { color: "#28a745" },
                        buttonStyle: { backgroundColor: "white" }
                    });
                }).catch(error => {
                    // alert(JSON.stringify(error.response.data))
                    if (error.response.data.error === 'FAIL_TO_ADD_GROUP') {
                        Toast.show({
                            text: "Thêm nhóm tài khoản thất bại",
                            buttonText: "Ok",
                            type: "danger",
                            duration: 5000,
                            buttonTextStyle: { color: "#dc3545" },
                            buttonStyle: { backgroundColor: "white" }
                        });
                    } else if (error.response.data.error === 'FAIL_TO_ADD_EMPLOYEE_DEPARTMENT') {
                        Toast.show({
                            text: "Thêm phòng ban thất bại",
                            buttonText: "Ok",
                            type: "danger",
                            duration: 5000,
                            buttonTextStyle: { color: "#dc3545" },
                            buttonStyle: { backgroundColor: "white" }
                        });
                    } else if (error.response.data.error === 'FAIL_TO_SEND_ACTIVE_EMAIL') {
                        Toast.show({
                            text: "Gửi mail kích hoạt tài khoản thất bại",
                            buttonText: "Ok",
                            type: "danger",
                            duration: 5000,
                            buttonTextStyle: { color: "#dc3545" },
                            buttonStyle: { backgroundColor: "white" }
                        });
                    } else if (error.response.data.error === 'EMAIL_OR_USERNAME_EXISTED') {
                        Toast.show({
                            text: "Tên tài khoản hoặc email đã tồn tại",
                            buttonText: "Ok",
                            type: "danger",
                            duration: 5000,
                            buttonTextStyle: { color: "#dc3545" },
                            buttonStyle: { backgroundColor: "white" }
                        });
                    } else {
                        Toast.show({
                            text: "Đăng ký tài khoản thất bại",
                            buttonText: "Ok",
                            type: "danger",
                            duration: 5000,
                            buttonTextStyle: { color: "#dc3545" },
                            buttonStyle: { backgroundColor: "white" }
                        });
                    }
                });
            } else {
                Toast.show({
                    text: "2 mật khẩu không trùng khớp",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }
        } else {
            Toast.show({
                text: "Vui lòng nhập đầy đủ thông tin",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }

    }

    render() {
        // const headerHeight = Header.HEIGHT;
        const statusBarHeight = StatusBar.currentHeight;
        const heightToPush = Platform.OS === 'ios' ? 64 : 107 + statusBarHeight;

        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Đăng ký tài khoản</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.addAccount()}>
                            <Icon style={{color: '#fff', fontSize: 37, fontWeight: 'bold'}} ios='ios-checkmark' android="md-checkmark"/>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <ScrollView keyboardDismissMode="on-drag"
                                keyboardShouldPersistTaps="always">
                        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={heightToPush} enabled>
                            <View>
                                <Item floatingLabel>
                                    <Label>Tên công ty (SHOP)</Label>
                                    <Input onChangeText={this.getCompany}
                                           value={this.state.company}/>
                                </Item>
                                <Item floatingLabel style={styles.item}>
                                    <Label>Họ và tên đệm</Label>
                                    <Input onChangeText={this.getLastName}
                                           value={this.state.last_name}/>
                                </Item>
                                <Item floatingLabel style={styles.item}>
                                    <Label>Tên</Label>
                                    <Input onChangeText={this.getFirstName}
                                           value={this.state.first_name}/>
                                </Item>
                                <Item floatingLabel style={styles.item}>
                                    <Label>Tên tài khoản / Email</Label>
                                    <Input onChangeText={this.getUserName}
                                           value={this.state.username}/>
                                </Item>
                                <Item floatingLabel style={styles.item}>
                                    <Label>Mật khẩu</Label>
                                    <Input secureTextEntry={true}
                                           onChangeText={this.getPassword}
                                           value={this.state.password}/>
                                </Item>
                                <Item floatingLabel style={styles.item}>
                                    <Label>Nhập lại mật khẩu</Label>
                                    <Input secureTextEntry={true}
                                           onChangeText={this.getRePassword}
                                           value={this.state.re_password}/>
                                </Item>
                                <Button block info style={styles.item} onPress={() => this.addAccount()}>
                                    <Text>Đăng ký</Text>
                                </Button>
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 5 - 5,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) * 3 / 10 + 5,
    },
    txtLabel: {
        color: '#d3d3d3',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    checkbox02: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});
