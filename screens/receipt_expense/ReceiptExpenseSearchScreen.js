import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Thumbnail, DatePicker, Input, Item, Footer, FooterTab
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View, RefreshControl, TouchableOpacity} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";

import Setting from "../../constants/Setting";
import _ from 'lodash';
import {getReceiptExpenseList} from "../../apis/receiptExpense";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class NotificationSearchScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        let today = new Date();
        let string_first_day = today.getFullYear() + '-' + ((today.getMonth() + 1) < 10 ? '0' : '') + (today.getMonth() + 1) + '-01';
        let string_last_day = today.getFullYear() + '-' + ((today.getMonth() + 1) < 10 ? '0' : '') + (today.getMonth() + 1) + '-' + new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
        this.state = {
            dateFrom: new Date(string_first_day),
            dateTo: new Date(string_last_day),
            keyword: '',
            receiptExpenseList: [],
            isFetching: false,
            isSearch: false,
            total_money: 0,
        };
        this.setDateFrom = this.setDateFrom.bind(this);
        this.setDateTo = this.setDateTo.bind(this);
    }

    setDateFrom(newDate) {
        this.setState({
            dateFrom: newDate
        });
    }

    setDateTo(newDate) {
        this.setState({
            dateTo: newDate
        });
    }

    getFirstDayOfMonth() {
        let date = new Date();
        return '01/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getLastDayOfMonth() {
        let date = new Date();
        return (new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getKeyword = (text) => {
        this.setState({ keyword: text });
    };

    convertDateObjectToString(date) {
        let d = date.getDate();
        let m = (date.getMonth() + 1);
        return date.getFullYear() + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d;
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getReceiptExpenseList(this.state.keyword, this.convertDateObjectToString(this.state.dateFrom), this.convertDateObjectToString(this.state.dateTo), (this.page - 1) * this.itemPerPage, this.itemPerPage).then(response => {
                // alert(this.convertDateObjectToString(this.state.dateFrom));
                // alert(JSON.stringify(response.data.data));
                let newData = [];
                _.forEach(response.data.data, item => {
                    let isExist = _.find(this.state.receiptExpenseList, {
                        id: item.id
                    });
                    if (!isExist) {
                        newData.push(item)
                    }
                });
                this.setState({
                    receiptExpenseList: this.state.receiptExpenseList.concat(newData),
                    total_money: parseInt(this.state.total_money) + parseInt(response.data.total_money),
                    isFetching: false,
                    allLoaded: response.data.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    receiptExpenseList: [],
                    total_money: 0,
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        this.page = 1;
        this.setState({
            receiptExpenseList: [],
            total_money: 0,
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text>Mã phiếu:   <Text style={{fontWeight: 'bold', color: Colors.tintColor, paddingLeft: 50,}}>PC{item.bill_number}</Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text>Giá trị:   <Text style={{fontWeight: 'bold', color: Colors.tintColor, paddingLeft: 50,}}>{item.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                        </Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.txtBold}>Nội dung:</Text>
                    <Text style={{textAlign: 'left'}}>{item.note} </Text>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Thời gian:</Text>   <Text style={{paddingLeft: 50,}}>{this.timeConverter(item.bill_date)} </Text>
                        </Text>
                    </View>
                    <View style={styles.item_col2}>
                        <Text><Text style={styles.txtBold}>Ngày tạo:</Text>   <Text style={{paddingLeft: 50,}}>{this.timestampConverter(item.created_time)} </Text>
                        </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Người tạo:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right'}}>{item.created_full_name} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Người nhận:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right'}}>{item.received_full_name} </Text>
                    </View>
                </View>
                <View style={styles.item_child}>
                    <View style={styles.item_col3}>
                        <Text style={styles.txtBold}>Trạng thái:</Text>
                    </View>
                    <View style={styles.item_col7}>
                        <Text style={{textAlign: 'right', color: Colors.tintColor, fontWeight: 'bold'}}>{this.checkStatus(item.payment_type)} </Text>
                    </View>
                </View>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không tìm thấy thu chi phù hợp!</Text>
            </View>
        );
    };

    checkStatus(id) {
        if (id == 0) {
            return 'Chờ thanh toán'
        } else if(id == 1) {
            return 'Tiền mặt'
        } else if(id == 2) {
            return 'Chuyển khoản'
        } else {
            return 'Thẻ'
        }
    }

    timeConverter(date_string) {
        let day = date_string.split("-");
        return day[2] + '/' + day[1] + '/' + day[0];
    }

    timestampConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    getReceiptExpenseSearch() {
        // alert(this.state.dateFrom.getTime());
        this.setState({
            isSearch: true,
        });
        this.loadData();
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Tìm kiếm thu chi</Title>
                    </Body>
                    <Right>
                        <Button transparent
                                onPress={() => this.getReceiptExpenseSearch()}
                        >
                            <Text style={{color: '#fff'}}>Xem</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                             <RefreshControl
                                 onRefresh={this.onRefresh.bind(this)}
                                 refreshing={this.state.isFetching}
                                 progressBackgroundColor={'#fff'}
                             />
                         }
                >
                    <View style={styles.keyword}>
                        <Item bordered>
                            <Icon active name='search' />
                            <Input placeholder='Nhập từ khóa tìm kiếm'
                                   onChangeText={this.getKeyword}
                                   value={this.state.keyword}/>
                        </Item>
                    </View>
                    <View style={styles.date}>
                        <View style={styles.dateFrom}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Từ ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getFirstDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateFrom}
                                    disabled={false}
                                />
                            </View>

                        </View>
                        <View style={styles.dateTo}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Đến ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getLastDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateTo}
                                    disabled={false}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={styles.reminder_list}>
                        {this.state.isSearch ?
                            <FlatList
                                data={this.state.receiptExpenseList}
                                renderItem={this.renderItem}
                                extraData={this.state.receiptExpenseList}
                                keyExtractor={this.extractItemKey}
                                ListEmptyComponent={this.ListEmpty}
                                onEndReached={() => this.loadMore()}
                                onEndReachedThreshold={0.4}
                            />
                            : null }
                    </View>

                </Content>
                <View>
                    <Footer>
                        <FooterTab style={{backgroundColor: '#2a81ab', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{color: '#fff', fontSize: 22, fontWeight: 'bold'}}>Tổng: {this.state.total_money ? this.state.total_money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0} đ</Text>
                        </FooterTab>
                    </Footer>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    item_child: {
        flexDirection: 'row',
    },
    item_col2: {
        width:( SCREEN_WIDTH - 35)/2,
    },
    item_col3: {
        width:( SCREEN_WIDTH - 35)*0.3,
    },
    item_col7: {
        width:( SCREEN_WIDTH - 35)*0.7,
    },
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 5 - 5,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) * 3 / 10 + 5,
    },
    txtLabel: {
        color: '#d3d3d3',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#f0ad4e',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    reminder_list: {
        backgroundColor: '#F7F7F7',
        flex: 1,
        marginTop: 10,
    },
    item: {
        padding: 10,
        backgroundColor: '#f1eee2',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

