import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Toast, Item, Input, Picker
} from "native-base";
import {Platform, StatusBar, StyleSheet, View} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {getEmployee, getHRMShiftList, registerHRMShift, saveHRMShift} from "../../apis/hrm";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;

export default class ShiftRegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedItems: [],
            staffList: [],
            shifts: [],
            shift_id: 0,
        };
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
    };


    getShift(value) {
        this.setState({
            shift_id: value
        });
    }

    async componentDidMount() {
        await this.loadData();
        await this.loadShift();
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getEmployee(0, 1000).then(employee => {
                let newData = [];
                let datas = {};
                datas.name = 'Danh sách nhân viên';
                datas.id = 0;
                _.forEach(employee.data, item => {
                    let itemStaff = {};
                    itemStaff.name = item.first_name + ' ' + item.last_name;
                    itemStaff.id = item.id;
                    newData.push(itemStaff);
                });
                datas.children = newData;
                let staffs = [];
                staffs.push(datas);
                // alert(JSON.stringify(staffs));
                this.setState({
                    staffList: staffs,
                    employeeList: employee.data,
                    isFetching: false,
                    allLoaded: employee.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    staffList: [],
                    employeeList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    loadShift() {
        this.setState({
            isFetching: true
        }, () => {
            getHRMShiftList().then(response => {
                // alert(JSON.stringify(response.data));
                let newData = [];
                _.forEach(response.data, shift => {
                    let isExist = _.find(this.state.shifts, {
                        id: shift.id
                    });
                    if (!isExist) {
                        newData.push(shift)
                    }
                });
                this.setState({
                    shifts: this.state.shifts.concat(newData),
                    isFetching: false,
                    allLoaded: response.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    shifts: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    getSelectedStaff() {
        this.setState({ selectedItems: this.state.selectedItems })
    }

    addShiftRegister() {
        let dataHRMShift = {
            shift_id: this.state.shift_id,
            selectedItems: JSON.stringify(this.state.selectedItems),
        };
        console.log(dataHRMShift);
        if (this.state.selectedItems.length === 0) {
            Toast.show({
                text: "Vui lòng chọn nhân viên",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        } else {
            registerHRMShift(dataHRMShift).then((response) => {
                Toast.show({
                    text: "Đăng ký ca làm việc thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                // console.log(error);
                Toast.show({
                    text: "Đăng ký ca làm việc thất bại",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            })
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Đăng ký ca làm việc</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.addShiftRegister()}>
                            <Text style={{color: '#fff'}}>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <View style={styles.item}>
                        <Text style={styles.txtBold}>Chọn nhân viên</Text>
                        <View>
                            <SectionedMultiSelect
                                items={this.state.staffList}
                                ref={SectionedMultiSelect => this.SectionedMultiSelect = SectionedMultiSelect}
                                uniqueKey="id"
                                subKey="children"
                                selectText="Áp dụng ca cho nhân viên"
                                confirmText="Áp dụng"
                                showDropDowns={false}
                                readOnlyHeadings={true}
                                hideSearch={true}
                                showCancelButton={true}
                                // hideConfirm={true}
                                onCancel={() => this.SectionedMultiSelect._removeAllItems()}
                                // onConfirm={() => this.getSelectedStaff()}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={this.state.selectedItems}
                            />
                        </View>
                        <View>
                            <Text style={styles.txtBold}>Chọn ca làm việc</Text>
                            <Picker
                                mode="dropdown"
                                iosHeader="Ca Làm việc"
                                placeholder="Chọn ca Làm việc"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.shift_id}
                                onValueChange={this.getShift.bind(this)}
                            >
                                {/*<Picker.Item label="Chọn ca làm việc" value="0"/>*/}
                                {this.state.shifts.map((item, key) => {
                                    return (
                                        <Picker.Item label={item.name + ' / ' + item.start_time + ' - ' + item.end_time} key={key} value={item.id} />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    note: {
        color: Colors.tintColor,
        lineHeight: 27,
    },
    item_row: {
        flexDirection: 'row',
    },
    item_col: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    txtLabel: {
        // color: '#d3d3d3',
        fontWeight: 'bold',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginRight: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginLeft: 10,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    checkbox02: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

