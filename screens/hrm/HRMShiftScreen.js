import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Toast,
} from "native-base";
import {
    Platform,
    StatusBar,
    StyleSheet,
    FlatList,
    View,
    TouchableOpacity,
    RefreshControl,
    Alert
} from 'react-native'
import Dimension from "../../constants/Dimension";
import _ from 'lodash';
import SideBar from "../../components/SideBar";
import {deleteHRMShift, getHRMShiftList} from "../../apis/hrm";
import Colors from "../../constants/Colors";

const SCREEN_WIDTH = Dimension.window.width;

export default class HRMShiftScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            shifts: [],
        }
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    async componentDidMount() {
        await this.loadData();
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getHRMShiftList().then(response => {
                // alert(JSON.stringify(response.data));
                let newData = [];
                _.forEach(response.data, shift => {
                    let isExist = _.find(this.state.shifts, {
                        id: shift.id
                    });
                    if (!isExist) {
                        newData.push(shift)
                    }
                });
                this.setState({
                    shifts: this.state.shifts.concat(newData),
                    isFetching: false,
                    allLoaded: response.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    shifts: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        this.setState({
            shifts: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    getShiftDetail = (item) => {
        this.props.navigation.navigate('ShiftEdit', {
            shiftData: item,
            refreshHRMShift: this.onRefresh,
        });
    };

    deleteShifConfirm(id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa ca làm việc này?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.deleteShift(id)
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    deleteShift(id) {
        deleteHRMShift(id).then(response => {
            this.removeItemFromList(id);
            Toast.show({
                text: "Xóa ca làm việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });
        }).catch(error => {
            Toast.show({
                text: "Xóa ca làm việc thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        });
    }

    removeItemFromList = (rid) => {
        // let newArray = [];
        let newArray = this.state.shifts.filter(function( item ) {
            return item.id !== rid;
        });
        this.setState({
            shifts: newArray,
        });
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.getShiftDetail(item)}>
                    <View style={styles.switch_type}>
                        <View style={styles.type}>
                            <Text style={styles.txtName}>{item.name}</Text>
                        </View>
                        <View style={styles.status_switch}>
                            <Button transparent small onPress={() => this.deleteShifConfirm(item.id)} style={{ alignSelf: 'center', padding: 0}}>
                                <Icon style={{color: '#dc3545'}} ios='ios-trash' android="md-trash"/>
                            </Button>
                        </View>
                    </View>

                    <View style={styles.switch_type}>
                        <View style={styles.hour}>
                            <Text><Text style={styles.txtBold}>Giờ vào: </Text>{item.start_time}</Text>
                        </View>
                        <View style={styles.hour}>
                            <Text> <Text style={styles.txtBold}>Giờ ra: </Text> {item.end_time}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Danh sách ca làm việc trống!</Text>
            </View>
        );
    };

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => { this.drawer = ref; }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                            style={{backgroundColor: '#2a81ab'}}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: '#fff'}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: '#fff'}}>Quản lý ca làm việc</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('ShiftCreate', {
                                refreshHRMShift: this.onRefresh,
                            })}>
                                <Icon style={{color: '#fff'}} ios='ios-add-circle' android="md-add-circle"/>
                            </Button>
                            <Button transparent onPress={() => this.props.navigation.navigate('ShiftRegister')}>
                                <Icon style={{color: '#fff'}} ios='ios-person-add' android="md-person-add"/>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                             refreshControl={
                                 <RefreshControl
                                     onRefresh={this.onRefresh.bind(this)}
                                     refreshing={this.state.isFetching}
                                     progressBackgroundColor={'#fff'}
                                 />
                             }
                    >
                        <View style={styles.content}>
                            <FlatList
                                data={this.state.shifts}
                                renderItem={this.renderItem}
                                extraData={this.state.shifts}
                                keyExtractor={this.extractItemKey}
                                ListEmptyComponent={this.ListEmpty}
                                // onEndReached={() => this.loadMore()}
                                onEndReachedThreshold={0.4}
                            />
                        </View>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtName: {
        fontWeight: 'bold',
        color: Colors.tintColor,
    },
    switch_type: {
        flexDirection: 'row',
    },
    status_switch: {
        width:( SCREEN_WIDTH - 30)*0.2,
    },
    type: {
        width:( SCREEN_WIDTH - 30)*0.8,
    },
    hour: {
        width:( SCREEN_WIDTH - 30)*0.5,
    },
    txtType: {
        width:( SCREEN_WIDTH - 30)*0.4,
        padding: 5,
        backgroundColor: '#5bc0de',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#ffffff',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
});

