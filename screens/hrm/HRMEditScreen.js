import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Textarea,
    CheckBox,
    DatePicker,
    ListItem,
    Item,
    Label,
    Input,
    Picker, Toast
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, KeyboardAvoidingView, Modal, Alert, TouchableOpacity, Image} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {saveCheckList} from "../../apis/checklist";
import {
    getEmployeeDetail,
    getGroupOptionData,
    getImageByUserName,
    getImageList,
    imageDelete, saveHRM, updateHRM,
    updateImages
} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class HRMEditScreen extends Component {
    constructor(props) {
        super(props);
        // let today = new Date();
        this.state = {
            modal: false,
            username: this.props.navigation.getParam('userID') ? this.props.navigation.getParam('userID') : '',
            email: '',
            password: '',
            re_password: '',
            code: '',
            isAdminShop: false,
            isActive: false,
            admin_group: 0,
            is_active: 0,
            last_name: '',
            first_name: '',
            gender: 0,
            date_of_birth: new Date(),
            birth_place: '',
            cmtnd: '',
            ngay_cap: new Date(),
            noi_cap: '',
            phone: '',
            address: '',
            home_address: '',
            description: '',
            salary: '',
            allowance: '',
            department_id: 0,
            position_id: 0,
            ngay_vao_lam: new Date(),
            ngay_hop_dong: new Date(),
            ngay_nghi_viec: new Date(),
            dataDepartment: this.props.navigation.getParam('listDepartment') ? this.props.navigation.getParam('listDepartment') : [],
            dataPosition: this.props.navigation.getParam('listPosition') ? this.props.navigation.getParam('listPosition') : [],
            staffID: this.props.navigation.getParam('staffID') ? this.props.navigation.getParam('staffID') : null,
            isPressData: [],
            store_id: this.props.navigation.getParam('storeID') ? this.props.navigation.getParam('storeID') : '',
            api_key: this.props.navigation.getParam('apiKey') ? this.props.navigation.getParam('apiKey') : '',
            image_by_user_id: [],
            images: this.props.navigation.getParam('imageList') ? this.props.navigation.getParam('imageList') : [],
        };
        this.setDateOfBirth = this.setDateOfBirth.bind(this);
        this.ngayCap = this.ngayCap.bind(this);
        this.ngayVaoLam = this.ngayVaoLam.bind(this);
        this.ngayHopDong = this.ngayHopDong.bind(this);
        this.ngayNghiViec = this.ngayNghiViec.bind(this);
    }

    async componentDidMount() {
        await this.getUserImage();
        await this.getStaffData();
    }

    setModalVisible() {
        this.setState({
            modal: true,
        });
    }

    getUserImage() {
        getImageByUserName(this.state.store_id, this.state.username).then(res => {
            // alert(JSON.stringify(res.data));
            this.setState({
                image_by_user_id: res.data,
            });
        }).catch(err => {
            console.log(err, 'Lỗi khi lấy danh sách ảnh chấm công theo tài khoản')
        });
    }

    convertTimestampToDateObject(UNIX_timestamp) {
        return new Date(UNIX_timestamp*1000);
    }

    timeConverter(UNIX_timestamp) {
        let date = UNIX_timestamp;
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    getStaffData() {
        getEmployeeDetail(this.state.staffID).then(response => {
            // alert(JSON.stringify(response.data));
            let staff = response.data;
            if (staff.admin_group == 1) {
                this.setState({
                    isAdminShop: true,
                })
            }
            if (staff.actived == 1) {
                this.setState({
                    isActive: true,
                })
            }
            this.setState({
                username: staff.user_id,
                email: staff.email,
                password: '',
                re_password: '',
                code: staff.code,
                admin_group: staff.admin_group,
                is_active: staff.actived,
                last_name: staff.last_name,
                first_name: staff.first_name,
                gender: staff.gender.toString(),
                date_of_birth: this.convertTimestampToDateObject(staff.birth_date),
                birth_place: staff.native_country,
                cmtnd: staff.passport_number,
                ngay_cap: this.convertTimestampToDateObject(staff.passport_issue_date),
                noi_cap: staff.passport_issue_place,
                phone: staff.phone_number,
                address: staff.address,
                home_address: staff.home_address,
                description: staff.work_description,
                salary: staff.primary_salary.toString(),
                allowance: staff.allowance.toString(),
                department_id: staff.department_id,
                position_id: staff.job_title_id,
                ngay_vao_lam: this.convertTimestampToDateObject(staff.date_in),
                ngay_hop_dong: this.convertTimestampToDateObject(staff.date_full_time),
                ngay_nghi_viec: this.convertTimestampToDateObject(staff.break_time),
                // department_data: response.data
            })
        }).catch(error => {
            console.log(error, 'Lỗi khi lấy thông tin nhân sự')
        });
    }

    setDateOfBirth(newDate) {
        this.setState({
            date_of_birth: newDate
        });
    }

    ngayCap(newDate) {
        this.setState({
            ngay_cap: newDate
        });
    }

    ngayVaoLam(newDate) {
        this.setState({
            ngay_vao_lam: newDate
        });
    }

    ngayHopDong(newDate) {
        this.setState({
            ngay_hop_dong: newDate
        });
    }

    ngayNghiViec(newDate) {
        this.setState({
            ngay_nghi_viec: newDate
        });
    }

    getFirstDayOfMonth() {
        let date = new Date();
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getLastDayOfMonth() {
        let date = new Date();
        return (new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getDescription = (text) => {
        this.setState({ description: text });
    };

    getAddress = (text) => {
        this.setState({ address: text });
    };

    getHomeAddress = (text) => {
        this.setState({ home_address: text });
    };

    getUsername = (text) => {
        this.setState({ username: text });
    };

    getEmail = (text) => {
        this.setState({ email: text });
    };

    getPassword = (text) => {
        this.setState({ password: text });
    };

    getRetypePassword = (text) => {
        this.setState({ re_password: text });
    };

    getCode = (text) => {
        this.setState({ code: text });
    };

    getLastName = (text) => {
        this.setState({ last_name: text });
    };

    getFirstName = (text) => {
        this.setState({ first_name: text });
    };

    getGender = (text) => {
        this.setState({ gender: text });
    };

    getNoiSinh = (text) => {
        this.setState({ birth_place: text });
    };

    getCMTND = (text) => {
        this.setState({ cmtnd: text });
    };

    getNoiCap = (text) => {
        this.setState({ noi_cap: text });
    };

    getPhone = (text) => {
        this.setState({ phone: text });
    };

    getSalary = (text) => {
        this.setState({ salary: text });
    };

    getAllowance = (text) => {
        this.setState({ allowance: text });
    };

    getDepartment = (text) => {
        this.setState({ department_id: text });
    };

    getPosition = (text) => {
        this.setState({ position_id: text });
    };

    convertDateObjectToTime(date) {
        return Math.round(date.getTime()/1000);
    }

    onAdminCheckBoxPress() {
        // alert(13);
        if (!this.state.isAdminShop) {
            this.setState({
                isAdminShop: true,
                admin_group: 1
            });
        } else {
            this.setState({
                isAdminShop: false,
                admin_group: 0
            });
        }
    }

    onActiveCheckBoxPress() {
        // alert(13);
        if (!this.state.isActive) {
            this.setState({
                isActive: true,
                is_active: 1,
            });
        } else {
            this.setState({
                isActive: false,
                is_active: 0,
            });
        }
    }

    deleteImage(face_id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có muốn xóa ảnh chấm công này?',
            [
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this.deleteImageAI(face_id)},
            ],

        );
    }

    deleteImageAI(face_id) {
        imageDelete(face_id, this.state.store_id).then(response => {
            Toast.show({
                text: "Xóa ảnh chấm công thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });
            this.getUserImage();
        }).catch(error => {
            Toast.show({
                text: "Lỗi khi xóa ảnh chấm công",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        });
    }

    addImage(face_id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có muốn thêm ảnh chấm công này?',
            [
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this.addImageAI(face_id)},
            ],

        );
    }

    addImageAI(face_id) {
        let data = {
            store_id: this.state.store_id,
            api_key: this.state.api_key,
            face_id: face_id,
            username_mapping: this.state.username,
            description: '',
            name: this.state.username,
        };
        // alert(JSON.stringify(data));
        // console.log(data);
        this.setState({
            modal: false,
        });
        updateImages(data).then(response => {
            Toast.show({
                text: "Thêm mới ảnh chấm công thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });
            this.getUserImage();
        }).catch(error => {
            Toast.show({
                text: "Lỗi khi thêm mới ảnh chấm công",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        });
    }

    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    editHRM() {
        let dataHRM = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            re_password: this.state.re_password,
            code: this.state.code,
            admin_group: this.state.admin_group,
            is_active: this.state.is_active,
            last_name: this.state.last_name,
            first_name: this.state.first_name,
            gender: this.state.gender,
            date_of_birth: this.convertDateObjectToTime(this.state.date_of_birth),
            birth_place: this.state.birth_place,
            cmtnd: this.state.cmtnd,
            ngay_cap: this.convertDateObjectToTime(this.state.ngay_cap),
            noi_cap: this.state.ngay_cap,
            phone: this.state.phone,
            address: this.state.address,
            home_address: this.state.home_address,
            description: this.state.description,
            salary: this.state.description,
            allowance: this.state.allowance,
            department_id: this.state.department_id,
            position_id: this.state.position_id,
            ngay_vao_lam: this.convertDateObjectToTime(this.state.ngay_vao_lam),
            ngay_hop_dong: this.convertDateObjectToTime(this.state.ngay_hop_dong),
            ngay_nghi_viec: this.convertDateObjectToTime(this.state.ngay_nghi_viec),
        };
        console.log(dataHRM);
        if (!this.state.username) {
            Toast.show({
                text: "Vui lòng nhập tên tài khoản",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (!this.state.last_name) {
            Toast.show({
                text: "Vui lòng nhập họ và tên đệm",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (!this.state.email) {
            Toast.show({
                text: "Vui lòng nhập email",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        } else {
            if (!this.validateEmail(this.state.email)) {
                Toast.show({
                    text: "Email không đúng định dạng",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }
        }
        if (this.state.password && (this.state.password != this.state.re_password)) {
            Toast.show({
                text: "Hai mật khẩu không trùng khớp",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (!this.state.gender) {
            Toast.show({
                text: "Vui lòng chọn giới tính",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (this.state.department_id == 0) {
            Toast.show({
                text: "Vui lòng chọn phòng ban",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        const { navigation } = this.props;
        if (this.state.department_id != 0 && this.state.last_name && this.state.gender &&
            (this.state.email && this.validateEmail(this.state.email)) &&
            (this.state.password && this.state.password == this.state.re_password)) {
            updateHRM(dataHRM).then((response) => {
                navigation.goBack(null);
                navigation.state.params.refreshHRM();
                Toast.show({
                    text: "Chỉnh sửa nhân viên thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                console.log(error.response.data);
                if (error.response.data.error) {
                    Toast.show({
                        text: error.response.data.error,
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: { color: "#dc3545" },
                        buttonStyle: { backgroundColor: "white" }
                    });
                } else {
                    Toast.show({
                        text: "Chỉnh sửa nhân viên thất bại",
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: { color: "#dc3545" },
                        buttonStyle: { backgroundColor: "white" }
                    });
                }
            })
        }
    }

    render() {
        // const headerHeight = Header.HEIGHT;
        const statusBarHeight = StatusBar.currentHeight;
        const heightToPush = Platform.OS === 'ios' ? 64 : 107 + statusBarHeight;

        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Chỉnh sửa nhân sự</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.editHRM()}>
                            <Text style={{color: '#fff'}}>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <ScrollView keyboardDismissMode="on-drag"
                                keyboardShouldPersistTaps="always">
                        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={heightToPush} enabled>
                            <View style={styles.item}>
                                <View>
                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible();
                                    }}
                                        style={{justifyContent: 'center', flexDirection: 'row', backgroundColor: '#f0ad4e', padding: 10}}>
                                        <Icon style={{ color: '#ffffff'}} ios='ios-images' android="md-images"/>
                                        <Text style={{paddingHorizontal: 10, paddingTop: 5, color: '#ffffff'}}>Thêm ảnh chấm công</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.txtBold}>Ảnh chấm công</Text>
                                    <View style={{flexDirection: 'row', flex: 1, flexWrap: 'wrap'}}>
                                        {this.state.image_by_user_id.map((item, key) => {
                                            return (
                                                <View style={styles.imgWorking1} key={key}>
                                                    <TouchableOpacity key={key} onPress={() => {
                                                        this.deleteImage(item.face_id);
                                                    }}>
                                                        <Image key={key} style={{resizeMode: 'cover', width: 100, height: 100}}
                                                               source={{uri: "https://ai.tuha.vn/api/facial_recognition?face_id=" + item.face_id}}/>
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        })}
                                        {/*<View style={styles.imgWorking1}>*/}
                                            {/*<Image style={styles.imageAI} source={require("../../assets/tuha_logo.png")}/>*/}
                                        {/*</View>*/}
                                        {/*<View style={styles.imgWorking1}>*/}
                                            {/*<Image style={styles.imageAI} source={require("../../assets/tuha_logo.png")}/>*/}
                                        {/*</View>*/}
                                        {/*<View style={styles.imgWorking1}>*/}
                                            {/*<Image style={styles.imageAI} source={require("../../assets/tuha_logo.png")}/>*/}
                                        {/*</View>*/}
                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Tên tài khoản</Text>
                                    <Item>
                                        <Input disabled  placeholder="Nhập tên tài khoản" onChangeText={this.getUsername} value={this.state.username}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Email</Text>
                                    <Item>
                                        <Input placeholder="Nhập tài khoản email" onChangeText={this.getEmail} value={this.state.email}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Mật khẩu</Text>
                                    <Item>
                                        <Input placeholder="Nhập mật khẩu" secureTextEntry={true}
                                               onChangeText={this.getPassword} value={this.state.password}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Nhập lại mật khẩu</Text>
                                    <Item>
                                        <Input placeholder="Nhập lại mật khẩu" secureTextEntry={true}
                                               onChangeText={this.getRetypePassword} value={this.state.re_password}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Mã nhân viên</Text>
                                    <Item>
                                        <Input placeholder="Mã nhân viên (Công ty tự quy định)" onChangeText={this.getCode} value={this.state.code}/>
                                    </Item>
                                </View>
                                <View style={styles.checkbox}>
                                    <View style={styles.checkbox01}>
                                        <ListItem>
                                            <CheckBox checked={this.state.isAdminShop} color="#20a8d8"
                                                      onPress={() => this.onAdminCheckBoxPress()}/>
                                            <Body>
                                            <Text>Admin Shop</Text>
                                            </Body>
                                        </ListItem>
                                    </View>
                                    <View style={styles.checkbox02}>
                                        <ListItem>
                                            <CheckBox checked={this.state.isActive} color="#20a8d8"
                                                      onPress={() => this.onActiveCheckBoxPress()}/>
                                            <Body>
                                            <Text>Kích hoạt</Text>
                                            </Body>
                                        </ListItem>
                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Họ và tên đệm</Text>
                                    <Item>
                                        <Input placeholder="Nhập họ tên đệm" onChangeText={this.getLastName} value={this.state.last_name}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Tên</Text>
                                    <Item>
                                        <Input placeholder="Tên nhân viên" onChangeText={this.getFirstName} value={this.state.first_name}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chọn giới tính</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Phân loại"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.gender}
                                        onValueChange={this.getGender.bind(this)}
                                    >
                                        <Picker.Item label="Chọn" value="0"/>
                                        <Picker.Item label="Nam" value="1"/>
                                        <Picker.Item label="Nữ" value="2"/>
                                    </Picker>
                                </View>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtLabel}>Ngày sinh</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.timeConverter(this.state.date_of_birth)}
                                            textStyle={{color: Colors.tintColor}}
                                            placeHolderTextStyle={{color: "#070707"}}
                                            onDateChange={this.setDateOfBirth}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Nơi sinh</Text>
                                    <Item>
                                        <Input placeholder="Nhập nơi sinh" onChangeText={this.getNoiSinh} value={this.state.birth_place}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>CMTND</Text>
                                    <Item>
                                        <Input placeholder="Chứng minh thư nhân dân" onChangeText={this.getCMTND} value={this.state.cmtnd}/>
                                    </Item>
                                </View>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtLabel}>Ngày cấp</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.timeConverter(this.state.ngay_cap)}
                                            textStyle={{color: Colors.tintColor}}
                                            placeHolderTextStyle={{color: "#070707"}}
                                            onDateChange={this.ngayCap}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Nơi cấp</Text>
                                    <Item>
                                        <Input placeholder="Nhập nơi cấp CMTND" onChangeText={this.getNoiCap} value={this.state.noi_cap}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Số điện thoại</Text>
                                    <Item>
                                        <Input placeholder="Nhập số điện thoại"
                                               onChangeText={this.getPhone} value={this.state.phone}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chỗ ở</Text>
                                    <Textarea rowSpan={2} bordered placeholder="Chỗ ở hiện tại"
                                              onChangeText={this.getAddress} value={this.state.address}/>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Hộ khẩu</Text>
                                    <Textarea rowSpan={2} bordered placeholder="Nhập hộ khẩu"
                                              onChangeText={this.getHomeAddress} value={this.state.home_address}/>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Mô tả công việc</Text>
                                    <Textarea rowSpan={3} bordered placeholder="Mổ tả công việc..."
                                              onChangeText={this.getDescription} value={this.state.description}/>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Lương thỏa thuận</Text>
                                    <Item>
                                        <Input placeholder="Nhập lương thỏa thuận" onChangeText={this.getSalary}
                                               keyboardType="numeric" value={this.state.salary}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Phụ cấp</Text>
                                    <Item>
                                        <Input placeholder="Nhập phụ cấp" onChangeText={this.getAllowance}
                                               keyboardType="numeric" value={this.state.allowance}/>
                                    </Item>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chọn phòng ban / bộ phận</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Phòng ban / bộ phận"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.department_id}
                                        onValueChange={this.getDepartment.bind(this)}
                                    >
                                        <Picker.Item label="Chọn phòng ban" value="0"/>
                                        {this.state.dataDepartment.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.name} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View>
                                    <Text style={styles.txtBold}>Chức vụ</Text>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader="Chọn chức vụ"
                                        iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                        style={{width: undefined}}
                                        selectedValue={this.state.position_id}
                                        onValueChange={this.getPosition.bind(this)}
                                    >
                                        <Picker.Item label="Chọn" value="0"/>
                                        {this.state.dataPosition.map((item, key) => {
                                            return (
                                                <Picker.Item label={item.name_1} key={key} value={item.id} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtLabel}>Ngày vào làm</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.timeConverter(this.state.ngay_vao_lam)}
                                            textStyle={{color: Colors.tintColor}}
                                            placeHolderTextStyle={{color: "#070707"}}
                                            onDateChange={this.ngayVaoLam}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtLabel}>Ngày hợp đồng</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.timeConverter(this.state.ngay_hop_dong)}
                                            textStyle={{color: Colors.tintColor}}
                                            placeHolderTextStyle={{color: "#070707"}}
                                            onDateChange={this.ngayHopDong}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtLabel}>Ngày nghỉ việc</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={new Date()}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.timeConverter(this.state.ngay_nghi_viec)}
                                            textStyle={{color: Colors.tintColor}}
                                            placeHolderTextStyle={{color: "#070707"}}
                                            onDateChange={this.ngayNghiViec}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                            </View>
                            <Modal
                                animationType="slide"
                                onRequestClose={() => {
                                    Alert.alert(
                                        'Tuha Work',
                                        'Hủy thêm mới ảnh chấm công?',
                                        [
                                            {
                                                text: 'Hủy',
                                                onPress: () => console.log('Hủy'),
                                                style: 'cancel',
                                            },
                                            {text: 'OK', onPress: () => this.setState({modal:false})},
                                        ],

                                    );
                                }}
                                // transparent={true}
                                visible={this.state.modal}>
                                <View style={styles.modalDialog}>

                                    <View style={styles.modalHeader}>
                                        <View style={{width: SCREEN_WIDTH * 0.8}}>
                                            <Text style={styles.modalTitle}>Danh sách ảnh chấm công</Text>
                                        </View>
                                        <View style={{width: SCREEN_WIDTH * 0.2}}>
                                            <Button transparent style={{marginTop: 7,}}
                                                    onPress={() => this.setState({modal: false})}>
                                                <Icon style={{color: '#dc3545'}} ios='ios-close-circle' android="md-close-circle"/>
                                            </Button>
                                        </View>
                                    </View>
                                    <ScrollView>
                                        <View style={styles.modalContent}>
                                            <View style={{flexDirection: 'row', flex: 1, flexWrap: 'wrap'}}>
                                                {this.state.images.map((item, key) => {
                                                    return (
                                                        <View style={styles.imgWorking} key={key}>
                                                            <TouchableOpacity key={key} onPress={() => {
                                                                this.addImage(item.face_id, item.store_id);
                                                            }}>
                                                                <Image key={key} style={{resizeMode: 'cover', width: 100, height: 100}}
                                                                       source={{uri: "https://ai.tuha.vn/api/facial_recognition?face_id=" + item.face_id}}/>
                                                            </TouchableOpacity>
                                                        </View>
                                                    )
                                                })}
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </Modal>
                        </KeyboardAvoidingView>
                    </ScrollView>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalContent: {
        // flexDirection: 'row',
        paddingBottom: 107,
    },
    imageAI: {
       // paddingVertical: 5,
       //  width: (SCREEN_WIDTH / 3) - 20,
        // height: 400,
    },
    imgWorking1: {
        width: (SCREEN_WIDTH / 3) - 20,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        borderWidth: 0.7,
        borderColor: 'lightgrey',
        paddingVertical: 5,
    },
    imgWorking: {
        width: (SCREEN_WIDTH / 3) - 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        borderWidth: 0.7,
        borderColor: 'lightgrey',
        paddingVertical: 5,
    },
    imgWorkingPress: {
        width: (SCREEN_WIDTH / 3) - 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        borderWidth: 1.7,
        borderColor: Colors.tintColor,
        paddingVertical: 5,
    },
    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div_note: {
        width:( SCREEN_WIDTH - 60)/2,
        marginRight: 10,
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: Colors.tintColor,
        paddingVertical: 17,
    },
    modalHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row',
    },
    modalBottom: {
        borderTopWidth: 0.5,
        borderTopColor: 'lightgrey',
    },
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    txtLabel: {
        // color: '#d3d3d3',
        // fontSize: 15,
        paddingLeft: 5,
        fontWeight: 'bold',
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        // width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    checkbox02: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

