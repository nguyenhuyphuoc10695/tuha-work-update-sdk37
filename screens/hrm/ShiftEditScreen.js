import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Textarea,
    CheckBox,
    DatePicker,
    ListItem, Toast, Item, Input, Picker
} from "native-base";
import {Platform, StatusBar, StyleSheet, View} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {editReminder, saveReminder} from "../../apis/notification";
import {updateHRMShift} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class ShiftEditScreen extends Component {
    constructor(props) {
        super(props);
        this.itemShiftData = this.props.navigation.getParam('shiftData') ? this.props.navigation.getParam('shiftData') : [];
        this.state = {
            start_time: this.itemShiftData.start_time.substring(0, 5),
            end_time: this.itemShiftData.end_time.substring(0, 5),
            name: this.itemShiftData.name,
            check_in_before: this.itemShiftData.check_in_before,
            check_out_after: this.itemShiftData.check_out_after,
            he_so: this.itemShiftData.he_so.toString(),
            hour_per_day: this.itemShiftData.hour_per_day.toString(),
        };
    }

    getName = (text) => {
        this.setState({ name: text });
    };

    getStartTime = (text) => {
        this.setState({ start_time: text });
    };

    getEndTime = (text) => {
        this.setState({ end_time: text });
    };

    getCheckoutBefore = (text) => {
        this.setState({ check_in_before: text });
    };

    getCheckoutAfter = (text) => {
        this.setState({ check_out_after: text });
    };

    getHeso = (text) => {
        this.setState({ he_so: text });
    };

    getHourPerDay = (text) => {
        this.setState({ hour_per_day: text });
    };

    updateShift() {
        let dataHRMShift = {
            start_time: this.state.start_time + ':00',
            end_time: this.state.end_time + ':00',
            name: this.state.name,
            check_in_before: this.state.check_in_before,
            check_out_after: this.state.check_out_after,
            he_so: this.state.he_so,
            hour_per_day: this.state.hour_per_day,
            id: this.itemShiftData.id,
        };
        const { navigation } = this.props;
        if (!this.state.name) {
            Toast.show({
                text: "Vui lòng nhập tên ca làm việc",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (!this.state.start_time) {
            Toast.show({
                text: "Vui lòng nhập giờ vào",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (!this.state.end_time) {
            Toast.show({
                text: "Vui lòng nhập giờ ra",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }
        if (this.state.name && this.state.start_time && this.state.end_time) {
            updateHRMShift(dataHRMShift).then((response) => {
                navigation.goBack(null);
                navigation.state.params.refreshHRMShift();
                Toast.show({
                    text: "Chỉnh sửa ca làm việc thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                // console.log(error);
                Toast.show({
                    text: "Chỉnh sửa ca làm việc thất bại",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            })
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Chỉnh sửa ca làm việc</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.updateShift()}>
                            <Text style={{color: '#fff'}}>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <View style={styles.item}>
                        <View>
                            <Text style={styles.txtBold}>Tên ca làm việc</Text>
                            <Item>
                                <Input placeholder="Nhập tên ca làm việc" onChangeText={this.getName} value={this.state.name}/>
                            </Item>
                        </View>
                        <View style={styles.item_row}>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Giờ vào</Text>
                                <Item>
                                    <Input placeholder="giờ:phút" onChangeText={this.getStartTime} value={this.state.start_time}/>
                                </Item>
                            </View>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Giờ ra</Text>
                                <Item>
                                    <Input placeholder="giờ:phút" onChangeText={this.getEndTime} value={this.state.end_time}/>
                                </Item>
                            </View>
                        </View>
                        <View style={styles.item_row}>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Vào sau (1)</Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Phân loại"
                                    iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                    style={{width: undefined}}
                                    selectedValue={this.state.check_in_before}
                                    onValueChange={this.getCheckoutBefore.bind(this)}
                                >
                                    <Picker.Item label="1 giờ" value="01:00:00"/>
                                    <Picker.Item label="1.5 giờ" value="01:30:00"/>
                                    <Picker.Item label="2 giờ" value="02:00:00"/>
                                    <Picker.Item label="45 phút" value="00:45:00"/>
                                    <Picker.Item label="30 phút" value="00:30:00"/>
                                    <Picker.Item label="15 phút" value="00:15:00"/>
                                </Picker>
                            </View>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Ra sau (2)</Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Phân loại"
                                    iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                    style={{width: undefined}}
                                    selectedValue={this.state.check_out_after}
                                    onValueChange={this.getCheckoutAfter.bind(this)}
                                >
                                    <Picker.Item label="1 giờ" value="01:00:00"/>
                                    <Picker.Item label="1.5 giờ" value="01:30:00"/>
                                    <Picker.Item label="2 giờ" value="02:00:00"/>
                                    <Picker.Item label="45 phút" value="00:45:00"/>
                                    <Picker.Item label="30 phút" value="00:30:00"/>
                                    <Picker.Item label="15 phút" value="00:15:00"/>
                                </Picker>
                            </View>
                        </View>
                        <View style={styles.item_row}>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Hệ số</Text>
                                <Item>
                                    <Input placeholder="0" onChangeText={this.getHeso} value={this.state.he_so}/>
                                </Item>
                            </View>
                            <View style={styles.item_col}>
                                <Text style={styles.txtBold}>Số giờ</Text>
                                <Item>
                                    <Input placeholder="8" onChangeText={this.getHourPerDay} value={this.state.hour_per_day}/>
                                </Item>
                            </View>
                        </View>
                        <View style={{paddingTop: 17,}}>
                            <Text style={styles.note}>(1) Vào trước: cho phép ghi nhận trước giờ vào bao nhiêu giờ:phút</Text>
                            <Text style={styles.note}>(2) Ra sau: cho phép ghi nhận giờ ra sau bao nhiêu giờ:phút</Text>
                        </View>
                    </View>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    note: {
        color: Colors.tintColor,
        lineHeight: 27,
    },
    item_row: {
        flexDirection: 'row',
    },
    item_col: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    txtLabel: {
        // color: '#d3d3d3',
        fontWeight: 'bold',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginRight: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginLeft: 10,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    checkbox02: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

