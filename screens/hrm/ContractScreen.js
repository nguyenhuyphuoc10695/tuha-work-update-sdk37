import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView} from 'react-native'
import Dimension from "../../constants/Dimension";
import {getContractInfomation} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;

export default class ContractScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staff_info: {},
            group_info: {},
            staffID: this.props.navigation.getParam('staffID') ? this.props.navigation.getParam('staffID') : null,
        };
    }

    async componentDidMount() {
        await this.getContractInfo();
    }

    getContractInfo() {
        getContractInfomation(this.state.staffID).then(response => {
            this.setState({
                staff_info : response.data.staff_info,
                group_info : response.data.group_info,
            });
        }).catch(error => {
            console.log(error, 'Lỗi khi lấy thông tin hợp đồng nhân sự');
        });
    }

    convertDateObjectToTime(date) {
        return Math.round(date.getTime() / 1000);
    }

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    timeConverterNextYear(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear() + 1;
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Hợp đồng lao động</Title>
                    </Body>
                    {/*<Right/>*/}
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <ScrollView>
                        <View style={styles.item}>
                            <Text style={styles.txtCenterBold1}>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</Text>
                            <Text style={styles.txtCenterBold2}>Độc lập – Tự do – Hạnh phúc</Text>
                            <Text style={{textAlign: 'right', marginTop: 20, marginRight: 70,}}>số………………</Text>
                            <Text style={styles.txtRight}>……………,ngày……tháng……năm……</Text>
                            <Text style={styles.txtCenterBold}>HỢP ĐỒNG LAO ĐỘNG</Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtNormal}>Chúng tôi, một bên là Ông/Bà:
                                <Text style={styles.txtUpperCase}> {this.state.group_info.legal_representative}</Text>
                            </Text>
                            <Text style={styles.txtNormal}>Chức vụ:
                                <Text style={styles.txtUpperCase}> GIÁM ĐỐC</Text>
                            </Text>
                            <Text style={styles.txtNormal}>Đại diện cho:
                                <Text style={styles.txtUpperCase}> {this.state.group_info.name}</Text>
                            </Text>
                            <Text style={styles.txtNormal}>
                                Địa chỉ: {this.state.group_info.address}
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtNormal}>Và một bên là Ông/Bà:
                                <Text style={styles.txtUpperCase}> {this.state.staff_info.first_name} {this.state.staff_info.last_name}</Text>
                            </Text>
                            <Text style={styles.txtNormal}>
                                Sinh ngày:
                                {this.state.staff_info.birth_date !=0 ? this.timeConverter(this.state.staff_info.birth_date) : '.................'}
                                Tại: {this.state.staff_info.native_country}
                            </Text>
                            <Text style={styles.txtNormal}>
                                Nghề nghiệp: ……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                Địa chỉ thường trú: {this.state.staff_info.address}
                            </Text>
                            <Text style={styles.txtNormal}>
                                Số CMTND: {this.state.staff_info.passport_number ? this.state.staff_info.passport_number + ' ' : '................. '} cấp ngày
                                {this.state.staff_info.passport_issue_date !=0 ? ' ' + this.timeConverter(this.state.staff_info.passport_issue_date) : '.................'}
                            </Text>
                            <Text style={styles.txtNormal}>
                                Số sổ lao động (nếu có) :………………..……cấp ngày……...…/………./…………….....
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtCenterBold2}>
                                Thỏa thuận ký kết hợp đồng lao động và cam kết làm đúng những điều khoản sau đây :
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 1 : Thời hạn và công việc hợp đồng
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Ông / bà : {this.state.staff_info.first_name + ' '}{this.state.staff_info.last_name + ' '}
                                 làm việc theo loại hợp đồng lao động có thời hạn từ ngày
                                {this.state.staff_info.date_full_time !=0 ?  ' ' + this.timeConverter(this.state.staff_info.date_full_time) + ' ' : '................. '}
                                đến ngày
                                {this.state.staff_info.date_full_time !=0 ? ' ' + this.timeConverterNextYear(this.state.staff_info.date_full_time) : '................. '}
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Thử việc từ ngày
                                {this.state.staff_info.date_in !=0 ? ' ' + this.timeConverter(this.state.staff_info.date_in) + ' ' : '................. '}
                                 đến ngày
                                {this.state.staff_info.date_full_time !=0 ? ' ' + this.timeConverter(this.state.staff_info.date_full_time) : '................. '}
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Địa điểm làm việc: {this.state.group_info.address}
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Chức vụ: {this.state.staff_info.department_name}
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Công việc phải làm:
                            </Text>
                            <View style={{
                                borderWidth: 0.7,
                                borderColor: 'lightgrey',
                                padding: 10,
                                backgroundColor: '#f7f7f7',
                                marginTop: 7,
                            }}>
                                <Text style={styles.txtNormal}>
                                    {this.state.staff_info.work_description}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 2 : Chế độ làm việc
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Thời giờ làm việc :……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Được cấp phát những dụng cụ làm việc gồm :
                                ……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Điều kiện an toàn và vệ sinh lao động tại nơi làm việc theo quy định hiện hành của nhà
                                nước.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 3 : Nghĩa vụ, quyền hạn và các quyền lợi người lao động được hưởng như sau :
                            </Text>
                            <Text style={styles.txtBold}>
                                1 – Nghĩa vụ :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Trong công việc, chịu sự điều hành trực tiếp của ông/bà: {this.state.group_info.legal_representative}
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Hoàn thành những công việc trong hợp đồng lao động.
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Chấp hành nghiêm túc nội quy, quy chế của đơn vị, kỷ luật lao động, an toàn lao động
                                và các quy định trong thỏa ước lao động tập thể.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                2 – Quyền hạn :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Có quyền đề xuất, khiếu nại, thay đổi, tạm hoãn, chấm dứt hợp đồng lao động theo quy
                                định của pháp luật lao động hiện hành.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                3 – Quyền lợi :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Mức lương chính hoặc tiền công:
                                {' ' + parseInt(this.state.staff_info.primary_salary).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' '}.
                                Được trả 1 lần từ ngày mùng 5 đến ngày mùng 10 hàng tháng.
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Phụ cấp gồm :……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Được trang bị gồm :……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Số ngày nghỉ hàng năm được hưởng lương (nghỉ lễ, phép, việc riêng) :
                                ……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Bảo hiểm xã hội:……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Được hưởng các phúc lợi :……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Được hưởng các khoản thưởng, nâng lương, bồi dưỡng nghiệp vụ, thực hiện:
                                ……………………………………………………………………………….......
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Được hưởng các chế độ ngừng việc trợ cấp thôi việc, bồi thường theo quy định của pháp
                                luật lao động
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 4 : Nghĩa vụ và quyền hạn của người sử dụng lao động :
                            </Text>
                            <Text style={styles.txtBold}>
                                1 – Nghĩa vụ :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Thực hiện đầy đủ những điều kiện cần thiết đã cam kết trong hợp đồng lao động để người
                                lao động làm việc đạt hiệu quả. Đảm bảo việc làm cho người lao động theo hợp đồng đã ký.
                                Thanh toán đầy đủ, dứt điểm các chế độ và quyền lợi của người lao động đã cam kết trong
                                hợp đồng lao động.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                2 – Quyền hạn :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Có quyền điều chuyển tạm thời người lao động, tạm ngừng việc, thay đổi, tạm hoãn, chấm
                                dứt hợp đồng lao động và áp dụng các biện pháp kỷ luật theo quy định của pháp luật lao
                                động.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 5 : Điều khoản chung :
                            </Text>
                            <Text style={styles.txtBold}>
                                1 – Những thỏa thuận khác :
                            </Text>
                            <Text style={styles.txtNormal}>
                                ………………………………………………………………………………..........
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                2 – Hợp đồng lao động có hiệu lực
                            </Text>
                            <Text style={styles.txtNormal}>
                                - Từ ngày {this.state.staff_info.date_full_time !=0 ? ' ' + this.timeConverter(this.state.staff_info.date_full_time) + ' ' : '................. '}
                                đến ngày {this.state.staff_info.date_full_time !=0 ? ' ' + this.timeConverterNextYear(this.state.staff_info.date_full_time) : '................. '}.
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.txtBold}>
                                Điều 6 : Hợp đồng lao động này làm thành 02 bản :
                            </Text>
                            <Text style={styles.txtNormal}>
                                - 01 bản do người lao động giữ.
                            </Text>
                            <Text style={styles.txtNormal}>
                                - 01 bản do người sử dụng lao động giữ.
                            </Text>
                            <Text style={styles.txtNormal}>
                                Làm tại : {this.state.group_info.address}
                            </Text>
                        </View>
                        <View style={styles.item}>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{width: SCREEN_WIDTH / 2}}>
                                    <Text style={{fontSize: 13,lineHeight: 27,textAlign: 'center', fontWeight: 'bold'}}>Người lao động</Text>
                                    <Text style={{fontSize: 12,lineHeight: 27,textAlign: 'center'}}>(ký tên)</Text>
                                </View>
                                <View style={{width: SCREEN_WIDTH / 2}}>
                                    <Text style={{fontSize: 13,lineHeight: 27,textAlign: 'center', fontWeight: 'bold'}}>Người sử dụng lao động</Text>
                                    <Text style={{fontSize: 12,lineHeight: 27,textAlign: 'center'}}>(ký tên, đóng dấu)</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtNormal: {
        fontSize: 15,
        lineHeight: 27
    },
    txtUpperCase: {
        fontSize: 15,
        textTransform: 'uppercase',
        paddingTop: 7,
    },
    txtCenterBold: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 37,
    },
    txtCenterBold1: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
    },
    txtCenterBold2: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 17,
    },
    txtRight: {
        textAlign: 'right',
    },
    txtBold: {
        fontWeight: 'bold',
    },
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    txtLabel: {
        // color: '#d3d3d3',
        // fontSize: 15,
        paddingLeft: 5,
        fontWeight: 'bold',
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        // width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    checkbox02: {
        width: (SCREEN_WIDTH - 30) / 2,
    },
    // txtBold: {
    //     fontWeight: 'bold',
    //     marginTop: 10,
    // },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginBottom: 20,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

