import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Thumbnail, Toast
} from "native-base";
import {Platform, StatusBar, StyleSheet, RefreshControl, FlatList, View, Alert} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Setting from "../../constants/Setting";
import {
    deleteHRM,
    getDepartmentList,
    getEmployee,
    getGroupOptionData,
    getImageList,
    getPositionList
} from "../../apis/hrm";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;

export default class HRMScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    state = {
        employeeList: [],
        department_data: [],
        position_data: [],
        isFetching: false,
        store_id: '',
        api_key: '',
        images: [],
    };

    constructor(props) {
        super(props);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

     async componentDidMount() {
         await this.getDepartmentData();
         await this.getPositionData();
         await this.loadData();
         await this.getGroupOption();
    }

    getGroupOption() {
        getGroupOptionData().then(response => {
            let group_option = response.data;
            // alert(JSON.stringify(group_option));
            this.setState({
                store_id: group_option.store_id.value,
                api_key: group_option.api_key.value,
            });
            getImageList(group_option.store_id.value).then(res => {
                // alert(JSON.stringify(res.data));
                this.setState({
                    images: res.data,
                });
            }).catch(err => {
                console.log(err, 'Lỗi khi lấy danh sách ảnh chấm công')
            });
        }).catch(error => {
            console.log(error, 'Lỗi khi lấy thông tin cấu hình nhóm')
        });
    }

    getDepartmentData() {
         getDepartmentList().then(response => {
             // alert(JSON.stringify(response.data));
             this.setState({
                 department_data: response.data
             })
         }).catch(error => {
             this.setState({
                 department_data: []
             });
         });
    }

    getPositionData() {
        getPositionList().then(response => {
            this.setState({
                position_data: response.data
            })
        }).catch(error => {
            this.setState({
                position_data: []
            });
        });
    }

    deleteConfirmStaff(id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa nhân viên này?',
            [
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this.deleteStaff(id)},
            ],

        );
    }

    deleteStaff(id) {
        deleteHRM(id).then(response => {
            Toast.show({
                text: "Xóa nhân viên thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });
        }).catch(error => {
            Toast.show({
                text: "Xóa nhân viên thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        });
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getEmployee((this.page - 1) * this.itemPerPage, this.itemPerPage).then(employee => {
                // alert(JSON.stringify(employee.data));
                let newData = [];
                let datas = this.uintToString(employee.data);
                // alert(JSON.stringify(datas));
                _.forEach(employee.data, item => {
                    let isExist = _.find(this.state.employeeList, {
                        id: item.id
                    });
                    if (!isExist) {
                        newData.push(item)
                    }
                });
                this.setState({
                    employeeList: this.state.employeeList.concat(newData),
                    isFetching: false,
                    allLoaded: employee.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    employeeList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    onRefresh = () => {
        this.page = 1;
        this.setState({
            employeeList: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    uintToString(uintArray) {
        let encodedString = String.fromCharCode.apply(null, uintArray);
        return decodeURIComponent(escape(encodedString));
    }

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <ListItem avatar noBorder >
                    <Left>
                        <Thumbnail square small source={require("../../assets/tuha_logo.png")}/>
                    </Left>
                    <Body>
                    <Text style={styles.name}>{item.full_name}</Text>
                    <Text note  style={styles.department}>Phòng/ban:
                        <Text style={{fontWeight: 'bold', color: '#8a6d3b'}}> {item.department_name}</Text>
                    </Text>
                    </Body>
                    <Right>
                        <Text note style={styles.gender}>Anh/Chị</Text>
                    </Right>
                </ListItem>
                <View style={styles.div01}>
                    <View style={styles.employee_code}>
                        <Text>Mã NV: <Text style={styles.txtBold}> {item.code}</Text></Text>
                    </View>
                    <View style={styles.birthday}>
                        <Text>NS: <Text style={styles.txtBold}> {item.birth_date}</Text></Text>
                    </View>
                </View>
                <View style={styles.div02}>
                    <View style={styles.mobile}>
                        <Text>Mobile: <Text style={styles.txtBold}> {item.phone_number}</Text></Text>
                    </View>
                    <View style={styles.identification}>
                        <Text>CMTND: <Text style={{fontWeight: 'bold', fontSize: 14,}}> {item.passport_number}</Text></Text>
                    </View>
                </View>
                <View style={styles.btnAction}>
                    <View style={styles.btn4}>
                        <Button bordered dark
                                onPress={() => this.props.navigation.navigate('ShiftRegisterStaff', {
                                    staffID: item.id,
                                    staffName: item.first_name + ' ' + item.last_name,
                                })}>
                            <Text>Ca ({item.has_shift})</Text>
                        </Button>
                    </View>
                    <View style={styles.btn4}>
                        <Button bordered dark style={styles.iconCenter}
                                onPress={() => this.props.navigation.navigate('Contract', {
                                    staffID: item.id,
                                })}>
                            <Icon ios='ios-school' android="md-school"/>
                        </Button>
                    </View>
                    <View style={styles.btn4}>
                        <Button bordered dark style={styles.iconCenter}
                        onPress={() => this.props.navigation.navigate('HRMEdit', {
                            staffID: item.id,
                            userID: item.user_id,
                            storeID: this.state.store_id,
                            apiKey: this.state.api_key,
                            imageList: this.state.images,
                            listDepartment: this.state.department_data,
                            listPosition: this.state.position_data,
                            refreshHRM: this.onRefresh,
                        })}>
                            <Icon ios='ios-create' android="md-create"/>
                        </Button>
                    </View>
                    <View style={styles.btn4}>
                        <Button bordered danger style={styles.iconCenter}
                        onPress={() => this.deleteConfirmStaff(item.id)}>
                            <Icon ios='ios-trash' android="md-trash"/>
                        </Button>
                    </View>
                </View>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Danh sách nhân viên trống!</Text>
            </View>
        );
    };

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => { this.drawer = ref; }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab" style={{ backgroundColor: '#2a81ab' }}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: '#fff'}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: '#fff'}}>Quản lý nhân sự</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('HRMCreate', {
                                listDepartment: this.state.department_data,
                                listPosition: this.state.position_data,
                                refreshHRM: this.onRefresh,
                            })}>
                                <Icon style={{color: '#fff'}} ios='ios-add-circle' android="md-add-circle"/>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                            <RefreshControl
                                onRefresh={this.onRefresh.bind(this)}
                                refreshing={this.state.isFetching}
                                progressBackgroundColor={'#fff'}
                            />
                         }
                    >
                        <View style={styles.content}>
                            <FlatList
                                data={this.state.employeeList}
                                renderItem={this.renderItem}
                                extraData={this.state.employeeList}
                                keyExtractor={this.extractItemKey}
                                ListEmptyComponent={this.ListEmpty}
                                onEndReached={() => this.loadMore()}
                                onEndReachedThreshold={0.4}
                            />
                        </View>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    iconCenter: {
        alignSelf: 'center'
    },
    btnAction: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    btn4: {
        width:( SCREEN_WIDTH - 100)/4,
        marginRight: 10,
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    identification: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    birthday: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    mobile: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    employee_code: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
    },
    item: {
        paddingVertical: 10,
        backgroundColor: '#f5fffa',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    name: {
        fontWeight: 'bold',
    },
    gender: {
        fontWeight: 'bold',
    },
    department: {
        color: '#8a6d3b',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});
