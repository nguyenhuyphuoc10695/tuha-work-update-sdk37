import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Toast, Picker
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, TouchableOpacity, Alert} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {
    deleteHRMStaffShift,
    getHRMShiftList,
    getHRMShiftRegisterList,
    saveHRMStaffShift
} from "../../apis/hrm";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;

export default class ShiftRegisterStaffScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staffID: this.props.navigation.getParam('staffID') ? this.props.navigation.getParam('staffID') : null,
            staffName: this.props.navigation.getParam('staffName') ? this.props.navigation.getParam('staffName') : null,
            shift_register: [],
            shifts: [],
            shift_id: 0,
        };
    }

    getShift(value) {
        this.setState({
            shift_id: value
        });
    }

    async componentDidMount() {
        await this.loadDataStaffShift();
        await this.loadShift();
    }

    loadDataStaffShift() {
        getHRMShiftRegisterList(this.state.staffID).then(response => {
            // alert(JSON.stringify(response.data));
            this.setState({
                shift_register: response.data
            })
        }).catch(error => {
            console.log('Lỗi khi lấy danh sách ca theo nhân viên', error)
        });
    }

    loadShift() {
        this.setState({
            isFetching: true
        }, () => {
            getHRMShiftList().then(response => {
                // alert(JSON.stringify(response.data));
                let newData = [];
                _.forEach(response.data, shift => {
                    let isExist = _.find(this.state.shifts, {
                        id: shift.id
                    });
                    if (!isExist) {
                        newData.push(shift)
                    }
                });
                this.setState({
                    shifts: this.state.shifts.concat(newData),
                    isFetching: false,
                    allLoaded: response.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    shifts: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    addStaffShift() {
        let dataHRMShiftStaff = {
            shift_id: this.state.shift_id,
            staff_id: this.state.staffID,
        };
        // alert(JSON.stringify(dataHRMShiftStaff));
        if (this.state.shift_id == 0) {
            Toast.show({
                text: "Vui lòng chọn ca làm việc",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        } else {
            saveHRMStaffShift(dataHRMShiftStaff).then((response) => {
                Toast.show({
                    text: "Thêm mới ca làm việc thành công cho nhân viên " + this.state.staffName,
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
                this.loadDataStaffShift();
            }).catch(error => {
                // console.log(error.response.data);
                Toast.show({
                    text: error.response.data.error,
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            })
        }
    }

    deleteStaffShiftConfirm(id) {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa ca làm việc này?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.deleteStaffShift(id)
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    deleteStaffShift(id) {
        deleteHRMStaffShift(id).then(response => {
            this.loadDataStaffShift();
            Toast.show({
                text: "Xóa đăng ký ca làm việc thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });

        }).catch(error => {
            Toast.show({
                text: "Xoa đăng ký ca thất bại",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        })
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>{this.state.staffName}</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.addStaffShift()}>
                            <Text>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <View style={styles.item}>
                        <Text style={{color: Colors.tintColor}}>
                            Danh sách ca được đăng ký
                        </Text>
                        {this.state.shift_register.map((item, key) => {
                            return (
                                <View style={{flexDirection: 'row', paddingVertical: 7, borderBottomColor: 'lightgrey', borderBottomWidth: 0.7}} key={key}>
                                    <Text style={styles.txtShift}>{key + 1}.{' ' + item.name + ' '}/{' ' + item.start_time + ' '}-{' ' + item.end_time} </Text>
                                    <View style={{width: (SCREEN_WIDTH - 20)*0.2,}}>
                                        <TouchableOpacity onPress={() => this.deleteStaffShiftConfirm(item.id)}>
                                            <Text style={styles.txtDel}>Xóa </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })}
                    </View>
                    <View style={styles.item}>
                        <Text style={{color: Colors.tintColor}}>
                            Thêm mới ca làm việc cho nhân viên
                        </Text>
                        <View>
                            <Picker
                                mode="dropdown"
                                iosHeader="Ca Làm việc"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                style={{width: undefined}}
                                selectedValue={this.state.shift_id}
                                onValueChange={this.getShift.bind(this)}
                            >
                                <Picker.Item label="Chọn ca làm việc" value="0"/>
                                {this.state.shifts.map((item, key) => {
                                    return (
                                        <Picker.Item label={item.name + ' / ' + item.start_time + ' - ' + item.end_time} key={key} value={item.id} />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtShift: {
        fontWeight: 'bold',
        width: (SCREEN_WIDTH - 20)*0.8,
        fontSize: 16,
    },
    txtDel: {
        fontWeight: 'bold',
        color: '#dc3545',
        textAlign: 'right',
    },
    note: {
        color: Colors.tintColor,
        lineHeight: 27,
    },
    item_row: {
        flexDirection: 'row',
    },
    item_col: {
        width: (SCREEN_WIDTH - 20) / 2,
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    txtLabel: {
        // color: '#d3d3d3',
        fontWeight: 'bold',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginRight: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginLeft: 10,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    checkbox02: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

