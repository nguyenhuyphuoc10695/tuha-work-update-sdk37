import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer, DatePicker, Picker, Toast, Textarea
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, TouchableOpacity, Modal, Alert, Image} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import _ from 'lodash';
import {getEmployee, getTimeSheetStaffData, saveActionLeave} from "../../apis/hrm";
import {getUserInfo, isAdminRole} from "../../apis/authenication";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class TimeSheetScreen extends Component {

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            selectedDate: null,
            date_selected: new Date(),
            month_selected: today.getMonth() + 1,
            year_selected: today.getFullYear(),
            timeSheetData: {},
            employeeList: [],
            staff_id: '',
            current_staff_id: 0,
            staff_name: '',
            total_point: 0,
            total_missing: 0,
            total_late: 0,
            total_hour_of_month: 0,
            total_working_day: 0,
            annual_leave_remaining_point: 0,
            modal: false,
            dateOff: today.getDate()  + "/" + (today.getMonth() + 1) + "/" + today.getFullYear(),
            dateOffTimestamp: 0,
            startDate: new Date(),
            endDate: new Date(),
            message: '',
            isAdmin: false,
        };
        this.setDate = this.setDate.bind(this);
        this.setStartDate = this.setStartDate.bind(this);
        this.setEndDate = this.setEndDate.bind(this);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    checkRoleAdmin() {
        let role = 'EMPLOYEE_ADMIN';
        isAdminRole(role).then(response => {
            if (response.data == 1) {
                this.setState({
                    isAdmin: true,
                });
            } else {
                this.setState({
                    isAdmin: false,
                });
            }
        }).catch(error => {
            console.log('lỗi kiểm tra vai trò admin', error.response.data)
        });
    }

    setStartDate(newDate) {
        this.setState({
            startDate: newDate
        });
    }

    setEndDate(newDate) {
        this.setState({
            endDate: newDate
        });
    }

    getMessage = (text) => {
        this.setState({ message: text });
    };

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    convertDateObjectToString1(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        return [(d > 9 ? '' : '0') + d, (m > 9 ? '' : '0') + m, y].join('/');
    }

    convertDateObjectToTime(date) {
        return Math.round(date.getTime()/1000);
    }

    convertTimeStampToDateObject(unix_timestamp) {
        return new Date(unix_timestamp *1000);
    }

    setDate(newDate) {
        this.setState({
            date_selected: newDate,
            month_selected: newDate.getMonth() + 1,
            year_selected: newDate.getFullYear(),
        });
        this.loadData(newDate, this.state.staff_id);
    }

    getStaff(value) {
        this.setState({
            staff_id: value
        });
        this.loadData(this.state.date_selected, value);
    }

    loadData(date, staff_id) {
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        // alert(staff_id);
        getTimeSheetStaffData(month, year, staff_id).then((response) => {
            // alert(JSON.stringify(response.data.dates));
            this.setState({
                timeSheetData: response.data.dates,
                staff_name: response.data.full_name,
                total_point: response.data.total_point,
                total_missing: response.data.total_missing,
                total_late: response.data.total_late,
                total_hour_of_month: response.data.total_hour_of_month,
                total_working_day: response.data.total_working_day,
                annual_leave_remaining_point: response.data.annual_leave_remaining,
            });
        }).catch((error) => {
            console.log('lỗi khi load bảng chấm công', error.response.data)
        });
    }

    async componentDidMount() {
        await this.checkRoleAdmin();
        await this.loadData(new Date(), 0);
        await this.loadDataStaff();
        await this.loadStaffInfo();
    }

    onRefresh = () => {
        this.loadData(this.state.date_selected, this.state.staff_id);
    };

    loadStaffInfo() {
        getUserInfo().then(response => {
            // alert(JSON.stringify(response.data))
            this.setState({
                staff_id: response.data.employee_id,
                current_staff_id: response.data.employee_id,
                staff_name: response.data.employee_fullname,
            })
        }).catch(error => {
            console.log('Lỗi khi lấy thông tin tài khoản', error.response.data);
        })
    }

    loadDataStaff() {
        this.setState({
            isFetching: true
        }, () => {
            getEmployee(0, 1000).then(employee => {
                // alert(JSON.stringify(employee.data));
                let newData = [];
                _.forEach(employee.data, item => {
                    let isExist = _.find(this.state.employeeList, {
                        id: item.id
                    });
                    if (!isExist) {
                        newData.push(item)
                    }
                });
                this.setState({
                    employeeList: this.state.employeeList.concat(newData),
                    isFetching: false,
                    allLoaded: employee.data.length == 0 ? true : false
                });
            }).catch(err => {
                this.setState({
                    employeeList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    dateAction(datetime) {
        // alert(123);
        let now = new Date();
        let startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        let timestamp = Math.floor(startOfDay / 1000);
        let date_onpress = this.timeConverter(datetime);
        // let date_now = now.getDate()  + "/" + (now.getMonth() + 1) + "/" + now.getFullYear();
        // console.log(this.state.staff_id , this.state.current_staff_id);
        if (((datetime >= timestamp) && (this.state.staff_id == this.state.current_staff_id)) || this.state.isAdmin) {
            this.setState({
                modal: true,
                dateOff: date_onpress,
                dateOffTimestamp: datetime,
                startDate: this.convertTimeStampToDateObject(datetime),
                endDate: this.convertTimeStampToDateObject(datetime),
            });
        }
    }

    dateActionAdmin(datetime, item) {
        if (this.state.isAdmin) {
            if (item.annual_leave == 0) {
                this.props.navigation.navigate('EditTimeSheet', {
                    dateChosen: datetime,
                    itemTimeSheet: item,
                    staffName: this.state.staff_name,
                    refreshTimeSheet: this.onRefresh,
                })
            } else {
                this.props.navigation.navigate('EditBreakTime', {
                    dateChosen: datetime,
                    itemTimeSheet: item,
                    staffName: this.state.staff_name,
                    refreshTimeSheet: this.onRefresh,
                })
            }
        }
    }

    actionLeave() {
        let start_time = this.convertDateObjectToTime(this.state.startDate);
        let finish_time = this.convertDateObjectToTime(this.state.endDate);
        if (start_time < this.state.dateOffTimestamp) {
            Toast.show({
                text: "Ngày bắt đầu không được trước ngày được chọn",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        if (start_time > finish_time) {
            Toast.show({
                text: "Ngày kết thúc phải sau ngày bắt đầu",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        if (start_time >= this.state.dateOffTimestamp && start_time <= finish_time) {
            let data = {
                start_date: start_time,
                end_date: finish_time,
                reason: this.state.message,
                staff_id: this.state.staff_id,
            };
            // alert(JSON.stringify(data))
            saveActionLeave(data).then(response => {
                this.setState({
                    modal: false,
                });
                Toast.show({
                    text: "Gửi đề xuất xin nghỉ thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                Toast.show({
                    text: error.response.data.error,
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: {color: "#dc3545"},
                    buttonStyle: {backgroundColor: "white"}
                });
            })
        }
    }

    getDateBackground(day) {
        let today = new Date();
        let d = today.getDate() < 10 ? '0' : '' + today.getDate();
        if (day.name == d) {
            return '#47BDE2';
        } else {
            return day.bg_color
        }
    }

    render() {
        const data = Object.keys(this.state.timeSheetData).map((key, index) => {
            return (
                <View style={styles.row} key={key}>
                    <View style={[styles.col3, {backgroundColor: this.getDateBackground(this.state.timeSheetData[key])}]}>
                        <Text style={styles.thead}>{this.state.timeSheetData[key].name}</Text>
                    </View>
                    <View style={styles.col7}>
                        {this.state.timeSheetData[key].tasks.length ?
                            this.state.timeSheetData[key].tasks.map((item, k) => {
                                return (
                                    <TouchableOpacity onPress={() => this.dateActionAdmin(key, item)} key={k}>
                                        <Text key={k} style={[styles.success, {backgroundColor: item.color}]}>
                                            {item.real_hour}
                                        </Text>
                                    </TouchableOpacity>
                                )
                            })
                            :
                            <TouchableOpacity onPress={() => this.dateAction(key)}>
                                <Text style={[styles.success, {backgroundColor: '#fff'}]}>0</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            )
        });
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => {
                    this.drawer = ref;
                }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                            style={{backgroundColor: '#2a81ab'}}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: "#fff"}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: "#fff"}}>Bảng giờ làm</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('TimeSheetRank')}>
                                <Text style={{color: '#fff'}}>Xếp hạng</Text>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content}>
                        <ScrollView>
                            <View style={styles.date}>
                                <View style={styles.txtDate}>
                                    <Text style={styles.txtBold}>Chọn 1 ngày trong tháng</Text>
                                </View>
                                <View style={[styles.datepicker, {width: (SCREEN_WIDTH - 20)*0.4,}]}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2118, 12, 31)}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText={this.convertDateObjectToString1(new Date())}
                                        textStyle={{color: Colors.tintColor}}
                                        placeHolderTextStyle={{color: "#070707"}}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                    />
                                </View>
                            </View>
                            <View>
                                <Text style={styles.txtBold}>Chọn nhân viên</Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Chọn nhân viên cần xem"
                                    iosIcon={<Icon name="arrow-dropdown-circle"
                                                   style={{color: Colors.tintColor, fontSize: 25}}/>}
                                    style={{width: undefined}}
                                    selectedValue={this.state.staff_id}
                                    onValueChange={this.getStaff.bind(this)}
                                >
                                    <Picker.Item enabled={false} label="Chọn nhân viên cần xem" value="0"/>
                                    {this.state.employeeList.map((item, key) => {
                                        return (
                                            <Picker.Item label={item.first_name + ' ' + item.last_name} key={key}
                                                         value={item.id}/>
                                        )
                                    })}
                                </Picker>
                            </View>
                            <View style={styles.month_name}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                    paddingVertical: 10,
                                    color: Colors.tintColor,
                                    fontSize: 27,
                                }}>
                                    Tháng {this.state.month_selected}, năm {this.state.year_selected}
                                </Text>
                            </View>
                            <View style={styles.note}>
                                <View style={styles.div_note}>
                                    <Button vertical bordered success>
                                        <Text>Tổng điểm</Text>
                                        <Text style={styles.txtBold}>{this.state.total_point}</Text>
                                    </Button>
                                </View>
                                <View style={styles.div_note}>
                                    <Button vertical bordered success>
                                        <Text>Số giờ làm</Text>
                                        <Text style={styles.txtBold}>{this.state.total_hour_of_month}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.note}>
                                <View style={styles.div_note}>
                                    <Button vertical bordered danger>
                                        <Text>Thiếu (phút)</Text>
                                        <Text style={styles.txtBold}>{this.state.total_missing}</Text>
                                    </Button>
                                </View>
                                <View style={styles.div_note}>
                                    <Button vertical bordered danger>
                                        <Text>Muộn (phút)</Text>
                                        <Text style={styles.txtBold}>{this.state.total_late}</Text>
                                    </Button>
                                </View>

                            </View>
                            <View style={styles.note}>
                                <View style={styles.div_note}>
                                    <Button vertical bordered info>
                                        <Text>Ngày công</Text>
                                        <Text style={styles.txtBold}>{this.state.total_working_day}</Text>
                                    </Button>
                                </View>
                                <View style={styles.div_note}>
                                    <Button vertical bordered info>
                                        <Text>Ngày nghỉ phép</Text>
                                        <Text style={styles.txtBold}>{this.state.annual_leave_remaining_point}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.col3_head}>
                                    <Text style={styles.thead}>Ngày</Text>
                                </View>
                                <View style={styles.col7_head}>
                                    <Text style={styles.thead}>{this.state.staff_name}</Text>
                                </View>
                            </View>
                            {data}
                            <Modal
                                animationType="slide"
                                onRequestClose={() => {
                                    Alert.alert(
                                        'Tuha Work',
                                        'Hủy đề xuất xin nghỉ?',
                                        [
                                            {
                                                text: 'Hủy',
                                                onPress: () => console.log('Hủy'),
                                                style: 'cancel',
                                            },
                                            {text: 'OK', onPress: () => this.setState({modal:false})},
                                        ],

                                    );
                                }}
                                transparent={true}
                                visible={this.state.modal}>
                                <View style={styles.modalDialog}>
                                    <View style={styles.modalContent}>
                                        <View style={styles.modalHeader}>
                                            <View style={{width: (SCREEN_WIDTH - 40) * 0.8}}>
                                                <Text style={styles.modalTitle}>Đề xuất xin nghỉ làm ngày {this.state.dateOff}</Text>
                                            </View>
                                            <View style={{width: (SCREEN_WIDTH - 40) * 0.2}}>
                                                <Button transparent style={{marginTop: 7,}}
                                                        onPress={() => this.setState({modal: false})}>
                                                    <Icon style={{color: '#dc3545'}} ios='ios-close-circle' android="md-close-circle"/>
                                                </Button>
                                            </View>
                                        </View>
                                        <ScrollView>
                                            <View>
                                                <View style={styles.item}>
                                                    <View style={styles.date}>
                                                        <View style={styles.dateFrom}>
                                                            <View style={styles.txtDate}>
                                                                <Text style={styles.txtBold}>Ngày bắt đầu</Text>
                                                            </View>
                                                            <View style={styles.datepicker}>
                                                                <DatePicker
                                                                    defaultDate={this.convertTimeStampToDateObject(this.state.dateOffTimestamp)}
                                                                    minimumDate={new Date(2018, 1, 1)}
                                                                    maximumDate={new Date(2118, 12, 31)}
                                                                    locale={"vn"}
                                                                    timeZoneOffsetInMinutes={undefined}
                                                                    modalTransparent={false}
                                                                    animationType={"fade"}
                                                                    androidMode={"default"}
                                                                    placeHolderText={this.timeConverter(this.state.dateOffTimestamp)}
                                                                    textStyle={{ color: Colors.tintColor }}
                                                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                                                    onDateChange={this.setStartDate}
                                                                    disabled={false}
                                                                />
                                                            </View>

                                                        </View>
                                                        <View style={styles.dateTo}>
                                                            <View style={styles.txtDate}>
                                                                <Text style={styles.txtBold}>Ngày kết thúc</Text>
                                                            </View>
                                                            <View style={styles.datepicker}>
                                                                <DatePicker
                                                                    defaultDate={this.convertTimeStampToDateObject(this.state.dateOffTimestamp)}
                                                                    minimumDate={new Date(2018, 1, 1)}
                                                                    maximumDate={new Date(2118, 12, 31)}
                                                                    locale={"vn"}
                                                                    timeZoneOffsetInMinutes={undefined}
                                                                    modalTransparent={false}
                                                                    animationType={"fade"}
                                                                    androidMode={"default"}
                                                                    placeHolderText={this.timeConverter(this.state.dateOffTimestamp)}
                                                                    textStyle={{ color: Colors.tintColor }}
                                                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                                                    onDateChange={this.setEndDate}
                                                                    disabled={false}
                                                                />
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <View>
                                                        <Text style={{fontWeight: 'bold', paddingVertical: 10,}}>Lý do nghỉ</Text>
                                                        <Textarea rowSpan={5} bordered placeholder="Nhập lý do nghỉ..."
                                                                  onChangeText={this.getMessage} value={this.state.message}/>
                                                    </View>
                                                    <View style={{marginTop: 20,}}>
                                                        <Button block info onPress={() => this.actionLeave()}>
                                                            <Text>Gửi đề xuất</Text>
                                                        </Button>
                                                    </View>
                                                </View>
                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>
                            </Modal>
                        </ScrollView>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalDialog: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    modalContent: {
        width: SCREEN_WIDTH - 40,
        height: SCREEN_HEIGHT / 2 + 127,
        backgroundColor: 'white',
        padding: 10,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 80) / 2,
        paddingBottom: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 80) / 2,
        paddingBottom: 10,
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: Colors.tintColor,
        paddingVertical: 17,
    },
    modalHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row',
    },
    modalBottom: {
        borderTopWidth: 0.5,
        borderTopColor: 'lightgrey',
    },
    success: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        // backgroundColor: '#5cb85c',
        width: (SCREEN_WIDTH - 20) * 0.17,
        marginRight: 5,
    },
    danger: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#d9534f',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    warning: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#f0ad4e',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    info: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#5bc0de',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    thead: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 7,
        color: '#fff',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    col3_head: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col7_head: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col3: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderLeftColor: 'lightgrey',
        borderLeftWidth: 0.7,
        // backgroundColor: '#ddd',
        padding: 7,
    },
    col7: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderRightColor: 'lightgrey',
        borderRightWidth: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 11,
        flexDirection: 'row',
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20) * 0.6,

    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        // width: (SCREEN_WIDTH - 20) * 0.4,

    },

    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    div_note: {
        width: (SCREEN_WIDTH - 60) / 2,
        marginRight: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff'
    },
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
    txtBold: {
        fontWeight: 'bold',
    },
    item: {
        margin: 10,
    },
});
