import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer, DatePicker, Picker
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, ActivityIndicator} from 'react-native'
import SideBar from "../../components/SideBar";
import {Calendar} from 'react-native-calendars';
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {getCalendarData} from "../../apis/calendar";
import {getEmployeeList} from "../../apis/checklist";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

const checklist = {key:'checklist', color: '#46b8da'};
const notice = {key:'notice', color: 'orange'};

export default class CalendarScreen extends Component {

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            selectedDate: null,
            date_selected: new Date(),
            month_selected: today.getMonth() + 1,
            year_selected: today.getFullYear(),
            calendarData: {},
            employeeList: [],
            staff_id: null,
            isFetching: false,
        };
        this.onDayPress = this.onDayPress.bind(this);
        this.setDate = this.setDate.bind(this);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    onDayPress(day) {
        // alert(this.convertDateObjectToString1(day));
        this.setState({
            selectedDate: day.dateString
        });
        this.props.navigation.navigate('ListByDate', {
            dateSelected: day.dateString,
            staffID: this.state.staff_id,
        });
    }

    convertDateObjectToString(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        return [y, (m > 9 ? '' : '0') + m, (d > 9 ? '' : '0') + d].join('-');
    }

    convertDateObjectToString1(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        return [(d > 9 ? '' : '0') + d, (m > 9 ? '' : '0') + m, y].join('/');
    }

    setDate(newDate) {
        this.setState({
            date_selected: newDate,
            month_selected: newDate.getMonth() + 1,
            year_selected: newDate.getFullYear(),
        });
        // this.loadData(newDate);
    }

    getStaff(value) {
        this.setState({
            staff_id: value,
        });
    }

    loadData() {
        this.setState({
            isFetching: true,
        }, () => {
            let objectData = {};
            getCalendarData(this.state.staff_id, this.state.month_selected, this.state.year_selected).then((response) => {
                let datas = response.data;
                Object.keys(datas).forEach(function(key) {
                    if (datas[key][0] === true && datas[key][1] === true) {
                        objectData[key] = {
                            dots: [checklist, notice]
                        };
                    } else if (datas[key][0] === true && datas[key][1] === false) {
                        objectData[key] = {
                            dots: [checklist]
                        };
                    } else if (datas[key][0] === false && datas[key][1] === true) {
                        objectData[key] = {
                            dots: [notice]
                        };
                    }
                    // console.log(key, datas[key]);
                });
                this.setState({
                    calendarData: objectData,
                    isFetching: false,
                });
            }).catch((error) => {
                this.setState({
                    calendarData: {},
                    isFetching: false,
                });
                console.log('lỗi khi load lịch làm việc')
            });
        });
        // let month = date.getMonth() + 1;
        // let year = date.getFullYear();
    }

    async componentDidMount() {
        await this.getWorkEmployee();
        await this.loadData();
    }

    getWorkEmployee() {
        getEmployeeList().then(response => {
            this.setState({
                employeeList: response.data,
            });
        }).catch(error => {
            console.log('Lỗi lấy danh sách nhân viên được giao')
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => { this.drawer = ref; }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab" style={{ backgroundColor: '#2a81ab' }}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: "#fff"}}  ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: "#fff"}} >Lịch của tôi</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.loadData()}>
                                <Text style={{color: '#fff', fontWeight: 'bold',}}>Xem</Text>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content}>
                        {
                            this.state.isFetching &&
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                                <ActivityIndicator  color={'#2196f3'} />
                                <Text style={{color: Colors.tintColor}}>  Đang tiến hành load dữ liệu</Text>
                            </View>
                        }
                        <ScrollView>
                            <View style={styles.date}>
                                <View style={styles.txtDate}>
                                    <Text style={styles.txtLabel}>Chọn 1 ngày trong tháng</Text>
                                </View>
                                <View style={styles.datepicker}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2118, 12, 31)}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText={this.convertDateObjectToString1(new Date())}
                                        textStyle={{ color: Colors.tintColor }}
                                        placeHolderTextStyle={{ color: "#070707" }}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                    />
                                </View>
                            </View>
                            <View>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Chọn nhân viên cần xem"
                                    placeholder="Chọn nhân viên cần xem"
                                    iosIcon={<Icon name="arrow-dropdown-circle"
                                                   style={{color: Colors.tintColor, fontSize: 25}}/>}
                                    style={{width: undefined}}
                                    selectedValue={this.state.staff_id}
                                    onValueChange={this.getStaff.bind(this)}
                                >
                                    <Picker.Item enabled={false} label="Chọn nhân viên cần xem" value="null"/>
                                    {this.state.employeeList.map((item, key) => {
                                        return (
                                            <Picker.Item label={item.full_name} key={key} value={item.id} />
                                        )
                                    })}
                                </Picker>
                            </View>
                            <View style={styles.month_name}>
                                <Text style={{textAlign: 'center', fontWeight: 'bold', paddingVertical: 10,}}>Tháng {this.state.month_selected}, năm {this.state.year_selected}</Text>
                            </View>
                            <View>
                                <Calendar
                                    onDayPress={this.onDayPress}
                                    style={styles.calendar}
                                    current={this.convertDateObjectToString(this.state.date_selected)}
                                    markingType={'multi-dot'}
                                    markedDates={this.state.calendarData}
                                    hideArrows={true}
                                    hideDayNames={false}
                                    theme={{'stylesheet.calendar.header': { header: { height: 0 } }}}
                                />
                            </View>
                            <View style={styles.note}>
                                <View style={styles.div_note}>
                                    <Button iconLeft bordered info
                                    onPress={() => this.props.navigation.navigate('Work')}>
                                        <Icon ios='ios-radio-button-on' android="md-radio-button-on"/>
                                        <Text>Công việc</Text>
                                    </Button>
                                </View>
                                <View style={styles.div_note}>
                                    <Button iconLeft bordered warning
                                    onPress={() => this.props.navigation.navigate('Notification')}>
                                        <Icon ios='ios-radio-button-on' android="md-radio-button-on"/>
                                        <Text>Thông báo</Text>
                                    </Button>
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20)*0.6,

    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20)*0.4,

    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    div_note: {
        width:( SCREEN_WIDTH - 60)/2,
        marginRight: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff'
    },
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
});
