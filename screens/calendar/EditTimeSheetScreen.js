import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Header,
    Title,
    Left,
    Icon,
    Toast, Item, Input, Content
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, Alert} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import _ from 'lodash';
import {deleteTimeSheet, updateTimeSheet} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class EditTimeSheetScreen extends Component {

    constructor(props) {
        super(props);
        // let today = new Date();
        this.dateChosen = this.props.navigation.getParam('dateChosen');
        this.itemTimeSheet = this.props.navigation.getParam('itemTimeSheet');
        this.staffName = this.props.navigation.getParam('staffName');
        this.state = {
            he_so: this.itemTimeSheet.he_so.toString(),
            check_in: this.itemTimeSheet.start_time != 0 ? this.convertDateObjectToString1(this.convertTimeStampToDateObject(this.itemTimeSheet.start_time)) : this.convertDateObjectToString1(new Date()),
            check_out: this.itemTimeSheet.end_time != 0 ? this.convertDateObjectToString1(this.convertTimeStampToDateObject(this.itemTimeSheet.end_time)) : this.convertDateObjectToString1(new Date()),
            // activeTabValue: 0,
        };
        this.setStartDate = this.setStartDate.bind(this);
        this.setEndDate = this.setEndDate.bind(this);
    }

    setStartDate(newDate) {
        this.setState({
            startDate: newDate
        });
    }

    setEndDate(newDate) {
        this.setState({
            endDate: newDate
        });
    }

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    convertDateObjectToString1(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        let h = dateObject.getHours();
        let minute = dateObject.getMinutes();
        return [y, (m > 9 ? '' : '0') + m, (d > 9 ? '' : '0') + d].join('-') + ' ' + (h > 9 ? '' : '0') + h + ':' + (minute > 9 ? '' : '0') + minute;
    }

    convertStringToTimeStamp(dateString) {
        let bits = dateString.split(/\D/);
        let date = new Date(bits[0], --bits[1], bits[2], bits[3], bits[4]);
        return date.getTime() / 1000;
    }

    convertDateObjectToTime(date) {
        return Math.round(date.getTime() / 1000);
    }

    convertTimeStampToDateObject(unix_timestamp) {
        return new Date(unix_timestamp * 1000);
    }

    // componentDidMount() {
    //     if (this.itemTimeSheet.annual_leave == 0) {
    //         this.setState({ activeTabValue: 0 });
    //     } else {
    //         this.setState({  activeTabValue: 1 });
    //     }
    // }

    getCheckIn = (number) => {
        this.setState({check_in: number});
    };

    getCheckOut = (number) => {
        this.setState({check_out: number});
    };

    getHeSoCong = (number) => {
        this.setState({he_so: number});
    };

    actionDateUpdate() {
        // alert(123);
        let start_time = this.convertStringToTimeStamp(this.state.check_in);
        let finish_time = this.convertStringToTimeStamp(this.state.check_out);
        // console.log(start_time, this.dateChosen);
        if (start_time < this.dateChosen) {
            Toast.show({
                text: "Ngày bắt đầu không được trước ngày được chọn",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        if (start_time > finish_time) {
            Toast.show({
                text: "Ngày kết thúc phải sau ngày bắt đầu",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        let data = {
            start_time: start_time,
            end_time: finish_time,
            staff_id: this.itemTimeSheet.employee_id,
            he_so: this.state.he_so,
            annual_leave_id: this.itemTimeSheet.id,
        };
        // console.log(data)
        const { navigation } = this.props;
        if (start_time >= this.dateChosen && start_time <= finish_time) {
            updateTimeSheet(data).then(response => {
                navigation.goBack(null);
                navigation.state.params.refreshTimeSheet();
                Toast.show({
                    text: "Cập nhật ngày công thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                // console.log(error.response.data);
                if (error.response.data.error) {
                    Toast.show({
                        text: error.response.data.error,
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: {color: "#dc3545"},
                        buttonStyle: {backgroundColor: "white"}
                    });
                } else {
                    Toast.show({
                        text: "Lỗi khi cập nhật chấm công",
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: {color: "#dc3545"},
                        buttonStyle: {backgroundColor: "white"}
                    });
                }
            })
        }
    }

    deleteConfirm() {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa dữ liệu chấm công này không?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.actionDateDelete()
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    actionDateDelete() {
        const { navigation } = this.props;
        deleteTimeSheet(this.itemTimeSheet.id).then(response => {
            navigation.goBack(null);
            navigation.state.params.refreshTimeSheet();
            Toast.show({
                text: "Xóa dữ liệu chấm công thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: {color: "#28a745"},
                buttonStyle: {backgroundColor: "white"}
            });
        }).catch(error => {
            Toast.show({
                text: error.response.data.error,
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        })
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Sửa ngày công {this.timeConverter(this.dateChosen)}</Title>
                    </Body>
                    {/*<Right/>*/}
                </Header>
                <Content padder style={styles.content}>
                    <ScrollView>
                        <View>
                            <View>
                                <Text style={styles.txtBold}>Giờ vào</Text>
                                <Item>
                                    <Input placeholder="VD: 2020-03-03 07:07" onChangeText={this.getCheckIn}
                                           value={this.state.check_in}/>
                                </Item>
                            </View>
                            <View>
                                <Text style={styles.txtBold}>Giờ ra</Text>
                                <Item>
                                    <Input placeholder="VD: 2020-03-03 07:07" onChangeText={this.getCheckOut}
                                           value={this.state.check_out}/>
                                </Item>
                            </View>
                            <View>
                                <Text style={styles.txtBold}>Hệ số</Text>
                                <Item>
                                    <Input keyboardType='numeric' placeholder="Nhập hệ số"
                                           onChangeText={this.getHeSoCong} value={this.state.he_so}/>
                                </Item>
                            </View>
                            <View style={{marginTop: 20,}}>
                                <Button block info onPress={() => this.actionDateUpdate()}>
                                    <Text>Lưu</Text>
                                </Button>
                            </View>
                            <View style={{marginTop: 20,}}>
                                <Button block danger onPress={() => this.deleteConfirm()}>
                                    <Text>Xóa dữ liệu ngày công</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalDialog: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    modalContent: {
        width: SCREEN_WIDTH - 40,
        height: SCREEN_HEIGHT / 2 + 127,
        backgroundColor: 'white',
        padding: 10,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        paddingBottom: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        paddingBottom: 10,
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: Colors.tintColor,
        paddingVertical: 17,
    },
    modalHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row',
    },
    modalBottom: {
        borderTopWidth: 0.5,
        borderTopColor: 'lightgrey',
    },
    success: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        // backgroundColor: '#5cb85c',
        width: (SCREEN_WIDTH - 20) * 0.17,
        marginRight: 5,
    },
    danger: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#d9534f',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    warning: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#f0ad4e',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    info: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#5bc0de',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    thead: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 7,
        color: '#fff',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    col3_head: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col7_head: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col3: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderLeftColor: 'lightgrey',
        borderLeftWidth: 0.7,
        // backgroundColor: '#ddd',
        padding: 7,
    },
    col7: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderRightColor: 'lightgrey',
        borderRightWidth: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 11,
        flexDirection: 'row',
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20) * 0.6,

    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        // width: (SCREEN_WIDTH - 20) * 0.4,

    },

    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    div_note: {
        width: (SCREEN_WIDTH - 60) / 2,
        marginRight: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff'
    },
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
    txtBold: {
        fontWeight: 'bold',
    },
    item: {
        margin: 10,
    },
});
