import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer, DatePicker,
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, FlatList} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import _ from 'lodash';
import {getTimeSheetData} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;

export default class TimeSheetRankScreen extends Component {

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            selectedDate: null,
            date_selected: new Date(),
            month_selected: today.getMonth() + 1,
            year_selected: today.getFullYear(),
            timeSheetRankData: {},
        };
        this.onDayPress = this.onDayPress.bind(this);
        this.setDate = this.setDate.bind(this);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    onDayPress(day) {
        // alert(this.convertDateObjectToString1(day));
        this.setState({
            selectedDate: day.dateString
        });
        this.props.navigation.navigate('ListByDate', {
            dateSelected: day.dateString
        });
    }

    convertDateObjectToString(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        return [y, (m > 9 ? '' : '0') + m, (d > 9 ? '' : '0') + d].join('-');
    }

    convertDateObjectToString1(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        return [(d > 9 ? '' : '0') + d, (m > 9 ? '' : '0') + m, y].join('/');
    }

    setDate(newDate) {
        this.setState({
            date_selected: newDate,
            month_selected: newDate.getMonth() + 1,
            year_selected: newDate.getFullYear(),
        });
        this.loadData(newDate);
    }

    loadData(date) {
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        getTimeSheetData(month, year).then(response => {
            this.setState({
                timeSheetRankData: response.data
            })
        }).catch((error) => {
            console.log('lỗi khi load bảng chấm công')
        });
    }

    async componentDidMount() {
        await this.loadData(new Date());
    }

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => { this.drawer = ref; }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab" style={{ backgroundColor: '#2a81ab' }}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: "#fff"}}  ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: "#fff"}} >Bảng xếp hạng</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content padder style={styles.content}>
                        <ScrollView>
                            <View style={styles.date}>
                                <View style={styles.txtDate}>
                                    <Text style={styles.txtBold}>Chọn 1 ngày trong tháng</Text>
                                </View>
                                <View style={styles.datepicker}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2118, 12, 31)}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText={this.convertDateObjectToString1(new Date())}
                                        textStyle={{ color: Colors.tintColor }}
                                        placeHolderTextStyle={{ color: "#070707" }}
                                        onDateChange={this.setDate}
                                        disabled={false}
                                    />
                                </View>
                            </View>
                            <View style={styles.month_name}>
                                <Text style={{textAlign: 'center', fontWeight: 'bold', paddingVertical: 10, color: Colors.tintColor, fontSize: 27,}}>
                                    Tháng {this.state.month_selected}, năm {this.state.year_selected}
                                </Text>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.col40_head}>
                                    <Text style={styles.thead}>Tên nhân viên</Text>
                                </View>
                                <View style={styles.col15_head}>
                                    <Text style={styles.thead}>TĐ</Text>
                                </View>
                                <View style={styles.col15_head}>
                                    <Text style={styles.thead}>M</Text>
                                </View>
                                <View style={styles.col15_head}>
                                    <Text style={styles.thead}>NC</Text>
                                </View>
                                <View style={styles.col15_head}>
                                    <Text style={styles.thead}>NP</Text>
                                </View>
                            </View>
                            <View>
                                <FlatList
                                    data={Object.keys(this.state.timeSheetRankData)}
                                    renderItem={({ item }) =>
                                        <View style={styles.row}>
                                            <View style={styles.col40}>
                                                <Text style={styles.thead1}>{item}. {this.state.timeSheetRankData[item].full_name}</Text>
                                            </View>
                                            <View style={styles.col15}>
                                                <Text style={styles.info}> {this.state.timeSheetRankData[item].total_point} </Text>
                                            </View>
                                            <View style={styles.col15}>
                                                <Text style={styles.danger}> {this.state.timeSheetRankData[item].total_late} </Text>
                                            </View>
                                            <View style={styles.col15}>
                                                <Text style={styles.info}> {this.state.timeSheetRankData[item].total_working_day} </Text>
                                            </View>
                                            <View style={styles.col15}>
                                            <Text style={styles.info}> {this.state.timeSheetRankData[item].annual_leave_remaining} </Text>
                                            </View>
                                        </View>
                                    }
                                    extraData={Object.keys(this.state.timeSheetRankData)}
                                    keyExtractor={(item) => item}
                                    ListEmptyComponent={() =>
                                        <View style={styles.row}>
                                            <Text style={{ textAlign: 'center' }}>Đang tiến hành load dữ liệu...</Text>
                                        </View>
                                    }
                                    // onEndReached={() => this.loadMore()}
                                    // onEndReachedThreshold={0.4}
                                />
                            </View>
                            {/*{Object.keys(this.state.timeSheetRankData).map((key, index) => {*/}
                                {/*return (*/}

                                {/*)*/}
                            {/*})}*/}
                        </ScrollView>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    danger: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#d9534f',
        fontSize: 11,
        paddingVertical: 14,
    },
    info: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 11,
        paddingVertical: 14,
    },
    thead: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 7,
        color: '#fff',
        fontSize: 12,
    },
    thead1: {
        fontWeight: 'bold',
        textAlign: 'left',
        padding: 7,
        fontSize: 12,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    col40_head: {
        width: (SCREEN_WIDTH - 20)*0.5,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col15_head: {
        width: (SCREEN_WIDTH - 20)*0.125,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col40: {
        width: (SCREEN_WIDTH - 20)*0.5,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderLeftColor: 'lightgrey',
        borderLeftWidth: 0.7,
        padding: 7,
    },
    col15: {
        width: (SCREEN_WIDTH - 20)*0.125,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderRightColor: 'lightgrey',
        borderRightWidth: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ddd',
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20)*0.6,

    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20)*0.4,

    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    div_note: {
        width:( SCREEN_WIDTH - 60)/2,
        marginRight: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff'
    },
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
    txtBold: {
        fontWeight: 'bold',
    },
});
