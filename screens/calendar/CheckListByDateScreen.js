import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Separator,
    ListItem,
    Right,
    Accordion,
} from "native-base";
import {Platform, StatusBar, StyleSheet, View} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {getCalendarDetail} from "../../apis/calendar";

const SCREEN_WIDTH = Dimension.window.width;

// const dataWorkArray = [
//     { name: "Công việc 1", content: "Lorem ipsum dolor sit amet" },
//     { name: "Công việc 2", content: "Lorem ipsum dolor sit amet" },
//     { name: "Công việc 3", content: "Lorem ipsum dolor sit amet" }
// ];
//
// const dataNotificationArray = [
//     { name: "Thông báo 1", content: "Lorem ipsum dolor sit amet" },
//     { name: "Thông báo 2", content: "Lorem ipsum dolor sit amet" },
//     { name: "Thông báo 3", content: "Lorem ipsum dolor sit amet" }
// ];

export default class CheckListByDateScreen extends Component {
    constructor(props) {
        super(props);
        // this.date = this.props.navigation.getParam('dateSelected');
        this.state = {
            dataWorkArray: [],
            dataNotificationArray: [],
        };
    }

    changedateFormat(dateString) {
        let date = dateString.split("-");
        return date[2] + "/" + date[1] + "/" + date[0];
    }

    convertDateObjectToTime(dateString) {
        let date = dateString.split("-");
        let newDate = date[1] + "/" + date[2] + "/" + date[0];
        return (new Date(newDate).getTime() / 1000);
    }

    // timeConverter(UNIX_timestamp) {
    //     let date = new Date(UNIX_timestamp*1000);
    //     let year = date.getFullYear();
    //     let month = date.getMonth() + 1;
    //     let day = date.getDate();
    //     return day + '/' + month + '/' + year;
    // }

    async componentDidMount() {
        // alert(this.timeConverter(1575133200));
        await this.loadData();
    }

    loadData() {
        let date = this.convertDateObjectToTime(this.props.navigation.state.params.dateSelected);
        let staff_id = this.props.navigation.state.params.staffID;
        // alert(this.props.navigation.state.params.dateSelected);
        getCalendarDetail(staff_id, date).then(response => {
            // console.log(response.data.tasks);
            this.setState({
                dataWorkArray: response.data.tasks,
                dataNotificationArray: response.data.reminders,
            })
        }).catch(error => {
            console.log('lỗi khi lấy danh sách công việc và thông báo theo ngày');
        })
    }

    _renderWorkHeader(item, expanded) {
        return (
            <View style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: "#ffffff"
            }}>
                <Text style={{fontWeight: "600", color: '#46b8da'}}>
                    {item.name.length < 37 ? item.name : item.name.substr(0, 37) + '...'}
                </Text>
                {expanded
                    ? <Icon style={{fontSize: 18, color: '#46b8da'}} name="remove-circle"/>
                    : <Icon style={{fontSize: 18, color: '#46b8da'}} name="add-circle"/>}
            </View>
        );
    }

    _renderNotificationHeader(item, expanded) {
        return (
            <View style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: "#ffffff"
            }}>
                <Text style={{fontWeight: "600", color: 'orange'}}>
                    {item.content.length < 37 ? item.content : item.content.substr(0, 37) + '...'}
                </Text>
                {expanded
                    ? <Icon style={{fontSize: 18, color: 'orange'}} name="remove-circle"/>
                    : <Icon style={{fontSize: 18, color: 'orange'}} name="add-circle"/>}
            </View>
        );
    }

    _renderWorkContent(item) {
        return (
            <View style={styles.item}>
                <View>
                    <Text style={styles.information}>Mô tả công việc: {item.description}</Text>
                </View>
                <View>
                    <Text>Người tạo:
                        <Text style={{fontWeight: 'bold', color: '#8a6d3b'}}> {item.first_name} {item.last_name}</Text>
                    </Text>
                </View>
                <View>
                    <Text>Thời gian:
                        <Text style={{fontWeight: 'bold',}}> {item.start_date} - {item.finish_date}</Text>
                    </Text>
                </View>
            </View>
        );
    }

    _renderNotificationContent(item) {
        return (
            <View style={styles.item}>
                <View>
                    <Text>Người tạo:
                        <Text style={{fontWeight: 'bold', color: '#8a6d3b'}}>  {item.first_name} {item.last_name}</Text>
                    </Text>
                </View>
                <View>
                    <Text>Thời gian:
                        <Text style={{fontWeight: 'bold',}}> {item.start_time} - {item.finish_time}</Text>
                    </Text>
                </View>
                <View>
                    <Text style={{fontWeight: "600", color: 'orange',}}>
                        {item.content}
                    </Text>
                </View>
            </View>
        );
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: "#fff"}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: "#fff"}}>
                        {this.props.navigation.state.params.dateSelected
                            ? 'Ngày ' + this.changedateFormat(this.props.navigation.state.params.dateSelected)
                            : 'Lịch của tôi'}
                    </Title>
                    </Body>
                    <Right/>
                </Header>
                <Content style={styles.content}>
                    <View style={styles.separate}>
                        <Text style={styles.title}>Danh sách công việc</Text>
                    </View>
                    <Accordion
                        dataArray={this.state.dataWorkArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderWorkHeader}
                        renderContent={this._renderWorkContent}
                    />

                    <View style={styles.separate}>
                        <Text style={styles.title}>Danh sách thông báo</Text>
                    </View>
                    <Accordion
                        dataArray={this.state.dataNotificationArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderNotificationHeader}
                        renderContent={this._renderNotificationContent}
                    />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff',
    },
    separate: {
        backgroundColor: '#f9f9f9',
    },
    title: {
        color: Colors.tintColor,
        fontWeight: 'bold',
        fontSize: 18,
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    item: {
        padding: 15,
        backgroundColor: '#F7F7F7',
        // marginTop: 10,
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 4
            }
        }),
    },
});

