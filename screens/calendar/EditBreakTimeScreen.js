import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Header,
    Title,
    Left,
    Icon,
    DatePicker, Picker, Toast, Textarea, Content
} from "native-base";
import {Platform, StatusBar, StyleSheet, View, ScrollView, Alert} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import _ from 'lodash';
import {deleteTimeSheet, updateBreakTime} from "../../apis/hrm";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class EditBreakTimeScreen extends Component {

    constructor(props) {
        super(props);
        // let today = new Date();
        this.dateChosen = this.props.navigation.getParam('dateChosen');
        this.itemTimeSheet = this.props.navigation.getParam('itemTimeSheet');
        this.staffName = this.props.navigation.getParam('staffName');
        this.state = {
            startDate: this.itemTimeSheet.start_time ? this.convertTimeStampToDateObject(this.itemTimeSheet.start_time) : this.convertTimeStampToDateObject(this.dateChosen),
            endDate: this.itemTimeSheet.end_time ? this.convertTimeStampToDateObject(this.itemTimeSheet.end_time) : this.convertTimeStampToDateObject(this.dateChosen),
            message: this.itemTimeSheet.leave_reason,
            leave_he_so: this.itemTimeSheet.he_so.toString(),
            // activeTabValue: 0,
        };
        this.setStartDate = this.setStartDate.bind(this);
        this.setEndDate = this.setEndDate.bind(this);
    }

    setStartDate(newDate) {
        this.setState({
            startDate: newDate
        });
    }

    setEndDate(newDate) {
        this.setState({
            endDate: newDate
        });
    }

    getMessage = (text) => {
        this.setState({ message: text });
    };

    getHeSo(value) {
        this.setState({
            leave_he_so: value
        });
    }

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        return day + '/' + month + '/' + year;
    }

    convertDateObjectToString1(dateObject) {
        let y = dateObject.getFullYear();
        let m = dateObject.getMonth() + 1; // getMonth() is zero-based
        let d = dateObject.getDate();
        let h = dateObject.getHours();
        let minute = dateObject.getMinutes();
        return [y, (m > 9 ? '' : '0') + m, (d > 9 ? '' : '0') + d].join('-') + ' ' + h + ':' + minute;
    }

    convertDateObjectToTime(date) {
        return Math.round(date.getTime()/1000);
    }

    convertTimeStampToDateObject(unix_timestamp) {
        return new Date(unix_timestamp *1000);
    }

    // componentDidMount() {
    //     if (this.itemTimeSheet.annual_leave == 0) {
    //         this.setState({ activeTabValue: 0 });
    //     } else {
    //         this.setState({  activeTabValue: 1 });
    //     }
    // }

    actionLeave() {
        let start_time = this.convertDateObjectToTime(this.state.startDate);
        let finish_time = this.convertDateObjectToTime(this.state.endDate);
        if (start_time < this.dateChosen) {
            Toast.show({
                text: "Ngày bắt đầu không được trước ngày được chọn",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        if (start_time > finish_time) {
            Toast.show({
                text: "Ngày kết thúc phải sau ngày bắt đầu",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: {color: "#dc3545"},
                buttonStyle: {backgroundColor: "white"}
            });
        }
        let data = {
            start_time: start_time,
            end_time: finish_time,
            leave_reason: this.state.message,
            leave_he_so: this.state.leave_he_so,
            annual_leave_id: this.itemTimeSheet.id,
        };
        const { navigation } = this.props;
        if (start_time >= this.dateChosen && start_time <= finish_time) {
            updateBreakTime(data).then(response => {
                navigation.goBack(null);
                navigation.state.params.refreshTimeSheet();
                Toast.show({
                    text: "Duyệt đề xuất xin nghỉ thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                console.log(error.response.data);
                if (error.response.data.error) {
                    Toast.show({
                        text: error.response.data.error,
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: {color: "#dc3545"},
                        buttonStyle: {backgroundColor: "white"}
                    });
                } else {
                    Toast.show({
                        text: "Lỗi khi duyệt nghỉ phép",
                        buttonText: "Ok",
                        type: "danger",
                        duration: 5000,
                        buttonTextStyle: {color: "#dc3545"},
                        buttonStyle: {backgroundColor: "white"}
                    });
                }
            })
        }
    }

    deleteConfirm() {
        Alert.alert(
            'Tuha Work',
            'Bạn có chắc muốn xóa dữ liệu chấm công này không?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.actionDateDelete()
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: false
            },
        );
    }

    actionDateDelete() {
        const { navigation } = this.props;
        // alert(this.itemTimeSheet.id);
        deleteTimeSheet(this.itemTimeSheet.id).then(response => {
            navigation.goBack(null);
            navigation.state.params.refreshTimeSheet();
            Toast.show({
                text: "Xóa dữ liệu chấm công thành công",
                buttonText: "Ok",
                type: "success",
                duration: 5000,
                buttonTextStyle: { color: "#28a745" },
                buttonStyle: { backgroundColor: "white" }
            });
        }).catch(error => {
            if (error.response.data.error) {
                Toast.show({
                    text: error.response.data.error,
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: {color: "#dc3545"},
                    buttonStyle: {backgroundColor: "white"}
                });
            } else {
                Toast.show({
                    text: "Lỗi khi xóa dữ liệu chấm công",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: {color: "#dc3545"},
                    buttonStyle: {backgroundColor: "white"}
                });
            }
        })
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Duyệt phép ngày {this.timeConverter(this.dateChosen)}</Title>
                    </Body>
                    {/*<Right/>*/}
                </Header>
                <Content padder style={styles.content}>
                    <ScrollView>
                        <View>
                            <View>
                                <Text style={{paddingVertical: 10,}}><Text style={styles.txtBold}>Nhân viên</Text>: {this.staffName}</Text>
                            </View>
                            <View style={styles.date}>
                                <View style={styles.dateFrom}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtBold}>Ngày bắt đầu</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={this.state.startDate}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.itemTimeSheet.start_time
                                                ? this.timeConverter(this.itemTimeSheet.start_time)
                                                : this.timeConverter(this.dateChosen)}
                                            textStyle={{ color: Colors.tintColor }}
                                            placeHolderTextStyle={{ color: "#d3d3d3" }}
                                            onDateChange={this.setStartDate}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                                <View style={styles.dateTo}>
                                    <View style={styles.txtDate}>
                                        <Text style={styles.txtBold}>Ngày kết thúc</Text>
                                    </View>
                                    <View style={styles.datepicker}>
                                        <DatePicker
                                            defaultDate={this.state.endDate}
                                            minimumDate={new Date(2018, 1, 1)}
                                            maximumDate={new Date(2118, 12, 31)}
                                            locale={"vn"}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={"fade"}
                                            androidMode={"default"}
                                            placeHolderText={this.itemTimeSheet.end_time
                                                ? this.timeConverter(this.itemTimeSheet.end_time)
                                                : this.timeConverter(this.dateChosen)}
                                            textStyle={{ color: Colors.tintColor }}
                                            placeHolderTextStyle={{ color: "#d3d3d3" }}
                                            onDateChange={this.setEndDate}
                                            disabled={false}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.txtBold}>Chọn hệ số</Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Hệ số"
                                    iosIcon={<Icon name="arrow-dropdown-circle" style={{color: Colors.tintColor, fontSize: 25}}/>}
                                    style={{width: undefined}}
                                    selectedValue={this.state.leave_he_so}
                                    onValueChange={this.getHeSo.bind(this)}
                                >
                                    <Picker.Item label="Không tính ngày công" value="0"/>
                                    <Picker.Item label="1/4 ngày công" value="0.25"/>
                                    <Picker.Item label="Nửa ngày công" value="0.5"/>
                                    <Picker.Item label="1 ngày công" value="1"/>
                                </Picker>
                            </View>
                            <View>
                                <Text style={{fontWeight: 'bold', paddingVertical: 10,}}>Lý do nghỉ</Text>
                                <Textarea rowSpan={5} bordered placeholder="Nhập lý do nghỉ..."
                                          onChangeText={this.getMessage} value={this.state.message}/>
                            </View>
                            <View style={{marginTop: 20,}}>
                                <Button block info onPress={() => this.actionLeave()}>
                                    <Text>Gửi đề xuất</Text>
                                </Button>
                            </View>
                            <View style={{marginTop: 20,}}>
                                <Button block danger onPress={() => this.deleteConfirm()}>
                                    <Text>Xóa dữ liệu ngày công</Text>
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    modalDialog: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    modalContent: {
        width: SCREEN_WIDTH - 40,
        height: SCREEN_HEIGHT / 2 + 127,
        backgroundColor: 'white',
        padding: 10,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        paddingBottom: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        paddingBottom: 10,
    },
    modalTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        color: Colors.tintColor,
        paddingVertical: 17,
    },
    modalHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row',
    },
    modalBottom: {
        borderTopWidth: 0.5,
        borderTopColor: 'lightgrey',
    },
    success: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        // backgroundColor: '#5cb85c',
        width: (SCREEN_WIDTH - 20) * 0.17,
        marginRight: 5,
    },
    danger: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#d9534f',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    warning: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#f0ad4e',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    info: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 3,
        color: '#fff',
        backgroundColor: '#5bc0de',
        width: (SCREEN_WIDTH - 20) * 0.17,
    },
    thead: {
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 7,
        color: '#fff',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    col3_head: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col7_head: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderColor: 'lightgrey',
        borderWidth: 0.7,
        marginTop: 27,
        backgroundColor: Colors.tintColor,
    },
    col3: {
        width: (SCREEN_WIDTH - 20) * 0.3,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderLeftColor: 'lightgrey',
        borderLeftWidth: 0.7,
        // backgroundColor: '#ddd',
        padding: 7,
    },
    col7: {
        width: (SCREEN_WIDTH - 20) * 0.7,
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.7,
        borderRightColor: 'lightgrey',
        borderRightWidth: 0.7,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 11,
        flexDirection: 'row',
    },
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        width: (SCREEN_WIDTH - 20) * 0.6,

    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        // width: (SCREEN_WIDTH - 20) * 0.4,

    },

    note: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        marginTop: 20,
    },
    div_note: {
        width: (SCREEN_WIDTH - 60) / 2,
        marginRight: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#ffffff'
    },
    calendar: {
        borderTopWidth: 1,
        paddingTop: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        height: 350
    },
    text: {
        textAlign: 'center',
        borderColor: '#bbb',
        padding: 10,
        backgroundColor: '#eee'
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2,
            }
        }),
    },
    txtBold: {
        fontWeight: 'bold',
    },
    item: {
        margin: 10,
    },
});
