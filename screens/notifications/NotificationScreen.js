import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Thumbnail
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View, RefreshControl, TouchableOpacity} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";

import Setting from "../../constants/Setting";
import {getReminderList} from "../../apis/notification";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class NotificationScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    state = {
        reminderList: [],
        isFetching: false,
        keyword: '',
        start_time: '',
        end_time: '',
    };

    constructor(props) {
        super(props);
    }

    closeDrawer() {
        this.drawer._root.close()
    }

    openDrawer() {
        this.drawer._root.open()
    };

    async componentDidMount() {
        await this.loadData();
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getReminderList(this.state.keyword, this.state.start_time, this.state.end_time, (this.page - 1) * this.itemPerPage, this.itemPerPage).then(reminders => {
                // alert(JSON.stringify(reminders.data));
                let newData = [];
                _.forEach(reminders.data, reminder => {
                    let isExist = _.find(this.state.reminderList, {
                        id: reminder.id
                    });
                    if (!isExist) {
                        newData.push(reminder)
                    }
                });
                this.setState({
                    reminderList: this.state.reminderList.concat(newData),
                    isFetching: false,
                    allLoaded: reminders.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    reminderList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    removeItemFromList = (rid) => {
        // let newArray = [];
        let newArray = this.state.reminderList.filter(function( item ) {
            return item.id !== rid;
        });
        this.setState({
            reminderList: newArray,
        });
    };

    onRefresh = () => {
        this.page = 1;
        this.setState({
            reminderList: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    getReminderDetail = (item) => {
        this.props.navigation.navigate('DetailNotification', {
            reminderData: item,
            removeReminder: this.removeItemFromList,
            refreshReminder: this.onRefresh,
        });
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.getReminderDetail(item)}>
                    <View style={styles.type}>
                        <Text style={styles.txtType}>Company</Text>
                    </View>
                    <View>
                        <Text style={styles.information}>{item.content ? item.content.replace(/(<([^>]+)>)/ig,"") : item.content}</Text>
                    </View>
                    <View>
                        <Text>Người tạo:
                            <Text style={{fontWeight: 'bold', color: '#8a6d3b'}}> {item.first_name} {item.last_name}</Text>
                        </Text>
                    </View>
                    <View>
                        <Text>Thời gian:
                            <Text style={{fontWeight: 'bold',}}> {this.timeConverter(item.start_time)} - {this.timeConverter(item.finish_time)}</Text>
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Danh sách thông báo trống!</Text>
            </View>
        );
    };

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    render() {
        return (
            <Container style={styles.container}>
                <Drawer ref={(ref) => {
                    this.drawer = ref;
                }} content={<SideBar navigation={this.props.navigation}/>} onClose={() => this.closeDrawer()}>
                    <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                            style={{backgroundColor: '#2a81ab'}}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{color: '#fff'}} ios='ios-menu' android="md-menu"/>
                            </Button>
                        </Left>
                        <Body style={styles.body}>
                        <Title style={{color: '#fff'}}>Quản lý thông báo</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={() => this.props.navigation.navigate('NotificationCreate', {
                                refreshReminder: this.onRefresh,
                            })}>
                                <Icon style={{color: '#fff'}} ios='ios-add-circle' android="md-add-circle"/>
                            </Button>
                            <Button transparent onPress={() => this.props.navigation.navigate('NotificationSearch')}>
                                <Icon style={{color: '#fff'}} ios='ios-search' android="md-search"/>
                            </Button>
                        </Right>
                    </Header>
                    <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                        refreshControl={
                           <RefreshControl
                               onRefresh={this.onRefresh.bind(this)}
                               refreshing={this.state.isFetching}
                               progressBackgroundColor={'#fff'}
                           />
                        }
                    >
                        <View style={styles.content}>
                        <FlatList
                        data={this.state.reminderList}
                        renderItem={this.renderItem}
                        extraData={this.state.reminderList}
                        keyExtractor={this.extractItemKey}
                        ListEmptyComponent={this.ListEmpty}
                        onEndReached={() => this.loadMore()}
                        onEndReachedThreshold={0.4}
                        />
                        </View>
                    </Content>
                </Drawer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#f0ad4e',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        padding: 10,
        backgroundColor: '#f1e6c1',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 2
            }
        }),
    },
});

