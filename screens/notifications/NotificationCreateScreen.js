import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Textarea,
    CheckBox,
    DatePicker,
    ListItem, Toast
} from "native-base";
import {Platform, StatusBar, StyleSheet, View} from 'react-native'
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";
import {editReminder, saveReminder} from "../../apis/notification";

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class NotificationCreateScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            message: '',
            isPublic: false,
            isEmail: false,
            public: 0,
            email: 0,
        };
        this.setStartDate = this.setStartDate.bind(this);
        this.setEndDate = this.setEndDate.bind(this);
    }

    setStartDate(newDate) {
        this.setState({
            startDate: newDate
        });
    }

    setEndDate(newDate) {
        this.setState({
            endDate: newDate
        });
    }

    getMessage = (text) => {
        this.setState({ message: text });
    };

    onPublicCheckBoxPress() {
        // alert(13);
        if (!this.state.isPublic) {
            this.setState({
                isPublic: true,
                public: 1
            });
        } else {
            this.setState({
                isPublic: false,
                public: 0
            });
        }
    }

    onEmailCheckBoxPress() {
        // alert(13);
        if (!this.state.isEmail) {
            this.setState({
                isEmail: true,
                public: 0,
                email: 1,
            });
        } else {
            this.setState({
                isEmail: false,
                email: 0,
            });
        }
    }

    convertDateObjectToTime(date) {
        return Math.round(date.getTime()/1000);
    }

    addNotification() {
        let dataReminder = {
            start_time: this.convertDateObjectToTime(this.state.startDate),
            finish_time: this.convertDateObjectToTime(this.state.endDate),
            public: this.state.public,
            email: this.state.email,
            message: this.state.message,
        };
        console.log(dataReminder);
        const { navigation } = this.props;
        if (this.state.message) {
            saveReminder(dataReminder).then((response) => {
                navigation.goBack(null);
                navigation.state.params.refreshReminder();
                Toast.show({
                    text: "Thêm mới thông báo thành công",
                    buttonText: "Ok",
                    type: "success",
                    duration: 5000,
                    buttonTextStyle: { color: "#28a745" },
                    buttonStyle: { backgroundColor: "white" }
                });
            }).catch(error => {
                // console.log(error);
                Toast.show({
                    text: "Thêm mới thông báo thất bại",
                    buttonText: "Ok",
                    type: "danger",
                    duration: 5000,
                    buttonTextStyle: { color: "#dc3545" },
                    buttonStyle: { backgroundColor: "white" }
                });
            })
        } else {
            Toast.show({
                text: "Vui lòng nhập nội dung thông báo",
                buttonText: "Ok",
                type: "danger",
                duration: 5000,
                buttonTextStyle: { color: "#dc3545" },
                buttonStyle: { backgroundColor: "white" }
            });
        }

    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Thêm mới thông báo</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.addNotification()}>
                            <Text style={{color: '#fff'}}>Lưu</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}>
                    <View style={styles.item}>
                        <View>
                            <Text style={{fontWeight: 'bold',}}>Nội dung thông báo</Text>
                            <Textarea rowSpan={5} bordered placeholder="Nhập nội dung thông báo..."
                                      onChangeText={this.getMessage} value={this.state.message}/>
                        </View>
                        <View style={styles.date}>
                            <View style={styles.dateFrom}>
                                <View style={styles.txtDate}>
                                    <Text style={styles.txtLabel}>Ngày bắt đầu</Text>
                                </View>
                                <View style={styles.datepicker}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2118, 12, 31)}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Chọn ngày bắt đầu"
                                        textStyle={{ color: Colors.tintColor }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.setStartDate}
                                        disabled={false}
                                    />
                                </View>

                            </View>
                            <View style={styles.dateTo}>
                                <View style={styles.txtDate}>
                                    <Text style={styles.txtLabel}>Ngày kết thúc</Text>
                                </View>
                                <View style={styles.datepicker}>
                                    <DatePicker
                                        defaultDate={new Date()}
                                        minimumDate={new Date(2018, 1, 1)}
                                        maximumDate={new Date(2118, 12, 31)}
                                        locale={"vn"}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={"fade"}
                                        androidMode={"default"}
                                        placeHolderText="Chọn ngày kết thúc"
                                        textStyle={{ color: Colors.tintColor }}
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.setEndDate}
                                        disabled={false}
                                    />
                                </View>

                            </View>
                        </View>
                        <View style={styles.checkbox}>
                            <View style={styles.checkbox01}>
                                <ListItem>
                                    <CheckBox checked={this.state.isPublic} color="#20a8d8"
                                              onPress={() => this.onPublicCheckBoxPress()}/>
                                    <Body>
                                    <Text>Công khai</Text>
                                    </Body>
                                </ListItem>
                            </View>
                            <View style={styles.checkbox02}>
                                <ListItem>
                                    <CheckBox checked={this.state.isEmail} color="#20a8d8"
                                              onPress={() => this.onEmailCheckBoxPress()}/>
                                    <Body>
                                    <Text>Gửi mail</Text>
                                    </Body>
                                </ListItem>
                            </View>
                        </View>
                    </View>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtDate: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    datepicker: {
        // width: (SCREEN_WIDTH - 20),
        paddingLeft: 0,
    },
    txtLabel: {
        // color: '#d3d3d3',
        fontWeight: 'bold',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginRight: 10,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        // flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
        // marginLeft: 10,
    },
    checkbox: {
        flexDirection: 'row',
    },
    checkbox01: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    checkbox02: {
        width:( SCREEN_WIDTH - 30)/2,
    },
    txtBold: {
        fontWeight: 'bold',
        marginTop: 10,
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    item: {
        // padding: 10,
        // backgroundColor: '#ffffff',
        // borderColor: 'lightgrey',
        // borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

