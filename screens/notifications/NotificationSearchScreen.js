import React, {Component} from 'react';
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Drawer,
    Thumbnail, DatePicker, Input, Item
} from "native-base";
import {Platform, StatusBar, StyleSheet, FlatList, View, RefreshControl, TouchableOpacity} from 'react-native'
import SideBar from "../../components/SideBar";
import Dimension from "../../constants/Dimension";
import Colors from "../../constants/Colors";

import Setting from "../../constants/Setting";
import {getReminderList} from "../../apis/notification";
import _ from 'lodash';

const SCREEN_WIDTH = Dimension.window.width;
const SCREEN_HEIGHT = Dimension.window.height;

export default class NotificationSearchScreen extends Component {
    itemPerPage = Setting.PER_PAGE;
    page = Setting.PAGE;

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            dateFrom: new Date(today.getFullYear(), today.getMonth()),
            dateTo: new Date(today.getFullYear(), (today.getMonth() + 1), 0),
            keyword: '',
            reminderList: [],
            isFetching: false,
            isSearch: false,
        };
        this.setDateFrom = this.setDateFrom.bind(this);
        this.setDateTo = this.setDateTo.bind(this);
    }

    setDateFrom(newDate) {
        this.setState({
            dateFrom: newDate
        });
    }

    setDateTo(newDate) {
        this.setState({
            dateTo: newDate
        });
    }

    getFirstDayOfMonth() {
        let date = new Date();
        return '01/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getLastDayOfMonth() {
        let date = new Date();
        return (new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()) + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getKeyword = (text) => {
        this.setState({ keyword: text });
    };

    convertDateObjectToTime(date) {
        return date.getTime()/1000;
    }

    loadData() {
        this.setState({
            isFetching: true
        }, () => {
            getReminderList(this.state.keyword, this.convertDateObjectToTime(this.state.dateFrom), this.convertDateObjectToTime(this.state.dateTo), (this.page - 1) * this.itemPerPage, this.itemPerPage).then(reminders => {
                // alert(JSON.stringify(reminders.data));
                let newData = [];
                _.forEach(reminders.data, reminder => {
                    let isExist = _.find(this.state.reminderList, {
                        id: reminder.id
                    });
                    if (!isExist) {
                        newData.push(reminder)
                    }
                });
                this.setState({
                    reminderList: this.state.reminderList.concat(newData),
                    isFetching: false,
                    allLoaded: reminders.data.length == 0 ? true : false
                });
            }).catch( err => {
                this.setState({
                    reminderList: [],
                    isFetching: false,
                    allLoaded: false
                });
            });
        });
    }

    removeItemFromList = (rid) => {
        // let newArray = [];
        let newArray = this.state.reminderList.filter(function( item ) {
            return item.id !== rid;
        });
        this.setState({
            reminderList: newArray,
        });
    };

    onRefresh = () => {
        this.page = 1;
        this.setState({
            reminderList: []
        });
        this.loadData();
        // this.props.getOrderList((this.page - 1) * this.itemPerPage, this.itemPerPage, this.keyWord, this.statusID);
    };

    loadMore = () => {
        // alert(this.props.isFetching);
        if (!this.state.allLoaded && !this.state.isFetching) {

            this.setState({
                isFetching: true
            });
            this.page = this.page + 1;
            this.loadData();
        }
    };

    getReminderDetail = (item) => {
        this.props.navigation.navigate('DetailNotification', {
            reminderData: item,
            removeReminder: this.removeItemFromList,
            refreshReminder: this.onRefresh,
        });
    };

    extractItemKey = (item) => `${item.id}`;

    renderItem = ({ item }) => {

        return (
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.getReminderDetail(item)}>
                    <View style={styles.type}>
                        <Text style={styles.txtType}>Company</Text>
                    </View>
                    <View>
                        <Text style={styles.information}>{item.content ? item.content.replace(/(<([^>]+)>)/ig,"") : item.content}</Text>
                    </View>
                    <View>
                        <Text>Người tạo:
                            <Text style={{fontWeight: 'bold', color: '#8a6d3b'}}> {item.first_name} {item.last_name}</Text>
                        </Text>
                    </View>
                    <View>
                        <Text>Thời gian:
                            <Text style={{fontWeight: 'bold',}}> {this.timeConverter(item.start_time)} - {this.timeConverter(item.finish_time)}</Text>
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    ListEmpty = () => {
        return (
            //View to show when list is empty
            <View style={styles.MainContainer}>
                <Text style={{ textAlign: 'center' }}>Không tìm thấy thông báo phù hợp!</Text>
            </View>
        );
    };

    timeConverter(UNIX_timestamp) {
        let date = new Date(UNIX_timestamp*1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = "0" + date.getMinutes();
        // return hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' + year;
        return day + '/' + month + '/' + year;
    }

    getReminderSearch() {
        // alert(this.state.dateFrom.getTime());
        this.setState({
            isSearch: true,
        });
        this.loadData();
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header noShadow iosBarStyle={"light-content"} androidStatusBarColor="#2a81ab"
                        style={{backgroundColor: '#2a81ab'}}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack(null)}>
                            <Icon style={{color: '#fff'}} ios='ios-arrow-round-back' android="md-arrow-round-back"/>
                        </Button>
                    </Left>
                    <Body style={styles.body}>
                    <Title style={{color: '#fff'}}>Tìm kiếm thông báo</Title>
                    </Body>
                    <Right>
                        <Button transparent
                        onPress={() => this.getReminderSearch()}
                        >
                            <Text style={{color: '#fff'}}>Xem</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder style={styles.content} contentContainerStyle={{flex: 1}}
                         refreshControl={
                             <RefreshControl
                                 onRefresh={this.onRefresh.bind(this)}
                                 refreshing={this.state.isFetching}
                                 progressBackgroundColor={'#fff'}
                             />
                         }
                >
                    <View style={styles.keyword}>
                        <Item bordered>
                            <Icon active name='search' />
                            <Input placeholder='Nhập từ khóa tìm kiếm'
                                   onChangeText={this.getKeyword}
                                   value={this.state.keyword}/>
                        </Item>
                    </View>
                    <View style={styles.date}>
                        <View style={styles.dateFrom}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Từ ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getFirstDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateFrom}
                                    disabled={false}
                                />
                            </View>

                        </View>
                        <View style={styles.dateTo}>
                            <View style={styles.txtDate}>
                                <Text style={styles.txtLabel}>Đến ngày</Text>
                            </View>
                            <View style={styles.datepicker}>
                                <DatePicker
                                    defaultDate={new Date()}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2118, 12, 31)}
                                    locale={"vn"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.getLastDayOfMonth()}
                                    textStyle={{color: Colors.tintColor}}
                                    placeHolderTextStyle={{color: "#070707"}}
                                    onDateChange={this.setDateTo}
                                    disabled={false}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={styles.reminder_list}>
                        {this.state.isSearch ?
                            <FlatList
                                data={this.state.reminderList}
                                renderItem={this.renderItem}
                                extraData={this.state.reminderList}
                                keyExtractor={this.extractItemKey}
                                ListEmptyComponent={this.ListEmpty}
                                onEndReached={() => this.loadMore()}
                                onEndReachedThreshold={0.4}
                            />
                        : null }
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    txtDate: {
        width: (SCREEN_WIDTH - 20) / 5 - 5,
    },
    datepicker: {
        width: (SCREEN_WIDTH - 20) * 3 / 10 + 5,
    },
    txtLabel: {
        color: '#d3d3d3',
        fontSize: 15,
    },
    date: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateFrom: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5,
    },
    dateTo: {
        width: (SCREEN_WIDTH - 20) / 2,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    type: {
        width:( SCREEN_WIDTH - 30)/5,
    },
    txtType: {
        padding: 5,
        backgroundColor: '#f0ad4e',
        borderRadius: 5,
        overflow: 'hidden',
        color: '#ffffff',
        fontSize: 11,
        textAlign: 'center',
    },
    div01: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    div02: {
        flexDirection: 'row',
        paddingHorizontal: 15,
    },
    txtBold: {
        fontWeight: 'bold',
    },
    container: {
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight
            }
        }),
        backgroundColor: '#2a81ab'
    },
    content: {
        backgroundColor: '#F7F7F7',
        flex: 1,
    },
    reminder_list: {
        backgroundColor: '#F7F7F7',
        flex: 1,
        marginTop: 10,
    },
    item: {
        padding: 10,
        backgroundColor: '#f1e6c1',
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginTop: 10,
    },
    information: {
        fontWeight: 'bold',
    },
    body: {
        ...Platform.select({
            ios: {
                flex: 3
            }
        }),
    },
});

