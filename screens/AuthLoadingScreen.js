import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    View,
    Alert
} from 'react-native';

class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }
    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        // alert(process.env.NODE_ENV);
        // alert(CONFIG.API);

        const token = await AsyncStorage.getItem('jwtToken');
        // const tokenF = null;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        if (token) {

            axios.interceptors.response.use((response) => {
                return response;
            },  (error) => {
                if (error.response) {
                    if (error.response.status === 401) {
                        this.props.navigation.navigate('Auth');
                    }
                } else {
                    if (error.message && error.message === 'Network Error') {
                        Alert.alert(
                            'Thông báo',
                            'Lỗi do mất kết nối internet',
                            [
                                {
                                    text: 'Ok', onPress: () => {
                                        console.log('Lỗi kết nối internet');
                                    }
                                }
                            ],
                            {
                                cancelable: false
                            },
                        );
                        // this.props.navigation.navigate('Manager');
                    }
                }
                return Promise.reject(error);
            });
            this.props.navigation.navigate('Calendar');
            // this.props.navigation.navigate('WorkList');
        } else  {
            // alert(123);
            this.props.navigation.navigate('Auth');
        }
    };

    // Render any loading content that you like here
    render() {
        return (
            <View>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        jwtToken: state.loginResult.jwtToken,
    };
}

export default connect(mapStateToProps)(AuthLoadingScreen);
