import React from "react";
import {AsyncStorage, Image, StyleSheet, View, Alert} from "react-native";
import {
    Text,
    Container,
    List,
    ListItem,
    Content,
    Icon,
    Left,
    Button,
    Body, Thumbnail,
} from "native-base";
import {connect} from "react-redux";
import {logout} from "../actions/actionLogin";
import Colors from "../constants/Colors";
import {isAdminRole} from "../apis/authenication";

export class SideBar extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            isAdminEmployee: false,
            // domain: this.props.domain
        }
    }

    logout = () => {
        Alert.alert(
            'Đăng xuất',
            'Bạn có muốn đăng xuất không?',
            [
                {
                    text: 'Đồng ý', onPress: () => {
                        AsyncStorage.clear().then(result => {
                            this.props.logout();
                            this.props.navigation.navigate('AuthLoading');
                        })
                    }
                },
                {
                    text: 'Hủy',
                    onPress: () => console.log('Hủy'),
                    style: 'cancel',
                }
            ],
            {
                cancelable: true
            },
        );

    };

    checkRoleAdmin() {
        let role = 'EMPLOYEE_ADMIN';
        isAdminRole(role).then(response => {
            if (response.data == 1) {
                this.setState({
                    isAdminEmployee: true,
                });
            } else {
                this.setState({
                    isAdminEmployee: false,
                });
            }
        }).catch(error => {
            console.log('lỗi kiểm tra vai trò admin', error.response.data)
        });
    }

    async componentDidMount() {
        await this.checkRoleAdmin();
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.logo}>
                        <View style={styles.logoImage}>
                            <Image style={styles.image} square source={require("../assets/tuha_logo.png")}/>
                        </View>
                        <View style={styles.logoText}>
                            <Text style={styles.title}>TUHA WORK</Text>
                        </View>
                    </View>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('Calendar')}
                    >
                        <Left>
                            <Icon ios='ios-calendar' android="md-calendar" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Lịch của tôi</Text>
                        </Body>
                    </ListItem>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('TimeSheet')}
                    >
                        <Left>
                            <Icon ios='ios-grid' android="md-grid" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Bảng giờ làm</Text>
                        </Body>
                    </ListItem>

                    {this.state.isAdminEmployee ?
                        <ListItem
                            icon
                            button
                            onPress={() => this.props.navigation.navigate('HRMShift')}
                        >
                            <Left>
                                <Icon ios='ios-time' android="md-time" style={styles.icon}/>
                            </Left>
                            <Body>
                            <Text style={styles.label}>Quản lý ca làm việc</Text>
                            </Body>
                        </ListItem>
                        : null}

                    {/*{this.state.isAdminEmployee ?*/}
                        {/*<ListItem*/}
                            {/*icon*/}
                            {/*button*/}
                            {/*onPress={() => this.props.navigation.navigate('HRMShift')}*/}
                        {/*>*/}
                            {/*<Left>*/}
                                {/*<Icon style={{color: 'white'}} ios='ios-time' android="md-time"/>*/}
                            {/*</Left>*/}
                            {/*<Body>*/}
                            {/*<Text style={styles.label}>Danh sách ca làm việc</Text>*/}
                            {/*</Body>*/}
                        {/*</ListItem>*/}
                        {/*: null}*/}

                    {/*{this.state.isAdminEmployee ?*/}
                        {/*<ListItem*/}
                            {/*icon*/}
                            {/*button*/}
                            {/*onPress={() => this.props.navigation.navigate('ShiftRegister')}*/}
                        {/*>*/}
                            {/*<Left>*/}
                                {/*<Icon style={{color: 'white'}} ios='ios-time' android="md-time"/>*/}
                            {/*</Left>*/}
                            {/*<Body>*/}
                            {/*<Text style={styles.label}>Đăng ký ca làm việc</Text>*/}
                            {/*</Body>*/}
                        {/*</ListItem>*/}
                        {/*: null}*/}

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('Work')}
                    >
                        <Left>
                            <Icon ios='ios-checkbox' android="md-checkbox" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Quản lý công việc</Text>
                        </Body>
                    </ListItem>

                    <ListItem
                        icon
                        button
                        onPress={() => this.props.navigation.navigate('Notification')}
                    >
                        <Left>
                            <Icon ios='ios-notifications' android="md-notifications" style={styles.icon}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Quản lý thông báo</Text>
                        </Body>
                    </ListItem>

                    {this.state.isAdminEmployee ?
                        <ListItem
                            icon
                            button
                            onPress={() => this.props.navigation.navigate('HRM')}
                        >
                            <Left>
                                <Icon ios='ios-person' android="md-person" style={styles.icon}/>
                            </Left>
                            <Body>
                            <Text style={styles.label}>Quản lý nhân sự</Text>
                            </Body>
                        </ListItem>
                    : null}
                    {this.state.isAdminEmployee ?
                        <ListItem
                            icon
                            button
                            onPress={() => this.props.navigation.navigate('ContractList')}
                        >
                            <Left>
                                <Icon ios='ios-document' android="md-document" style={styles.icon}/>
                            </Left>
                            <Body>
                            <Text style={styles.label}>Quản lý hợp đồng</Text>
                            </Body>
                        </ListItem>
                        : null}
                    {this.state.isAdminEmployee ?
                        <ListItem
                            icon
                            button
                            onPress={() => this.props.navigation.navigate('ReceiptExpenseList')}
                        >
                            <Left>
                                <Icon ios='ios-cash' android="md-cash" style={styles.icon}/>
                            </Left>
                            <Body>
                            <Text style={styles.label}>Quản lý thu chi</Text>
                            </Body>
                        </ListItem>
                        : null}

                    <ListItem
                        icon
                        button
                        onPress={() => this.logout()}
                    >
                        <Left>
                            <Icon ios='ios-log-out' android="md-log-out" style={styles.iconLogOut}/>
                        </Left>
                        <Body>
                        <Text style={styles.label}>Đăng xuất</Text>
                        </Body>
                    </ListItem>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        // userInfoData: state.userInfoResult.userInformation,
        // domain: state.domainResult.domainInfo,
    };
}

export default connect(mapStateToProps, {logout})(SideBar);

const styles = StyleSheet.create({
    iconLogOut: {
        color: '#47b5e0',
    },
    logo: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingVertical: 30,
        backgroundColor: '#f7f7f7'
        // alignItems: 'center',
    },
    logoText: {
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoImage: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 50,
        height: 50,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 37,
        textAlign: 'center',
        color: '#47b5e0',
    },
    icon: {
        color: '#47b5e0',
    },
    label: {
        color: '#47b5e0',
    },
});
