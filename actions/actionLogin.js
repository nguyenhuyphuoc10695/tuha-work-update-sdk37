import {getRefeshToken, getTuhaToken, getTuhaUserInfo, getTuhaWorkToken, loginToTuhaWork} from '../apis/authenication';

export function loginError() {
    return {type: 'LOGIN_ERROR'};
}

export function loginFail() {
    return {type: 'WRONG_USERNAME_PASSWORD'};
}

export function logout() {
    return {type: 'LOGOUT'};
}

export function startLogin() {
    return {type: 'START_LOGIN'};
}

export function loginSuccess(jwtToken) {
    return {
        type: 'LOGIN_SUCCESS',
        jwtToken
    };
}

export function networkError() {
    return {type: 'NETWORK_ERROR'};
}

export function outOfdate() {
    return {type: 'OUT_OF_DATE'};
}

export function refreshToken(reToken) {
    return {
        type: 'REFRESH_TOKEN',
        reToken
    };
}

export function login(credential, props) {
    return dispatch => {
        dispatch(startLogin());
        loginToTuhaWork(credential).then(response => {
            // console.log('aa', response.data.token);
            if (response.data.token) {
                // AsyncStorage.setItem('jwtToken', response.data.token);
                dispatch(loginSuccess(response.data.token));
            } else {
                dispatch(loginFail());
            }
        }).catch(err => {
            // console.log(err)
            if (!err.response) {
                if (err.message && err.message === 'Network Error')
                    dispatch(networkError());
                else
                    dispatch(loginError());
            } else {
                if (err.response.status === 422) {
                    dispatch(loginFail());
                } else {
                    dispatch(loginError());
                }
            }
        });
    };
}

export function loginWithTuha(credential, props) {
    return dispatch => {
        dispatch(startLogin());
        getTuhaToken(credential).then(response => {
            // console.log('aa', response.data);
            let jwt = response.data.jwt;
            if (jwt) {
                getTuhaUserInfo(jwt).then(res => {
                    // console.log(res.data);
                    getTuhaWorkToken(res.data).then(resp => {
                        // console.log(resp.data);
                        if (resp.data.token) {
                            // alert(resp.data.token);
                            dispatch(loginSuccess(resp.data.token));
                        } else {
                            dispatch(loginFail());
                        }
                    }).catch(e => {
                        console.log(e.response.data);
                    });
                }).catch(err => {
                    console.log(err);
                });
            } else {
                if (response.data.error) {
                    dispatch(outOfdate());
                } else {
                    dispatch(loginFail());
                }

            }
        }).catch(error => {
            // console.log(err)
            if (!error.response) {
                if (error.message && error.message === 'Network Error')
                    dispatch(networkError());
                else
                    dispatch(loginError());
            } else {
                if (error.response.status === 422) {
                    dispatch(loginFail());
                } else {
                    dispatch(loginError());
                }
            }
        });
    };
}
