import getEnvVars from '../environment/environment';
const { apiUrl } = getEnvVars();
export default {
    API: apiUrl,
    APP_VERSION: 1
}
